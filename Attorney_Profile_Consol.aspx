﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Attorney_Profile.aspx.cs" Inherits="_Default" Theme="General_Consol" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Day Pitney Billing Attorney Profile</title>
</head>
<body>
    <form id="frmAttorneyProfile" runat="server">
    
    <div>
        <asp:Label ID="lbl2" runat="server" Visible="true" Font-Size="12px"  
               Text="Attorney Profile" Font-Names="Verdana" Font-Bold="true"></asp:Label>
    </div>

    <asp:Label runat="server" ID="lbl1" Visible="true" Font-Size="12px" Font-Names="Verdana" Font-Italic="true"></asp:Label>

    <asp:FormView runat="server" ID="fvAttyProfile" DataSourceID="dsAttyProfile" 
        DataKeyNames="empl_uno" OnDataBound="fvAttyProfile_DataBound"
        SkinID="MyFormView"   >
        
        <ItemTemplate>
            
            <table border="0" width="100%"  >
            <thead>
                <tr>
                    <td colspan="2">
                    Attorney:  
                    <asp:HyperLink ID="hlAtty" Target="_blank" runat="server" 
                        Text='<%# Bind("atty_name") %>'  ForeColor="Blue" 
                        NavigateUrl='<%# Eval("Atty_Portal") %>'></asp:HyperLink>
                    </td>
                    <td colspan="2">
                    LAA:  
                    <asp:HyperLink ID="hlLAA" Target="_blank" runat="server" 
                        Text='<%# Bind("LAA") %>'  ForeColor="Blue" 
                        NavigateUrl='<%# Eval("Laa_Portal") %>'></asp:HyperLink>
                    </td>
                </tr>  
             </thead> 
                <tr> 
                    <td style="width:10%">Job Title:</td> 
                    <td> <%# Eval("Atty_JobTitle")%></td> 
                </tr>
                <tr>
                    <td>Office:</td> 
                    <td> <%# Eval("Atty_Office")%></td> 
                    <td>Office:</td> 
                    <td> <%# Eval("LAA_Office")%></td> 
                </tr> 
                <tr> 
                    <td>Phone:</td> 
                    <td><%# Eval("Atty_Phone")%></td> 
                    <td>Phone:</td> 
                    <td> <%# Eval("LAA_Phone")%></td>
                </tr>
                <tr>
                    <td>Department:</td> 
                    <td> <%# Eval("Atty_Dept")%></td> 
                    <td>Department:</td> 
                    <td> <%# Eval("LAA_Dept")%></td>
                </tr> 

                <thead>
                <tr>
                    <td colspan="2">
                    Billing Specialist:  
                    <asp:HyperLink ID="hlBiller" Target="_blank" runat="server" 
                        Text='<%# Bind("Bill_Specialist") %>'  ForeColor="Blue" 
                        NavigateUrl='<%# Eval("Biller_Portal") %>'></asp:HyperLink>
                    </td>
                    <td colspan="2">
                    Collection Specialist:  
                    <asp:HyperLink ID="hlCollector" Target="_blank" runat="server" 
                        Text='<%# Bind("Collector") %>'  ForeColor="Blue" 
                        NavigateUrl='<%# Eval("Collector_Portal") %>'></asp:HyperLink>
                    </td>
                </tr>  
                </thead>

                <tr> 
                    <td>Office:</td> 
                    <td> <%# Eval("Bill_Specialist_Office")%></td>
                    <td>Office:</td> 
                    <td> <%# Eval("Collector_Office")%></td> 
                </tr>
                <tr>
                    <td>Phone:</td> 
                    <td> <%# Eval("Bill_Specialist_Phone")%></td> 
                     <td>Phone:</td> 
                    <td> <%# Eval("Collector_Phone")%></td> 
                </tr> 
                
             
                <br />
   
             </table>
        </ItemTemplate>
    </asp:FormView>

    <asp:DataList runat="server" ID="dlAttyProfile" DataSourceID="dsAttyProfile" width="100%">
        <ItemTemplate>
            <asp:Label runat="server" ID="lbl2" Font-Names="Verdana" 
            Font-Bold="true" Font-Size="12px">Attorney Info:</asp:Label>
                    <br />
                    <asp:Label BackColor="#C0C0C0" ID="lblInfo" runat="server" Font-Size="Small" Font-Names="Verdana" Text='<%# Eval("Atty_Info")%>' Width="100%" BorderStyle="None" Height="100%"></asp:Label>
        </ItemTemplate>
    </asp:DataList>
    
    <asp:SqlDataSource runat="server" ID="dsAttyProfile" ConnectionString="<%$ ConnectionStrings:DPDATAHUBConnectionString %>"
     SelectCommand="select * from vw_ba_profile where empl_uno = @empl_uno">
        <SelectParameters>
            <asp:QueryStringParameter Name="empl_uno" QueryStringField="ba" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
