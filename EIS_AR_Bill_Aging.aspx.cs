﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using DP.DataLayer.DataObjects;

public partial class EIS_AR_Bill_Aging : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string clientCode = Request.QueryString["Client_Code"];

        lbl2.Text = string.Empty;
        if (clientCode != null)
            lbl2.Text = lbl2.Text + "AR Aging Bill Details for: " + getClientName(clientCode);

        lbl2.Visible = (gvBills.Rows.Count > 0);

        if (gvBills.Rows.Count > 0)
            lbl1.Text = "Click Header to sort, click Bill Number to view.";
        else
            lbl1.Text = "No Bills found.";

        Label l = Master.FindControl("lblHeader") as Label;
        l.Text = "Day Pitney Bill Aging";

    }
    protected string getClientName(string _client_Code)
    {
        string Client_Name = String.Empty;
         

        Client_Name =  DB.GetScalar("Select client_name from hbm_client where client_code = '" + _client_Code + "'").ToString();
        
        return Client_Name;
    }
}
