﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Attorney_Profile.aspx.cs" Inherits="_Default" Theme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Day Pitney Billing Attorney Profile</title>
</head>


<body>
    <form id="frmAttorneyProfile" runat="server">
    <ajaxToolkit:ToolkitScriptManager ID="scriptmanager" runat="server"></ajaxToolkit:ToolkitScriptManager>
    <asp:UpdateProgress DynamicLayout="false" ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpTop" >
        <ProgressTemplate>
          <div class="ModalPopupBG">
                    <img alt="progress" src="Styles/Images/loading.gif" 
                            style="padding: 10px;position:fixed;top:45%;left:50%;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>




   <div>
        <asp:Image ID="imgLogo" runat="server" ImageUrl="Pictures\logosmall.gif"  />
    </div>
    <br />
    <div>
        <asp:Label ID="lbl2" runat="server" Visible="true" Font-Size="18px"  
               Text="Attorney Profile" Font-Names="Verdana" Font-Italic="true"></asp:Label>
    </div>
    <br />
    
    <asp:Label runat="server" ID="lbl1" Visible="true" Font-Size="12px" Font-Names="Verdana" Font-Italic="true"></asp:Label>
   <br />

   <asp:UpdatePanel ID="upTop" runat="server">

    <ContentTemplate>


    <asp:FormView runat="server" ID="fvAttyProfile" DataSourceID="dsAttyProfile" 
        DataKeyNames="empl_uno" OnDataBound="fvAttyProfile_DataBound"
        SkinID="MyFormView"   >
        <ItemTemplate>
            <hr /> 
            
            <table border="0" width="60%"  >
                <tr>
                    <td colspan="2">
                    <h4>Attorney:  
                    <asp:HyperLink ID="hlAtty" Target="_blank" runat="server" 
                        Text='<%# Bind("atty_name") %>'  ForeColor="Red" 
                        NavigateUrl='<%# Eval("Atty_Portal") %>'></asp:HyperLink></h4>
                    </td>
                </tr>   
                <tr> 
                    <td style="width:10%">Job Title:</td> 
                    <td> <%# Eval("Atty_JobTitle")%></td> 
                </tr>
                <tr>
                    <td>Office:</td> 
                    <td> <%# Eval("Atty_Office")%></td> 
                </tr> 
                <tr> 
                    <td>Phone:</td> 
                    <td><%# Eval("Atty_Phone")%></td> 
                </tr>
                <tr>
                    <td>Department:</td> 
                    <td> <%# Eval("Atty_Dept")%></td> 
                </tr> 

                <tr>
                    <td colspan="2">
                    <h4>LAA:  
                    <asp:HyperLink ID="hlLAA" Target="_blank" runat="server" 
                        Text='<%# Bind("LAA") %>'  ForeColor="Red" 
                        NavigateUrl='<%# Eval("Laa_Portal") %>'></asp:HyperLink></h4>
                    </td>
                </tr> 
                <tr> 
                    <td>Office:</td> 
                    <td> <%# Eval("LAA_Office")%></td> 
                </tr>
                <tr>
                    <td>Phone:</td> 
                    <td> <%# Eval("LAA_Phone")%></td>
                </tr> 
                <tr>
                    <td>Department:</td> 
                    <td> <%# Eval("LAA_Dept")%></td>
                </tr> 
                
                <tr>
                    <td colspan="2">
                    <h4>Billing Specialist:  
                    <asp:HyperLink ID="hlBiller" Target="_blank" runat="server" 
                        Text='<%# Bind("Bill_Specialist") %>'  ForeColor="Red" 
                        NavigateUrl='<%# Eval("Biller_Portal") %>'></asp:HyperLink></h4>
                    </td>
                </tr>  
                <tr> 
                    <td>Office:</td> 
                    <td> <%# Eval("Bill_Specialist_Office")%></td>
                </tr>
                <tr>
                    <td>Phone:</td> 
                    <td> <%# Eval("Bill_Specialist_Phone")%></td> 
                </tr> 
                
              
                <tr>
                    <td colspan="2">
                    <h4>Collection Specialist:  
                    <asp:HyperLink ID="hlCollector" Target="_blank" runat="server" 
                        Text='<%# Bind("Collector") %>'  ForeColor="Red" 
                        NavigateUrl='<%# Eval("Collector_Portal") %>'></asp:HyperLink></h4>
                    </td>
                </tr> 
                <tr> 
                    <td>Office:</td> 
                    <td> <%# Eval("Collector_Office")%></td> 
                </tr>
                <tr>
                    <td>Phone:</td> 
                    <td> <%# Eval("Collector_Phone")%></td> 
                </tr> 
                <br />
                <br />
             </table>
        </ItemTemplate>
    </asp:FormView>

    <asp:DataList runat="server" ID="dlAttyProfile" DataSourceID="dsAttyProfile" width="100%">
        <ItemTemplate>
            <label>Attorney Info:</label>
                    <br />

                    <asp:TextBox  ID="txtInfo" runat="server" ReadOnly="true" Font-Size="Small"
                    Text='<%# Eval("Atty_Info")%>' TextMode="MultiLine" Width="100%" BorderStyle="None" Wrap="true" Height="200px" ></asp:TextBox>
        </ItemTemplate>
    </asp:DataList>
    
    </ContentTemplate>

    </asp:UpdatePanel>
    
    <asp:SqlDataSource runat="server" ID="dsAttyProfile" 
        ConnectionString="<%$ ConnectionStrings:DPDATAHUBConnectionString %>"
        SelectCommand="select * from vw_ba_profile where empl_uno = @empl_uno">
        <SelectParameters>
            <asp:QueryStringParameter Name="empl_uno" QueryStringField="ba" />
        </SelectParameters>
    </asp:SqlDataSource>
    </form>
</body>
</html>
