﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="FinancialSummary.aspx.cs" Inherits="FinancialSummary" Theme="General" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head id="Head1" runat="server">
    <title>Financial Summary</title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form id="Form1" runat="server">
            <div class="title">
                <asp:Image runat="server" ID="imgDP" ImageUrl="Pictures/logosmall.gif" />
            </div>

             <div style="float:right; vertical-align:bottom; color:#FFFFFF; margin-right:5px" >
                <a href="javascript:window.print()"><img src="Pictures/print.gif" alt="printer"/></a>
             </div>

    <asp:FormView ID="fvFinSum" runat="server" DataSourceID="dsFinSum" DataKeyNames="folderID"                       SkinID="MyFormView">
    <ItemTemplate>
            
            <table border="2" width="100%"  >
            
                <tr>
                    <td><b> <%# Eval("Client_Code")%></b></td> 
                    <td><b> <%# Eval("Client_Name")%></b></td> 
                </tr>   
                <tr> 
                    <td style="width:10%"><b>CRP:</b></td> 
                    <td> <%# Eval("crp")%></td> 
                </tr>
                <tr>
                    <td><b>Client Opened:</b></td> 
                    <td> <%# Eval("client_open_date", "{0:M-dd-yyyy}")%></td> 
                </tr> 
                <tr>
                    <td style="width:20%"><b>Client Category:</b></td>
                    <td><b> <%# Eval("clnt_cat_desc")%></b></td> 
                </tr>
                <tr>
                    <td style="width:20%"><b>Client Class:</b></td>
                    <td><b> <%# Eval("clnt_class_desc")%></b></td> 
                </tr>
                <br />
                <br />
             </table>
             <br />
             <table  border="2" width="100%"  >
                <thead>
                    <tr>
                        <td align="left"><b>Metric</b></td>
                        <td align="right"><b>Total</b></td>
                        <td align="right"><b>0-30</b></td>
                        <td align="right"><b>31-60</b></td>
                        <td align="right"><b>61-90</b></td>
                        <td align="right"><b>91-120</b></td>
                        <td align="right"><b>121-150</b></td>
                        <td align="right"><b>>150</b></td>
                    </tr>
                 </thead>
                 <tr>
                    <td><b>AR (Fees & Disb)</b></td>
                    <td align="right"><%# Eval("ar_fee_disb_total", "{0:C}")%></td>
                    <td align="right"><%# Eval("ar_fee_disb_0_30", "{0:C}")%></td>
                    <td align="right"><%# Eval("ar_fee_disb_31_60", "{0:C}")%></td>
                    <td align="right"><%# Eval("ar_fee_disb_61_90", "{0:C}")%></td>
                    <td align="right"><%# Eval("ar_fee_disb_91_120", "{0:C}")%></td>
                    <td align="right"><%# Eval("ar_fee_disb_121_150", "{0:C}")%></td>
                    <td align="right"><%# Eval("ar_fee_disb_over150", "{0:C}")%></td>
                 </tr>
                 <tr>
                    <td><b>WIP Fees</b></td>
                    <td align="right"><%# Eval("wip_fees_total", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_fees_0_30", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_fees_31_60", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_fees_61_90", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_fees_91_120", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_fees_121_150", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_fees_over_150", "{0:C}")%></td>
                 </tr>
                 <tr>
                    <td><b>WIP Costs</b></td>
                    <td align="right"><%# Eval("wip_costs_total", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_costs_0_30", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_costs_31_60", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_costs_61_90", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_costs_91_120", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_costs_121_150", "{0:C}")%></td>
                    <td align="right"><%# Eval("wip_costs_over_150", "{0:C}")%></td>
                 </tr>
                 <tr></tr>
                 <tr>
                    <td><b>Retainer Balance</b></td>
                    <td align="right"><%# Eval("retainer_balance", "{0:C}")%></td>
                </tr>
                <tr>
                    <td><b>Unapplied Balance</b></td>
                    <td align="right"><%# Eval("unapplied_balance", "{0:C}")%></td>
                </tr>
                <tr>
                    <td><b>Trust Balance</b></td>
                    <td align="right"><%# Eval("trust_balance", "{0:C}")%></td>
                </tr>
             </table>
              <br />
             <table  border="2" width="100%"  >
                <thead>
                    <tr>
                        <td style="width: 50%;"></td>
                        <td align="right" style="width: 25%;"><b>Rolling 12 Months</b></td>
                        <td align="right" style="width: 25%;"><b>Life To Date</b></td>
                    </tr>
                </thead>
                <tr>
                    <td align="left"><b>Billed Amount</b></td>
                    <td align="right"><%# Eval("roll12_fees_billed", "{0:C}")%></td>
                    <td align="right"><%# Eval("ltd_fees_billed", "{0:C}")%></td>
                </tr>
                <tr>
                    <td align="left"><b>Realization Base Pcnt</b></td>
                    <td align="right"><%# Eval("roll12_rlz_base_pcnt", "{0:###.##}") + "%"%></td>
                    <td align="right"><%# Eval("ltd_rlz_base_pcnt", "{0:###.##}") + "%"%></td>
                </tr>
                <tr></tr>
                <tr>
                    <td align="left"><b>Discretionary Fee WO Value</b></td>
                    <td align="right"><%# Eval("roll12_discr_fee_wo_val", "{0:C}")%></td>
                    <td align="right"><%# Eval("ltd_discr_fee_wo_val", "{0:C}")%></td>
                </tr>
                <tr>
                    <td align="left"><b>Discretionary Fee WO</b></td>
                    <td align="right"><%# Eval("roll12_discr_fee_wo", "{0:###.##}") + "%"%></td>
                    <td align="right"><%# Eval("ltd_discr_fee_wo", "{0:###.##}") + "%"%></td>
                </tr>

                <tr>
                    <td align="left"><b>Hard Disb WO</b></td>
                    <td align="right"><%# Eval("roll12_hard_disb_wo", "{0:C}")%></td>
                    <td align="right"><%# Eval("ltd_hard_disb_wo", "{0:C}")%></td>
                </tr>

                <tr></tr>

                <tr>
                    <td align="left"><b>Fee Receipts</b></td>
                    <td align="right"><%# Eval("roll12_fee_receipt", "{0:C}")%></td>
                     <td align="right"><%# Eval("ltd_fee_receipt", "{0:C}")%></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left"><b>AR Fee WO</b></td>
                    <td align="right"><%# Eval("roll12_ar_fee_wo", "{0:C}")%></td>
                    <td align="right"><%# Eval("ltd_ar_fee_wo", "{0:C}")%></td>
                    <td></td>
                </tr>
                <tr></tr>
                <tr>
                    <td align="left"><b>Bill Speed (Days)</b></td>
                    <td align="right"><%# Eval("bill_speed")%></td>
                    <td align="right"><%# Eval("ltd_bill_speed")%></td>
                    <td></td>
                </tr>
                <tr>
                    <td align="left"><b>Collect Speed (Days)</b></td>
                    <td align="right"><%# Eval("collect_speed")%></td>
                    <td align="right"><%# Eval("ltd_collect_speed")%></td>
                    <td></td>
                </tr>
             </table>
        </ItemTemplate>

    </asp:FormView>
    <asp:SqlDataSource ID="dsFinSum" runat="server" ConnectionString="<%$ ConnectionStrings:DPDATAHUBConnectionString %>"
     SelectCommand="select * from financialsummary where client_code = @client_code">
        <SelectParameters>
            <asp:QueryStringParameter Name="client_code" QueryStringField="client_code" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

</form>
</body>
</html>

