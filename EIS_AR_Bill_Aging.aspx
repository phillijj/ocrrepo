﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EIS_AR_Bill_Aging.aspx.cs" Inherits="EIS_AR_Bill_Aging" Theme="General" MasterPageFile="~/Site.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <asp:Label ID="lbl2" runat="server" Visible="true" Font-Size="18px"  
               Text="" Font-Names="Verdana" Font-Italic="true"></asp:Label>
    
    <br />
    
    <asp:Label runat="server" ID="lbl1" Visible="true" Font-Size="12px" Font-Names="Verdana" Font-Italic="true"></asp:Label>
   <br />
    <asp:GridView ID="gvBills" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="tran_uno" DataSourceID="dsCMS" SkinID="MyGrid" AllowSorting="True">
        <Columns>
            <asp:TemplateField HeaderText="tran_uno" SortExpression="tran_uno" 
                Visible="False">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("tran_uno") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Bill Number" SortExpression="Bill_Num">
            <ItemTemplate>
            <asp:HyperLink ID="hlBillImage" Target="_blank" runat="server" 
                Text='<%# Bind("bill_num") %>'  ForeColor="Red" 
                NavigateUrl='<%# string.Format("http://hfapps02/expertimage/openimage.aspx?DocumentType=cmsbill&DocumentID={0}",Eval("bill_num")) %>'  />
            </ItemTemplate>
            </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Bill Date" SortExpression="bill_date">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("bill_date","{0:M-dd-yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Billed Fees" SortExpression="Billed_Fees">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Billed_Fees", "{0:C}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Billed Disb" SortExpression="Billed_Disb">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("Billed_Disb", "{0:C}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Billed Total" SortExpression="Billed_Total">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("Billed_Total", "{0:C}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AR Fees" SortExpression="AR_Fees">
                 <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("AR_Fees", "{0:C}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AR Disb" SortExpression="AR_Disb">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("AR_Disb", "{0:C}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AR Total" SortExpression="AR_Total">
                 <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("AR_Total", "{0:C}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Last Payment" SortExpression="Last_Payment">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("Last_Payment","{0:M-dd-yyyy}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="dsCMS" runat="server" 
        ConnectionString="<%$ ConnectionStrings:System.Data.SqlClient %>" 
        SelectCommand="SELECT 
                        b.tran_uno, 
                        b.bill_num, 
                        b.bill_date, 
                        SUM(a.Billed_Fees) Billed_Fees, 
                        SUM(a.Billed_Disb) Billed_Disb, 
                        SUM(a.Billed_Fees + a.Billed_Disb) Billed_Total, 
                        SUM(a.AR_Fees) AR_Fees, 
                        SUM(a.AR_Disb) AR_Disb, 
                        SUM(a.AR_Fees + a.AR_Disb) AR_Total, 
                        MAX(a.Last_Payment) Last_Payment 
                        FROM DBO._DBH_ARBills a 
                        inner join eis.vMatter2_DBH m on a.matter_uno = m.matter_uno 
                        inner join DBO.BLT_BILL b on a.bill_Tran_uno = b.tran_uno 
                        WHERE m.client_uno= 
                                    (select Convert(varchar,CLIENT_UNO) from hbm_client where client_code = @client_code)
                        GROUP BY b.tran_uno, b.Bill_Num, b.Bill_Date, b._Bill_Status 
                        ORDER BY BILL_NUM">
        <SelectParameters>
            <asp:QueryStringParameter Name="client_code" QueryStringField="client_code" />
        </SelectParameters>
    </asp:SqlDataSource>
    </asp:Content>