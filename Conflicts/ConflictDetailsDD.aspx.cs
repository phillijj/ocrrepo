﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using DP.DataLayer.DataObjects;
using System.Data.SqlClient;
using DP.Framework.Log;
using System.Web.Configuration;

public partial class Conflicts_ConflictDetailsDD : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {
        //Request.QueryString["uno"];
        if (Request.QueryString["fid"] == null)
            Response.Redirect("http://dpnbi4/conflicts/timeout.aspx");


        string _autoId = Request.QueryString["autoid"];
        string _signOff = Request.QueryString["signoff"];
        string _fid = Request.QueryString["fid"];
        string _source = Request.QueryString["source"];
        lblClient.Text = HttpUtility.UrlDecode(Request.QueryString["name"]);

        bool b = (_source == "client");

        pnlMatters.Visible = b || _source == "ROclient";
        pnlRelatedNames.Visible = b || _source == "ROclient";

        //hide Party Hits
        gvPartyHits.Enabled = false;
        pnlPartyHits.Visible = false;

        HyperLink hl = (HyperLink)Master.FindControl("hlPrint");
        hl.NavigateUrl = WebConfigurationManager.AppSettings["ConflictReportLocation"].ToString() +
                                                             "&rs:Command=Render&fid=" + _fid;
        hl.Visible = false;

        ImageButton ib = (ImageButton)Master.FindControl("ibHelp");
        ib.Visible = false;
        Label lHelp = Master.FindControl("lblHelp") as Label;
        lHelp.Visible = false;

        Label l = Master.FindControl("lblHeader") as Label;
        l.Text = "Day Pitney Conflicts Client Hit Details";

        lblStatus.Text = GetStatus(_signOff);
    }
    private string GetStatus(string _signOff)
    {

        if (_signOff == string.Empty)
            return _signOff;

        IList<Reason> list = new List<Reason>();

        if (ViewState["Reasons"] == null)
            ViewState["Reasons"] = DAL.GetReasonCodes();

        list = (List<Reason>)ViewState["Reasons"];
        
        Reason r;

        try
        {
            r = (Reason)list.Where(p => p.txtReasonCode == _signOff).Single();
        }
        catch (Exception)
        {
            return string.Empty;
        }

        return "Resolved: " + r.txtReasonDesc;

    }
}