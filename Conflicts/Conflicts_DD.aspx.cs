﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using DP.DataLayer.DataObjects;
using System.Data.SqlClient;
using DP.Framework.Log;
using System.Web.Configuration;

public partial class Conflicts_DD : System.Web.UI.Page
{
    string _autoId;
    string _fid;
    string _signOff;
    string _partyId;
    string _returnpartyId;

    public string AutoId
    {
        get { return _autoId; }
        set {_autoId = value; }
    }
    public string FID
    {
        get { return _fid; }
        set { _fid = value; }
    }
    public string SignOff
    {
        get { return _signOff; }
        set { _signOff = value; }
    }
    public string PartyId
    {
        get { return _partyId; }
        set { _partyId = value; }
    }
    public string ReturnPartyId
    {
        get { return _returnpartyId; }
        set { _returnpartyId = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string _uno = Request.QueryString["uno"];
        string _source = Request.QueryString["source"];
        lblClient.Text = HttpUtility.UrlDecode(Request.QueryString["name"]);
        AutoId = Request.QueryString["autoid"];
        FID = Request.QueryString["fid"];
        SignOff = Request.QueryString["signoff"];
        PartyId = Request["partypkid"];
        ReturnPartyId=Request["ddlpartypkid"];
        if (!Page.IsPostBack)
        {
            lbBack.PostBackUrl = "~/Conflicts/ClearHitsMain.aspx?fid=" + FID + "&partypkid=" + ReturnPartyId + "&activetab=" + Request.QueryString["activetab"];
            ddlSignOff.DataBind();
        }

        bool b = (_source == "client");

        pnlMatters.Visible = b;
        pnlRelatedNames.Visible = b || _source=="ROclient";
        pnlPartyHits.Visible = b;
        pnlSignOff.Visible = !(AutoId == null);

        HyperLink hl = (HyperLink)Master.FindControl("hlPrint");
        hl.NavigateUrl = WebConfigurationManager.AppSettings["ConflictReportLocation"].ToString() + 
                                                             "&rs:Command=Render&fid=" + FID;
        hl.Visible = true;

        Label l = Master.FindControl("lblHeader") as Label;
        l.Text = "Day Pitney Conflicts Client Hit Details";

        lblStatus.Text = GetStatus(SignOff);


    }
    private string GetStatus(string _signOff)
    {
        if (_signOff == string.Empty)
            return _signOff;

        IList<Reason> list = new List<Reason>();
        list = DAL.GetReasonCodes();
        Reason r;

        try
        {
            r = (Reason)list.Where(p => p.txtReasonCode == _signOff).Single();
        }
        catch(Exception)
        {
            return string.Empty;
        }

        return "Resolved: " + r.txtReasonDesc;
            
    }

    protected void gvClientContacts_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            // Programmatically reference the PopupControlExtender
            PopupControlExtender pce = e.Row.FindControl("extend1") as PopupControlExtender;

            // Set the BehaviorID
            string behaviorID = string.Concat("pce", e.Row.RowIndex);
            pce.BehaviorID = behaviorID;

            // Programmatically reference the Image control
            Image i = (Image)e.Row.Cells[1].FindControl("imgMagnify");

            // Add the clie nt-side attributes (onmouseover & onmouseout)
            string OnMouseOverScript = string.Format("$find('{0}').showPopup();", behaviorID);
            string OnMouseOutScript = string.Format("$find('{0}').hidePopup();", behaviorID);

            i.Attributes.Add("onmouseover", OnMouseOverScript);
            i.Attributes.Add("onmouseout", OnMouseOutScript);
        }
    }
    [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
    public static string GetDynamicContent(string contextKey)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        SqlParameter[] _params = {new SqlParameter("@uno",SqlDbType.Int)};

            _params[0].Value = int.Parse(contextKey);
            DataTable dt = DAL.GetDataTable("prc_qsel_NameDetails",_params, "MetastormConnectionString");
            using (System.Data.DataTableReader reader = dt.CreateDataReader())
            {

                sb.Append("<table>");
                if (!reader.HasRows)
                    sb.Append("No Data found </table>");
                else
                {
                    sb.Append("<tr>");
                    sb.Append("<td style='width:200px;'><b><u>Name</u></b></td>");
                    sb.Append("<td style='width:300px;'><b><u>Address</u></b></td>");
                    sb.Append("<td style='width:80px;'><b><u>City</u></b></td>");
                    sb.Append("<td style='width:50px;'><b><u>State</u></b></td>");
                    sb.Append("<td style='width:50px;'><b><u>Country</u></b></td>");
                    sb.Append("<td style='width:50px;'><b><u>Zip</u></b></td>");
                    sb.Append("<td style='width:50px;'><b><u>Empl?</u></b></td>");
                    sb.Append("<td style='width:50px;'><b><u>Vend?</u></b></td>");
                    sb.Append("</tr>");
                    while (reader.Read())
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + reader["Name"].ToString() + "</td>");
                        sb.Append("<td>" + reader["Address"].ToString() + "</td>");
                        sb.Append("<td>" + reader["City"].ToString() + "</td>");
                        sb.Append("<td>" + reader["State_Code"].ToString() + "</td>");
                        sb.Append("<td>" + reader["Country_Code"].ToString() + "</td>");
                        sb.Append("<td>" + reader["Post_Code"].ToString() + "</td>");
                        sb.Append("<td>" + reader["is_employee"].ToString() + "</td>");
                        sb.Append("<td>" + reader["is_vendor"].ToString() + "</td>");

                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                }
            }


        return sb.ToString();

    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        Boolean updated = true;
        string d = ddlSignOff.SelectedValue;
        if (SignOff == string.Empty && d == string.Empty)
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('Sign-Off reason must be selected.');", true);
            return;
        }

        bool b = DAL.ClearHits(d, Convert.ToInt32(AutoId),"");
        if (updated == true)//only set if updated is true, once false it is kept
            updated = b;
 
        if (updated)
            Server.Transfer("~/Conflicts/Default.aspx?fid=" + FID);        
    }
    protected void ddlSignOff_DataBound(object sender, EventArgs e)
    {
        ddlSignOff.Items.Clear();
        ddlSignOff.Items.Add(new ListItem("Client", "CurrentClient"));
        ddlSignOff.Items.Add(new ListItem("Friendly", "MyFriend"));
        ddlSignOff.Items.Add(new ListItem("OK", "OK"));
        if (SignOff == string.Empty)//not signed off
        {
            ddlSignOff.Items.Add(new ListItem("Select Sign-Off reason", ""));
            ddlSignOff.SelectedValue = "";
        }
        else
        {
            ddlSignOff.Items.Add(new ListItem("Undo", ""));
            ddlSignOff.SelectedValue = SignOff;
        }
    }
}