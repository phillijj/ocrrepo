﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Conflicts_Default : System.Web.UI.Page
{
     string _fid;

    public String FID
    {
        get { return _fid; }
        set { _fid = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
        }

         FID = Request.QueryString["fid"];
         HyperLink hl = (HyperLink)Master.FindControl("hlPrint");
         hl.NavigateUrl = WebConfigurationManager.AppSettings["ConflictReportLocation"].ToString() + "&rs:Command=Render&fid=" + FID;
         hl.Visible = true;
         Label l = Master.FindControl("lblHeader") as Label;
         l.Text = "Day Pitney Online Conflict Report";
         if(FID != null)
            SetCounts();
    }
    private void SetCounts()
    {
        IList<string> al = new List<string>();
        al = DAL.GetHitCount(FID,null,null);

        if (al.Count == 6)
        {
            AjaxControlToolkit.TabContainer tc = (AjaxControlToolkit.TabContainer)tc1;
            foreach (object obj in tc.Controls)
            {
                if (obj is AjaxControlToolkit.TabPanel)
                {
                    AjaxControlToolkit.TabPanel tabPanel = (AjaxControlToolkit.TabPanel)obj;
                    if (tabPanel.ID == "tpClient")
                    {
                        tabPanel.HeaderText = "Clients (" + al[0] + ")(" + al[3] + ")";
                        if (gvClientHits.FooterRow != null)
                        {
                            Button b = (Button)gvClientHits.FooterRow.FindControl("btnClientUpdate");
                            if (b != null)
                            {
                                b.Visible = true;
                                if (cbClientAll.Checked && al[3].ToString() == "0")
                                    b.Visible = false;
                                else if (!cbClientAll.Checked && al[0].ToString() == "0")
                                    b.Visible = false;
                            }
                        }
                    }
                    else if (tabPanel.ID == "tpName")
                    {
                        tabPanel.HeaderText = "Names (" + al[1] + ")(" + al[4] + ")";

                        btnNameUpdate.Visible=true;
                        if (cbNameAll.Checked && al[4].ToString() == "0")
                            btnNameUpdate.Visible = false;
                        else if (!cbNameAll.Checked && al[1].ToString() == "0")
                            btnNameUpdate.Visible = false;
                    }
                    else if (tabPanel.ID == "tpPendingName")
                    {
                        tabPanel.HeaderText = "Pending Names (" + al[2] + ")(" + al[5] + ")";
       
                        btnPendingNameUpdate.Visible = true;
                        if (cbPendingNameAll.Checked && al[5].ToString() == "0")
                            btnPendingNameUpdate.Visible = false;
                        else if (!cbPendingNameAll.Checked && al[2].ToString() == "0")
                            btnPendingNameUpdate.Visible = false;
 
                    }
                }
            }
        }
    }

    protected void btnClientUpdate_Click(object sender, EventArgs e)
    {
        Boolean updated = true;
        DropDownList ddl = (DropDownList)gvClientHits.HeaderRow.FindControl("ddlSignOff");
        string d = ddl.SelectedValue;

        if (!cbClientAll.Checked && d == string.Empty)//do not allow clearing on uncleared items
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('Sign-Off reason must be selected.');", true);
            return;
        }

        foreach (GridViewRow gvr in gvClientHits.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("cbClientSignOff");
            if (cb.Checked)
            {
                Label l = (Label)gvr.FindControl("lblPK");
                bool b = DAL.ClearHits(d, Convert.ToInt32(l.Text),"");
                if (updated == true)//only set if updated is true, once false it is kept
                    updated = b;

            }
        }
        if (updated)
        {
            gvClientHits.DataBind();
            dlClientHitCount.DataBind();
            SetCounts();
        }

    }
    protected void btnNameUpdate_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < dlNameHits.Items.Count; i++)
        {
            Label l = (Label)dlNameHits.Items[i].FindControl("lblID");
            DropDownList ddl = (DropDownList)dlNameHits.Items[i].FindControl("ddlSignOff");
            HiddenField hf = (HiddenField)dlNameHits.Items[i].FindControl("hfSignOffCode");

            if (l != null && ddl != null)
            {
                if (!(ddl.SelectedValue == hf.Value))
                {
                    DAL.ClearHits(ddl.SelectedValue, Convert.ToInt32(l.Text),"");
                }
            }
        }

        dlNameHitCount.DataBind();
        dlNameHits.DataBind();
        gvNameClearedHits.DataBind();
        SetCounts();
    }
    
    protected void btnPendingNameUpdate_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < dlPendingHits.Items.Count; i++)
        {
            Label l = (Label)dlPendingHits.Items[i].FindControl("lblIDb");
            DropDownList ddl = (DropDownList)dlPendingHits.Items[i].FindControl("ddlSignOffb");
            HiddenField hf = (HiddenField)dlPendingHits.Items[i].FindControl("hfSignOffCodeb");

            if (l != null && ddl != null)
            {
                if (!(ddl.SelectedValue == hf.Value))
                {
                    DAL.ClearHits(ddl.SelectedValue, Convert.ToInt32(l.Text),"");
                }
            }
        }

        dlPendingNameHitCount.DataBind();
        dlPendingHits.DataBind();
        SetCounts();

    }
    protected void cbClientAll_CheckedChanged(object sender, EventArgs e)
    {
        this.hfClientSignOff.Value = (cbClientAll.Checked ? "0" : "1");
        gvClientHits.EmptyDataText = (cbClientAll.Checked ? "No Cleared Client Hit Data Found" : "No Uncleared Client Hit Data Found");
    }
    protected void cbNameAll_CheckedChanged(object sender, EventArgs e)
    {
        this.hfNameSignOff.Value = (cbNameAll.Checked ? "0" : "1");
        
    }
    protected void cbPendingNameAll_CheckedChanged(object sender, EventArgs e)
    {
        this.hfPendingNameSignOff.Value = (cbPendingNameAll.Checked ? "0" : "1");
    }
    protected void dlNameHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        HiddenField hf = (HiddenField)e.Item.FindControl("hfSignOffCode");
        DropDownList dl = (DropDownList)e.Item.FindControl("ddlSignOff");
        if (hf != null && dl != null)
        {
            FillDropDownLists(ref dl, hf.Value);
        }
    }
    protected void dlPendingHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        HiddenField hf = (HiddenField)e.Item.FindControl("hfSignOffCodeb");
        DropDownList dl = (DropDownList)e.Item.FindControl("ddlSignOffb");
        if (hf != null && dl != null)
        {
            FillDropDownLists(ref dl, hf.Value);
        }
    }
    
    private void FillDropDownLists(ref DropDownList _dl, string _hf)
    {
        _dl.Items.Clear();

        _dl.Items.Add(new ListItem("Client", "CurrentClient"));
        _dl.Items.Add(new ListItem("Friendly", "MyFriend"));
        _dl.Items.Add(new ListItem("OK", "OK"));

        if (_hf != "")
        {
            _dl.Items.Add(new ListItem("Undo", ""));
            _dl.SelectedValue = _hf;
        }
        else
        {
            _dl.Items.Add(new ListItem("Select Sign-Off reason", ""));
            _dl.SelectedValue = "";
        }
    }
    protected void ddlSignOff_ItemDataBound(object sender, EventArgs e)
    {
        
    }
    protected void gvClientHits_DataBound(object sender, EventArgs e)
    {
        
    }
    protected void gvClientHits_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType.ToString() == "Header")
        {
            DropDownList dl = (DropDownList)e.Row.FindControl("ddlSignOff");
            if (dl != null)
            {
                dl.Items.Clear();
                dl.Items.Add(new ListItem("Client", "CurrentClient"));
                dl.Items.Add(new ListItem("Friendly", "MyFriend"));
                dl.Items.Add(new ListItem("OK", "OK"));
                if (cbClientAll.Checked)//show cleared items, give unclear option
                    dl.Items.Add(new ListItem("Undo", ""));
                else//show uncleared items, give signoff option
                    dl.Items.Add(new ListItem("Select Sign-Off reason", ""));

                dl.SelectedValue = "";
            }
        }

    }
}