﻿create database ASPNET_ApplicationServices;

USE [master]
GO
CREATE LOGIN [appServices] WITH PASSWORD=N'appServices185', 
DEFAULT_DATABASE=[ASPNET_ApplicationServices], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
USE [ASPNET_ApplicationServices]
GO
CREATE USER [appServices] FOR LOGIN [appServices]
GO
USE [ASPNET_ApplicationServices]
GO
EXEC sp_addrolemember N'db_owner', N'appServices'
GO

Open visual studio command prompt from the Visual studio tools folder from the start menu and type aspnet_regsql

and follow the wizard to register the database for asp.net membership and role providers.


create function fn_GetSignoffComments
(@hitautoid int)

returns varchar(max)
as

begin
declare @counter int, @count int, @msg varchar(max)

declare @t table(id int, msg varchar(250))

select @counter = 1;

insert into @t(id,msg)
select 
row_number() over (partition  by hitautoid order by SignOffDate desc)  as id,
SignOffBy + ' ' + CONVERT(varchar(10),signoffdate,110) + ' ' + rtrim(comments) as msg
from nbiconflictsignoff 
where HitAutoId = @hitautoid

select @count = MAX(id) from @t

while @counter <= @count
begin

select @msg = isnull(@msg,'') + isnull(msg,'') + CHAR(13)+CHAR(10) from @t where id = @counter

select @counter = @counter + 1;
end


return @msg;
end


create table NBI_CMS_Conflict_Related_Names
(
pkRelatedNameId int identity(1,2) primary key,
CMSsearchID int,
SessionID int,
PartyPKId int,
eFolderId nvarchar(31),
Relationship varchar(5),
Description varchar(80),
is_client varchar(1),
dom_is_client varchar(1),
hit_name varchar(120),
dom_hit_name varchar(120),
client_code varchar(10),
Relationship_desc varchar(80),
Last_Modified datetime default getdate()
)