﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using DPConflicts;
using DP.Framework.Log;
using System.Net.Mail;

public partial class Conflicts_ClearHitsMain : System.Web.UI.Page
{
    #region Properties
    public int HITCOUNT//JJP 05/01/2013
    {
        get { return Convert.ToInt32(ViewState["_hc"]); }
        set { ViewState["_hc"] = value; }
    }
    public string PKID
    {
        get { return ViewState["_pkid"].ToString(); }
        set { ViewState["_pkid"] = value; }
    }

    public string FID
    {
        get { return ViewState["_fid"].ToString(); }
        set { ViewState["_fid"] = value; }
    }
    public string MRPCRP
    {
        get { return ViewState["_mrpcrp"].ToString(); }
        set { ViewState["_mrpcrp"] = value; }
    }
    public string USAGE
    {
        get { return ViewState["_usage"].ToString(); }
        set { ViewState["_usage"] = value; }
    }
    
    #endregion

    public static readonly int GRIDITEMSSELECTED = Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["GridItemsSelected"].ToString());
       
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            FID = Request.QueryString["fid"];
            if (Request.QueryString["fid"] == null)
                Response.Redirect("http://dpnbi/conflicts/timeout.aspx");

            //JJP 07/10/2013 add string.Empty check
            //JJP 07/15/2013 add zero check
            if (Request.QueryString["partypkid"] == null ||
                Request.QueryString["partypkid"].ToString() == String.Empty ||
                Request.QueryString["partypkid"].ToString() == "0")
                PKID = "-99";
            else
                PKID = Request.QueryString["partypkid"];
            //JJP 07/10/2013 add string.Empty check


            if (Request.QueryString["mrpcrp"] == null ||
                Request.QueryString["mrpcrp"].ToString() == String.Empty)
                MRPCRP = "-99";
            else
                MRPCRP = Request.QueryString["mrpcrp"];


            ddlPKId.DataBind();
            ddlPKId.SelectedValue = PKID;

            if (Request.QueryString["secure"] != null)
            {
                if (Request.QueryString["secure"] == "true")
                {
                    ddlMRPCRP.DataBind();//JJP 04/22/2013
                    ddlMRPCRP.SelectedValue = MRPCRP;
                    this.ddlMRPCRP.Enabled = false;

                    this.ddlPKId.Enabled = (PKID == "-99");
                    tpEmail.Visible = false;

                }
            }
            if (Request.QueryString["activetab"] != null)
            {
                this.tc1.ActiveTab.TabIndex = Convert.ToInt16(Request.QueryString["activetab"]);
            }


            //set Grid Items Selected
            foreach (var item in chkClientAllList.Items.Cast<ListItem>())
            {
                if (item.Value == "0")
                    item.Text = "Select first " + GRIDITEMSSELECTED.ToString() + " active and retired displayed hits";
                if (item.Value == "1")
                    item.Text = "Select all retired items in the first " + GRIDITEMSSELECTED.ToString() + " hits";
            }
            foreach (var item in chkNameAllList.Items.Cast<ListItem>())
            {
                if (item.Value == "0")
                    item.Text = "Select first " + GRIDITEMSSELECTED.ToString() + " active and retired displayed hits";
                if (item.Value == "1")
                    item.Text = "Select all closed matter items in the first " + GRIDITEMSSELECTED.ToString() + " hits";
            }
            foreach (var item in chkPendingNameAllList.Items.Cast<ListItem>())
            {
                if (item.Value == "0")
                    item.Text = "Select first " + GRIDITEMSSELECTED.ToString() + " displayed hits";
            }


            //handle for CELPC Review
            //default DisplayClearedHits==false, hide pnlMid

            if (Request.QueryString["usage"] != null)
            {
                USAGE = Request.QueryString["usage"];
                if (USAGE.ToLower() == "review")
                {
                    //JJP 4/19/13 per Sue C. default Display Cleared Hits false
                    //JJP 4/23/2013 per Sue C. default Display Cleared Hits back to true 
                    cbDisplayHits.Checked = true;
                    cbDisplayHits.Visible = true;
                    chkClientAllList.Visible = false;
                    imgClientAllList.Visible = false;
                    chkNameAllList.Visible = false;
                    imgNameAllList.Visible = false;
                    chkPendingNameAllList.Visible = false;
                    imgPendingNameAllList.Visible = false;
                    tc1.Tabs[(int)TAB_NUMBER.Email].Visible = false;
                    pnlMid.Visible = false;
                    cbDisplayHits_CheckedChanged(this, e);
                }
            }
            else
            {
                USAGE = "update";
                SetSignOff(); //JJP
            }

            Label l = Master.FindControl("lblHeader") as Label;
            l.Text = "Online Conflict Report";

            SetConflictReportURL();
            //JJP 08/28/2013 populate header info
            SetHeaderInfo();

        }

        else//postback
        {
            
        }

        if (FID != string.Empty)
        {
            SetCounts();
            if (HITCOUNT == 0)//JJP 05/01/2013 No Hits found requiring signoff
            {
                pnlTop.Visible = false;
                pnlMid.Visible = false;
                tc1.Tabs[(int)TAB_NUMBER.Adverse].Visible = false;
                tc1.Tabs[(int)TAB_NUMBER.Client_Related].Visible = false;
                tc1.Tabs[(int)TAB_NUMBER.Email].Visible = false;
                tc1.Tabs[(int)TAB_NUMBER.Opposite].Visible = false;
                pnlNoHitsToClear.Visible = true;
                lblNoHitsToClear.Text = "No Hits found that need to be cleared for folder id: " + FID;
            }

        }
    }

    protected void SetConflictReportURL()
    {
        
        HyperLink hl = (HyperLink)Master.FindControl("hlPrint");
        hl.NavigateUrl = WebConfigurationManager.AppSettings["ConflictReportLocation"].ToString() + "&rs:Command=Render&fid=" + FID + "&partypkid=" + ddlPKId.SelectedValue.ToString();
        hl.Visible = true;

        ImageButton ib = (ImageButton)Master.FindControl("ibHelp");
        ib.CausesValidation = true;
        ib.OnClientClick = "window.open('/Help/ClearHitsMainHelp.html','name','height=700, width=900,toolbar=no,directories=no,status=no, menubar=no,scrollbars=yes,resizable=no'); return false;";

    }

    protected void SetHeaderInfo()
    {
        IList<string> al = new List<String>();
        al = DAL.GetHeaderInfo(FID);

        Label l1 = Master.FindControl("lbl1") as Label;
        Label l2 = Master.FindControl("lbl2") as Label;
        Label l3 = Master.FindControl("lbl3") as Label;
        Label l4 = Master.FindControl("lbl4") as Label;
        Label lh = Master.FindControl("lblHeader") as Label;

        l1.Text = al[0];
        l2.Text = al[1];
        l3.Text = al[2];
        l4.Text = al[3];
        lh.Text = lh.Text + " - Folder Name: " + al[4];

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        
        //handle no items to clear
        if (DAL.IsComplete(FID) && !this.cbDisplayHits.Checked)
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('There are currently no items to clear.  Please close this window and continue with the New Business Intake Workflow application.');", true);

            return;

        }

        if (ddlSignOff.SelectedValue == "")//required
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('Sign-Off reason must be selected.');", true);
            //keep selected item backcolor
            cbSelectItem_CheckedChanged(this, e);
            return;
        }


        //handle required comments
        if (DAL.IsRequired(ddlSignOff.SelectedValue) && String.IsNullOrEmpty(txtSignOffComments.Text))
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('Sign-Off reason must be selected.');", true);
            //keep selected item backcolor
            cbSelectItem_CheckedChanged(this, e);
            return;
        }

        //identify selected tab

        if(tc1.ActiveTab.ID=="tpClient")
        {
            if (!IsItemSelected(ref dlClientNameHits))
            {
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('At least one hit must be selected.');", true);
                return;
            }

            Update(ref dlClientNameHits);
            dlClientNameHits.DataBind();
            dlClientHitCount.DataBind();
        }
        else if (tc1.ActiveTab.ID == "tpName")
        {
            if (!IsItemSelected(ref dlNameHits))
            {
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('At least one hit must be selected.');", true);
                return;
            }

            Update(ref dlNameHits);
            dlNameHits.DataBind();
            dlNameHitCount.DataBind();
        }
        else if (tc1.ActiveTab.ID == "tpPendingName")
        {
            if (!IsItemSelected(ref dlPendingHits))
            {
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('At least one hit must be selected.');", true);
                return;
            }

            Update(ref dlPendingHits);
            dlPendingHits.DataBind();
            dlPendingNameHitCount.DataBind();
        }

        txtSignOffComments.Text = string.Empty;
        dlTotalHitCount.DataBind();
        SetCounts();
    }

    protected bool IsItemSelected(ref DataList _dl)
    {
        bool b = false;
        for (int i = 0; i < _dl.Items.Count; i++)
        {
            CheckBox cb = (CheckBox)_dl.Items[i].FindControl("cbSelectItem");
            if (cb != null)
            {
                if (cb.Checked)
                {
                    b = true;
                    break;
                }
            }
        }
        return b;
    }

    protected void Update(ref DataList _dl)
    {
        for (int i = 0; i < _dl.Items.Count; i++)
        {
            Label l = (Label)_dl.Items[i].FindControl("lblID");
            CheckBox cb = (CheckBox)_dl.Items[i].FindControl("cbSelectItem");
            
            if (l != null && cb != null)
            {
                if (cb.Checked)
                {
                    string s = ddlSignOff.SelectedItem.Text + " " + txtSignOffComments.Text;

                    //JP add some kind of comment if not required
                    if (string.IsNullOrEmpty(s))
                        s = ddlSignOff.SelectedItem.Text;

                    DAL.ClearHits(ddlSignOff.SelectedValue, Convert.ToInt32(l.Text), s);

                    //JJP 08/22/2013 call sp in MS
                    DAL.FlagHitsInMS(FID);
                }
            }
        }
        chkClientAllList.ClearSelection();
        chkNameAllList.ClearSelection();
        chkPendingNameAllList.ClearSelection();

        if (DAL.IsComplete(FID))
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('ALL CONFLICTS CLEARED\\nExit this form by selecting the red X on the top right');", true);
        return;
    }

    protected void ddlPKId_SelectedIndexChanged(object sender, EventArgs e)
    {
        PKID = ddlPKId.SelectedValue;
        SetCounts();
    }
    protected void ddlMRPCRP_SelectedIndexChanged(object sender, EventArgs e)
    {
        MRPCRP = ddlMRPCRP.SelectedValue;//JJP 01/29/2013
        SetCounts();
    }
    protected void cbDisplayHits_CheckedChanged(object sender, EventArgs e)
    {
        this.hfClientSignOff.Value = (cbDisplayHits.Checked ? "0" : "1");
        
        this.hfNameSignOff.Value = (cbDisplayHits.Checked ? "0" : "1");
        
        this.hfPendingNameSignOff.Value = (cbDisplayHits.Checked ? "0" : "1");
        
        SetSignOff();

        //not needed here
        //SetCounts();
    }
    protected void SetSignOff()
    {
        //set ddlSignOff
        ddlSignOff.Items.Clear();
        ddlSignOff.DataBind();
        if (this.cbDisplayHits.Checked)//show cleared items, give unclear option
        {
            this.ddlSignOff.Items.Insert(0, new ListItem("UnClear", "-99"));
            ddlSignOff.SelectedValue = "-99";
        }
        else//show uncleared items, give signoff option
        {
            ddlSignOff.Items.Insert(0, new ListItem("Select Sign-Off reason", ""));
            ddlSignOff.SelectedValue = "";
        }

    }

    private void SetCounts()
    {
        
        IList<string> al = new List<string>();
        al = DAL.GetHitCount(FID, Convert.ToInt32(PKID),Convert.ToInt32(MRPCRP));//JJP 01/29/2013 ddlMRPCRP.SelectedValue));
        int _c = 0;//JJP 05/01/2013

        if (al.Count == 13)//JJP 05/01/2013
        {
            AjaxControlToolkit.TabContainer tc = (AjaxControlToolkit.TabContainer)tc1;
            foreach (object obj in tc.Controls)
            {
                if (obj is AjaxControlToolkit.TabPanel)
                {
                    AjaxControlToolkit.TabPanel tabPanel = (AjaxControlToolkit.TabPanel)obj;

                    if (tabPanel.ID == "tpClient")
                        tabPanel.HeaderText = "Client Related Hits (" + al[0] + ")(" + al[3] + ")";
                    else if (tabPanel.ID == "tpName")
                        tabPanel.HeaderText = "Adverse Hits/Warnings (" + al[1] + ")(" + al[4] + ")";
                    else if (tabPanel.ID == "tpPendingName")
                        tabPanel.HeaderText = "Opposite Pending Hits (" + al[2] + ")(" + al[5] + ")";
                }
            }
            _c = Convert.ToInt32(al[12]);//JJP 05/01/2013
        }
        HITCOUNT= _c;
    }

    //following will refire the selected tabs' checkchanged event
    protected void tc1_ActiveTabChanged(object sender, EventArgs e)
    {

        //disabled autopostback when tab changed

        /*
        if (tc1.ActiveTabIndex == 0 && chkClientAll.Checked)
            chkClientAll_CheckedChanged(this, new EventArgs());
        else if (tc1.ActiveTabIndex == 1 && chkNameAll.Checked)
            chkNameAll_CheckedChanged(this, new EventArgs());
        else if (tc1.ActiveTabIndex == 2 && chkPendingNameAll.Checked)
            chkPendingNameAll_CheckedChanged(this, new EventArgs());
       */
        //do not show panels if email tab selected
        //pnlTop.Visible = tc1.ActiveTabIndex != ((int)TAB_NUMBER.Email);
        //pnlMid.Visible = tc1.ActiveTabIndex != ((int)TAB_NUMBER.Email);
    }

    #region tpClient

    protected void dsClientHits_OnSelected(object sender, SqlDataSourceStatusEventArgs e)
    {
        chkClientAllList.Visible = (e.AffectedRows > 1 &&  USAGE.ToLower() != "review");
        imgClientAllList.Visible = chkClientAllList.Visible;
        lbldlClientNamesHitsNoData.Visible = (e.AffectedRows == 0);
        if (e.AffectedRows == 0)
        {
            lbldlClientNamesHitsNoData.Visible = true;
            lbldlClientNamesHitsNoData.Text = (cbDisplayHits.Checked ? "No Cleared Client Hit Data Found" :
                                                                        "No UnCleared Client Hit Data Found");
        }
    }

    protected void chkClientAllList_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = GRIDITEMSSELECTED;

        if (chkClientAllList.SelectedValue == string.Empty)//all hits unchecked
        {
            foreach (DataListItem li in dlClientNameHits.Items)
            {
                if (i == 0)
                    return;

                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked = false;

                i -= 1;
            }
        }
        else if (chkClientAllList.SelectedValue == "0")//all hits
        {
            foreach (DataListItem li in dlClientNameHits.Items)
            {
                if (i == 0)
                    return;

                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked = true;// chkClientAll.Checked;
                if (cb.Checked)
                    li.BackColor = System.Drawing.Color.Salmon;

                i -= 1;
            }
        }
        else if (chkClientAllList.SelectedValue == "1")//only hits with closed matters
        {
            foreach (DataListItem li in dlClientNameHits.Items)
            {
                if (i == 0)
                    return;

                Label l = (Label)li.FindControl("lblCloseDt");

                if (!String.IsNullOrEmpty(l.Text))
                {
                    CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                    cb.Checked = true;
                    if (cb.Checked)
                        li.BackColor = System.Drawing.Color.Salmon;

                    i -= 1;
                }
            }
        }

        i = 0;
    }

    protected void dlClientNameHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        HiddenField hf = (HiddenField)e.Item.FindControl("hfClientNameSignOffCode");
        
        //Set visibility for Client Party Resolution
        Label l = (Label)e.Item.FindControl("lblResolution");
        if (hf != null)
            l.Visible = !hf.Value.Equals(string.Empty);


        //hide/display link if client

        l = (Label)e.Item.FindControl("lblHitSource");
        HyperLink hl = (HyperLink)e.Item.FindControl("hlClient");
        Label ll = (Label)e.Item.FindControl("lblClientName");
        if (l != null && hl != null && ll != null)
        {
            hl.Visible=(l.Text=="Client");
            ll.Visible = (l.Text == "Name");
        }

        //hide checkbox if usage=review
        if (USAGE.ToLower() == "review")
        {
            CheckBox cb = (CheckBox)e.Item.FindControl("cbSelectItem");
            if(cb != null)
                cb.Visible = false;
        }

    }


    //individual checkbox in Client Party Data List
    protected void cbSelectItem_CheckedChanged(object sender, EventArgs e)
    {
        /*
        var selected = dlClientNameHits.Items.Cast<DataListItem>()
        .Where(li => ((CheckBox)li.FindControl("cbClientPartySignOff")).Checked)
        .Select(li => li);
        */
        
        //set backcolor of selected items

        if (tc1.ActiveTab.ID == "tpClient")
        {
            foreach (DataListItem li in dlClientNameHits.Items)
            {
                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                if (cb.Checked)
                    li.BackColor = System.Drawing.Color.Salmon;
            }
        }
        else if (tc1.ActiveTab.ID == "tpName")
        {
            foreach (DataListItem li in dlNameHits.Items)
            {
                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                if (cb.Checked)
                    li.BackColor = System.Drawing.Color.Salmon;
            }
        }
        else if (tc1.ActiveTab.ID == "tpPendingName")
        {
            foreach (DataListItem li in dlPendingHits.Items)
            {
                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                if (cb.Checked)
                    li.BackColor = System.Drawing.Color.Salmon;
            }
        }

    }
    #endregion tpClient

    #region ClientGrid
    protected void gvClientHits_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void gvClientHits_DataBound(object sender, EventArgs e)
    {
        if (gvClientHits.Rows.Count > 0)
        {
            gvClientHits.Columns[14].Visible = cbDisplayHits.Checked;
        }

    }

    

    protected void lbComments_Click(object sender, EventArgs e)
    {
        
        LinkButton lb = (LinkButton)sender;
        GridViewRow row = (GridViewRow)lb.NamingContainer;
        if (row != null)
        {
            int index = row.RowIndex; //gets the row index selected

            string sql = "SELECT comments FROM nbi_cms_conflict_hits WHERE hitautoid = " +
           gvClientHits.DataKeys[row.RowIndex].Value;

            fvComments.DataSource = DAL.GetDataTable(sql, "MetastormConnectionString");
            fvComments.DataBind();
            ModalPopupExtender1.Show();

        }
    }
    #endregion

    #region tpName

    protected void dsNameHits_OnSelected(object sender, SqlDataSourceStatusEventArgs e)
    {
        chkNameAllList.Visible = (e.AffectedRows > 1 && USAGE.ToLower() != "review");
        imgNameAllList.Visible = chkNameAllList.Visible;
        lbldlNameHitsNoData.Visible = (e.AffectedRows == 0);
        if (e.AffectedRows == 0)
        {
            lbldlNameHitsNoData.Visible = true;
            lbldlNameHitsNoData.Text = (cbDisplayHits.Checked ? "No Cleared Party Hit Data Found" :
                                                            "No UnCleared Party Hit Data Found");
        }
    }

    protected void chkNameAllList_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = GRIDITEMSSELECTED;

        if (chkNameAllList.SelectedValue == string.Empty)//all hits unchecked
        {
            foreach (DataListItem li in dlNameHits.Items)
            {
                if (i == 0)
                    return;

                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked = false;

                i -= 1;
            }
        }
        else if (chkNameAllList.SelectedValue == "0")//all hits
        {
            foreach (DataListItem li in dlNameHits.Items)
            {
                if (i == 0)
                    return;

                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked = true;// chkClientAll.Checked;
                if (cb.Checked)
                    li.BackColor = System.Drawing.Color.Salmon;

                i -= 1;
            }
        }
        else if (chkNameAllList.SelectedValue == "1")//only hits with closed matters
        {
            foreach (DataListItem li in dlNameHits.Items)
            {
                if (i == 0)
                    return;

                Label l = (Label)li.FindControl("lblCloseDt");

                if (!String.IsNullOrEmpty(l.Text))
                {
                    CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                    cb.Checked = true;
                    if (cb.Checked)
                        li.BackColor = System.Drawing.Color.Salmon;

                    i -= 1;
                }
            }
        }

        i = 0;
    }

    protected void dlNameHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {

        //Set visibility for Client Party Resolution
        HiddenField hf = (HiddenField)e.Item.FindControl("hfSignOffCode");
        Label l = (Label)e.Item.FindControl("lblResolution");
        if (hf != null)
            l.Visible = !hf.Value.Equals(string.Empty);

        if (dlNameHits.Items.Count > 0)
            lbldlNameHitsNoData.Visible = false;
        else
            lbldlNameHitsNoData.Visible = true;

        //hide checkbox if usage=review
        if (USAGE.ToLower() == "review")
        {
            CheckBox cb = (CheckBox)e.Item.FindControl("cbSelectIteml");
            if (cb != null)
                cb.Visible = false;
        }
    }

    #endregion

    #region tpPendingName

    protected void dsPendingNameHits_OnSelected(object sender, SqlDataSourceStatusEventArgs e)
    {
        chkPendingNameAllList.Visible = (e.AffectedRows > 1 && USAGE.ToLower() != "review");
        imgPendingNameAllList.Visible = chkPendingNameAllList.Visible;
        lbldlPendingHitsNoData.Visible = (e.AffectedRows == 0);
        if (e.AffectedRows == 0)
        {
            lbldlPendingHitsNoData.Visible = true;
            lbldlPendingHitsNoData.Text = (cbDisplayHits.Checked ? "No Cleared Pending Hit Data Found" :
                                                                    "No UnCleared Pending Hit Data Found");
        }
    }

    protected void chkPendingNameAllList_SelectedIndexChanged(object sender, EventArgs e)
    {
        int i = GRIDITEMSSELECTED;

        if (chkPendingNameAllList.SelectedValue == string.Empty)//all hits unchecked
        {
            foreach (DataListItem li in dlPendingHits.Items)
            {
                if (i == 0)
                    return;

                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked = false;

                i -= 1;
            }
        }
        else if (chkPendingNameAllList.SelectedValue == "0")//all hits
        {
            foreach (DataListItem li in dlPendingHits.Items)
            {
                if (i == 0)
                    return;

                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked = true;// chkClientAll.Checked;
                if (cb.Checked)
                    li.BackColor = System.Drawing.Color.Salmon;

                i -= 1;
            }
        }
        else if (chkPendingNameAllList.SelectedValue == "1")//only hits with closed matters
        {
            foreach (DataListItem li in dlPendingHits.Items)
            {
                if (i == 0)
                    return;

                Label l = (Label)li.FindControl("lblCloseDt");

                if (!String.IsNullOrEmpty(l.Text))
                {
                    CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                    cb.Checked = true;
                    if (cb.Checked)
                        li.BackColor = System.Drawing.Color.Salmon;

                    i -= 1;
                }
            }
        }

        i = 0;
    }

    protected void dlPendingHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        
        if (dlPendingHits.Items.Count > 0)
            lbldlPendingHitsNoData.Visible = false;
        else
            lbldlPendingHitsNoData.Visible = true;

        HiddenField hf = (HiddenField)e.Item.FindControl("hfSignOffCodeb");

        //Set visibility for Client Party Resolution
        Label l = (Label)e.Item.FindControl("lblResolution");
        if (hf != null)
            l.Visible = !hf.Value.Equals(string.Empty);

        //hide checkbox if usage=review
        if (USAGE.ToLower() == "review")
        {
            CheckBox cb = (CheckBox)e.Item.FindControl("cbSelectItem");
            if (cb != null)
                cb.Visible = false;
        }
    }
    #endregion

    #region NoClear

    //set to Bold for Clients
    protected void gvRelatedNameHits_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow )
        {
            Label l = (Label)e.Row.Cells[5].FindControl("lblDomIsClient");
            if (l.Text == "Y")
            {
                Label ldnh = (Label)e.Row.Cells[1].FindControl("lblDomHitName");
                ldnh.Font.Bold = true;
            }


            l = (Label)e.Row.Cells[4].FindControl("lblIsClient");
            if (l.Text == "Y")
            {
                Label lnh = (Label)e.Row.Cells[3].FindControl("lblHitName");
                lnh.Font.Bold = true;
            }
        }
    }
    protected void gvNameLinkHits_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
    }
    protected void gvRetMatters4_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    #endregion

    #region tpEmail

    protected void gvEmailSent_DataBound(object sender, EventArgs e)
    {
        lblEmailLog.Visible = (gvEmailSent.Rows.Count > 0);
    }

    protected void ddlEmailMRPCRP_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlEmailPKId.Items.Clear();
        ddlEmailPKId.DataBind();

        //JJP 07/11/2013 default "send to" 
        
        ListItem _li = ddlEmailRecipient.Items.FindByText(ddlEmailMRPCRP.SelectedItem.Text);
        int _x = 0;
        if (int.TryParse(ddlEmailMRPCRP.SelectedValue, out _x))
        {
            if (Convert.ToInt32(ddlEmailMRPCRP.SelectedValue) > 0 && _li != null)
            {
                ddlEmailRecipient.SelectedIndex=
                    ddlEmailRecipient.Items.IndexOf(
                            ddlEmailRecipient.Items.FindByText(ddlEmailMRPCRP.SelectedItem.Text));
  
                //ddlEmailRecipient.SelectedItem.Text = ddlEmailMRPCRP.SelectedItem.Text;
                //AuthenticatedUser au = DAL.GetAuthenticatedUser(ddlEmailMRPCRP.SelectedValue);
                //ddlEmailRecipient.SelectedItem.Value = au.EmailAddress;  
            }
        } 
        
    }
    protected void ddlEmailPKId_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (ddlEmailMRPCRP.SelectedItem.Text == "" || 
            this.ddlEmailMessage.SelectedItem.Text +  this.txtMessage.Text == string.Empty ||
            ddlEmailPKId.SelectedItem.Text == "" || ddlEmailRecipient.SelectedItem.Text == "")//required
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('CRP/MRP, Email Recipient, Message and Party Name are all required and must be selected.');", true);
            return;
        }
        //JJP 04/23/2013 add url from config file
        string _url = System.Configuration.ConfigurationManager.AppSettings["EmailURL"].ToString();
        lblURL.Text = _url + this.FID + "&partypkid=" +
            this.ddlEmailPKId.SelectedValue + "&mrpcrp=" + ddlEmailMRPCRP.SelectedValue + "&secure=true";

        String s = System.Web.HttpContext.Current.User.Identity.Name;
        UserProfile up = UserProfile.GetUserProfile(s);
        //JJP 05/03/2013 add dropdown to return email address recipient
        //AuthenticatedUser au = DAL.GetAuthenticatedUser(ddlEmailMRPCRP.SelectedValue);

        string msg = "<b>" + this.ddlEmailMessage.SelectedItem.Text + " " + this.txtMessage.Text + "</b><br/><br/>" + "<a href=" + lblURL.Text + 
            ">Click to go to Report</a> " + 
            "<br/><br/><b>Once completed, please reply to me via this e-mail or call me directly.</b>";

        //JJP 05/03/2013 add dropdown to return email address recipient
        //if (sendEmail(up.EmailAddress, au.EmailAddress, this.ddlEmailSubject.SelectedItem.Text, msg))   
        if (sendEmail(up.EmailAddress.TrimEnd(), ddlEmailRecipient.SelectedValue, this.ddlEmailSubject.SelectedItem.Text, msg))
            if (DAL.LogEmailRecord(FID, up.ProperName, ddlEmailRecipient.SelectedItem.Text, ddlEmailPKId.SelectedItem.Text, ddlEmailMRPCRP.SelectedItem.Text))
            {
                dsEmailSent.DataBind();
                gvEmailSent.DataBind();

                //notify user that sending email is complete
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('Your email has been sent successfully.');", true);
                //execute proc to update hit status
                //if(!DAL.FireHitsCompleteProc(FID))
                    //SingletonLogger.Instance.Error("Error calling DAL.FireHitsCompleteProc for Folder: " + FID.ToString());
            }
            else
                SingletonLogger.Instance.Error("Email not logged");
        else
            SingletonLogger.Instance.Error("Email not sent.  From Address: " + up.EmailAddress + " Recipient: " + ddlEmailRecipient.SelectedValue);

        //JJP 07/17/2013 Clear all checkboxes in case user selected a checkbox
        UnCheckItems();
                
    }

    private void UnCheckItems()
    {

            foreach (DataListItem li in dlClientNameHits.Items)
            {
                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked=false;
            }

            foreach (DataListItem li in dlNameHits.Items)
            {
                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked = false;
            }

            foreach (DataListItem li in dlPendingHits.Items)
            {
                CheckBox cb = (CheckBox)li.FindControl("cbSelectItem");
                cb.Checked = false;
            }

    }
    private bool sendEmail(string _from, string _to, string _subject, string _body)
    {
        MailMessage message = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();

        try
        {
            MailAddress fromAddress = new MailAddress(_from);
            message.From = fromAddress;

            string _s = System.Configuration.ConfigurationManager.AppSettings["UseDevEmailAddressTo"].ToString();
            if (_s == "true")
            {
                message.To.Add(System.Configuration.ConfigurationManager.AppSettings["DEVEmailAddressTo"].ToString());
            }
            else
            {
                message.Bcc.Add(System.Configuration.ConfigurationManager.AppSettings["EmailAddressTo"].ToString());
                message.To.Add(_to);
            }

           
            message.Subject = _subject;
            message.IsBodyHtml = true;
            message.Body = _body;
            message.IsBodyHtml=true;
            smtpClient.Host = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
            smtpClient.Port = 25;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["SMTPLoginName"], System.Configuration.ConfigurationManager.AppSettings["SMTPLoginPwd"]);

            smtpClient.Send(message);
            return true;
        }
        catch (Exception ex)
        {
            SingletonLogger.Instance.Error(ex.Message);
            return false;
        }
    }


    protected void ddlEmailPKId_DataBound(object sender, EventArgs e)
    {
        if (ddlEmailPKId.Items.Count > 1)
        {
            ddlEmailPKId.Items.Insert(0,new ListItem("Select Party Name","0"));
            ddlEmailPKId.Items.Insert(1,new ListItem("All Party Name Hits for " + ddlEmailMRPCRP.SelectedItem.Text, "-99"));
            ddlEmailPKId.SelectedIndex = 0;
        }
    }

    protected void ddlEmailMessage_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (!ddlEmailMessage.SelectedValue.Equals("0"))
            //this.txtMessage.Text = ddlEmailMessage.SelectedItem.Text;
        //else
            //this.txtMessage.Text = string.Empty;
    }


    #endregion

    protected void scriptmanager_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        if (e.Exception.Data["ExtraInfo"] != null)
        {
            scriptmanager.AsyncPostBackErrorMessage =
                e.Exception.Message +
                e.Exception.Data["ExtraInfo"].ToString();
        }
        else
        {
            scriptmanager.AsyncPostBackErrorMessage =
                "An unspecified error occurred.";
        }

    }
}
public enum TAB_NUMBER : int {Client_Related=0,Adverse,Opposite,No_Signoff,Email};