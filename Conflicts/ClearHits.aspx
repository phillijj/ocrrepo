﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ClearHits.aspx.cs" Inherits="Conflicts_ClearHits"  Theme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="scriptmanager" runat="server"></ajaxToolkit:ToolkitScriptManager>

    <asp:HiddenField ID="FID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="PKID" runat="server" EnableViewState="true" />

    <asp:Panel ID="pnlTop" runat="server" 
        BorderStyle="Solid" 
        BorderWidth="1px" 
        BorderColor="Gray" 
        BackColor="#ffffaa">

        <label><b>Select Matter Party:</b></label>
        <asp:DropDownList ID="ddlPKId" runat="server" 
            DataSourceID="dsPKId" 
            DataTextField="originalname" 
            DataValueField="partypkid" 
            AppendDataBoundItems="true" 
            AutoPostBack="true"
            onselectedindexchanged="ddlPKId_SelectedIndexChanged" 
            BackColor="#dfdfdf" 
            Font-Bold="true" 
            ForeColor="Blue">
            <asp:ListItem Selected="True" Text="Select Party Name" Value="0"></asp:ListItem>
            <asp:ListItem Selected="False" Text="All Party Names Searched" Value="-99"></asp:ListItem>
        </asp:DropDownList>

        <asp:SqlDataSource ID="dsPKId" runat="server" 
            ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
            SelectCommand="select partypkid,originalname from NBI_CMS_Conflict_Sessions where fid = @fid">
            <SelectParameters>
                <asp:QueryStringParameter Name="fid" QueryStringField="fid" ConvertEmptyStringToNull="true" />
            </SelectParameters>
        </asp:SqlDataSource>

    </asp:Panel>


    
    

    <ajaxToolkit:TabContainer ID="tc1" runat="server" Font-Bold="true"  
        onactivetabchanged="tc1_ActiveTabChanged">
        
        <ajaxToolkit:TabPanel ID="NewClient" runat="server" HeaderText="NewClient">
            <ContentTemplate>
                
                    <asp:DataList runat="server" ID="dlNewClientHits" 
                            DataSourceID="dsNewClientHits" 
                            GridLines="Horizontal"
                            CssClass="myGrid" 
                            RepeatDirection="Horizontal" 
                            RepeatColumns="2"
                            CellPadding="2" 
                            CellSpacing="2" 
                            Width="98%"
                            OnItemDataBound="dlNewClientHits_ItemDataBound" 
                            ondatabinding="dlNewClientHits_DataBinding" 
                        onselectedindexchanged="dlNewClientHits_SelectedIndexChanged">
                        <HeaderTemplate>
                            <label>Client (Code) Name</label>
                        </HeaderTemplate>
                        <ItemStyle BorderColor="Black" BorderStyle="Dotted" />
                        <ItemTemplate>
                            
                            <asp:Label runat="server" ID="lblPK" 
                                Visible="false" 
                                Text='<%#Eval("hitautoid")%>'>
                            </asp:Label>

                            <asp:Label runat="server" ID="lblPartyPKId" 
                                Visible="false" 
                                Text='<%#Eval("partypkid")%>'>
                            </asp:Label>

                            <asp:Label ID="lblNBIPartyName" runat="server"
                                Font-Bold="true" 
                                Font-Size="12px" 
                                Font-Underline="false"
                                ForeColor="Blue"  
                                Text='<%#Eval("NBIPartyName") %>'>
                            </asp:Label> 

                        </ItemTemplate>
                        <AlternatingItemStyle BackColor="#DDDDDD"/>
                        </asp:DataList>
 
                

               <asp:SqlDataSource ID="dsNewClientHits" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_clienthitdetail" 
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:Parameter 
                            DbType="Int16" 
                            DefaultValue="1"
                            Name="signoff" />
                        <asp:ControlParameter 
                            ControlID="ddlPKId" 
                            DefaultValue="" 
                            Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>


            </ContentTemplate>
            
        </ajaxToolkit:TabPanel>
                       
        <ajaxToolkit:TabPanel ID="tpClient" runat="server" HeaderText="Client Related" Height="100%">
            
            <ContentTemplate>
                    
                <h4>Clients</h4>

                <table width="99%">
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="cbClientAll" runat="server" 
                                CausesValidation="True" 
                                AutoPostBack="True"
                                Text="Display Cleared Client Hits" 
                                OnCheckedChanged="cbClientAll_CheckedChanged" />
                        </td>
                        <td>
                            &nbsp&nbsp
                        </td>
                        <td align="right">
                            <asp:DataList ID="dlClientHitCount" runat="server" DataSourceID="dsClientHitCount">
                                <ItemTemplate>
                                    Remaining Hits:
                                    <asp:Label ID="lblToBeClearedHits" runat="server" 
                                        CssClass="redBold" 
                                        Text='<%#Eval("tobeclearedhits")%>'>
                                    </asp:Label>&#160;&#160;
                                    Total Hits:
                                    <asp:Label ID="lblTotalHits" runat="server" 
                                        CssClass="redBold" 
                                        Text='<%#Eval("totalhits")%>'>
                                    </asp:Label>
                                 </ItemTemplate>
                            
                            </asp:DataList>
                        </td>
                    </tr>
                </table>

                <asp:Label runat="server" ID="lbldlClientHitsNoData"
                    Font-Bold="False" 
                    Font-Size="Smaller" 
                    ForeColor="Gray"  
                    Visible="False" 
                    Text="No Client Hit Data Found"></asp:Label>


                <asp:GridView ID="gvClientHits" runat="server" 
                    AllowPaging="True" 
                    AllowSorting="True"
                    DataSourceID="dsClientHits" 
                    DataKeyNames="FID" 
                    AutoGenerateColumns="False" 
                    SkinID="MyGrid"
                    PageSize="20" 
                    ShowFooter="True" 
                    OnDataBound="gvClientHits_DataBound" 
                    OnRowDataBound="gvClientHits_RowDataBound">

                    <Columns>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField Visible="False" DataField="PartyPKID" />
                        <asp:BoundField ReadOnly="True" HeaderText="Client (Code) Name" 
                            DataField="NBIPartyName"
                            SortExpression="NBIPartyName" />
                        <asp:BoundField ReadOnly="True" HeaderText="Affiliation" 
                            DataField="nbiAffiliation"
                            SortExpression="nbiaffiliation" />
                        
                        <asp:TemplateField SortExpression="cms_hit_name" HeaderText='Hit Name'>
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server"
                                    NavigateUrl='<%# "Conflicts_DD.aspx?uno=" + Eval("cms_uno") + "&source=client"  + "&name=" + 
                        HttpUtility.UrlEncode(Eval("nbipartyname").ToString()) + "&autoid=" + Eval("hitautoid") + "&fid=" + Eval("fid") + "&signoff=" + Eval("SignOffCode") + "&partypkid=" + Eval("PartyPKID") + "&ddlpartypkid=" + ddlPKId.SelectedValue + "&activetab=" + hfActiveTab.Value  %>'
                                     Target="_self" 
                                     Text='<%#Eval("cms_hit_name")%>' />
                             </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:BoundField ReadOnly="True" HeaderText="CRP" DataField="cms_crp_mrp" 
                            SortExpression="cms_crp_mrp" />

                        <asp:TemplateField SortExpression="cms_hit_source" HeaderText="Source">
                            <ItemTemplate>
                                <asp:Label ID="lblHitSource" runat="server" 
                                    Text='<%#Eval("cms_hit_source")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField SortExpression="txtReasonDesc" HeaderText="Resolution"  
                            Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblResolution" runat="server" Text='<%#Eval("resolution")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="Select">
                            <FooterTemplate>
                                <asp:Button ID="btnClientUpdate" runat="server" 
                                    Text="Update" 
                                    Visible="true" 
                                    OnClick="btnClientUpdate_Click" />
                            </FooterTemplate>
                            <HeaderTemplate>
                                <asp:DropDownList ID="ddlSignOff" runat="server" 
                                    OnDataBound="ddlSignOff_ItemDataBound" 
                                    DataSourceID="objClearReason" 
                                    DataValueField="txtReasonCode" 
                                    DataTextField="txtReasonDesc">
                                    <asp:ListItem Selected="True" Text="Select Sign-Off Reason" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </HeaderTemplate>
                            
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="cbClientSignOff" />
                            </ItemTemplate>
                            <FooterStyle CssClass="submitButton" />
                        </asp:TemplateField>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblCMS_Uno" runat="server" Text='<%#Eval("cms_uno")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>          

      

  
                    <h4>Client Parties</h4>

                    <table width="99%">
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="cbClientNameAll" runat="server" CausesValidation="True"
                                    AutoPostBack="True" Text="Display Cleared Client Party Hits" 
                                    OnCheckedChanged="cbClientNameAll_CheckedChanged" />
                            </td>
                            <td>
                                &nbsp&nbsp
                            </td>
                            <td align="right">
                                <asp:DataList ID="dlClientNameHitCount" runat="server" 
                                    DataSourceID="dsClientNameHitCount">
                                    <ItemTemplate>
                                        Remaining Hits:
                                        <asp:Label ID="lblClientToBeClearedHits" runat="server" 
                                            CssClass="redBold" 
                                            Text='<%#Eval("tobeclearedhits")%>'>
                                        </asp:Label>&#160;&#160;
                                        Total Hits:
                                        <asp:Label ID="lblClientTotalHits" runat="server" 
                                            CssClass="redBold" 
                                            Text='<%#Eval("totalhits")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddlClientPartySignOff" runat="server"                         
                                    OnDataBound="ddlClientPartySignOff_ItemDataBound" 
                                    DataSourceID="objClearReason" 
                                    DataValueField="txtReasonCode" 
                                    DataTextField="txtReasonDesc">
                                    <asp:ListItem Selected="True" Text="Select Sign-Off Reason"></asp:ListItem>
                                </asp:DropDownList>

                                <asp:Button runat="server" ID="btnClientNameUpdate" 
                                    Text="Update" 
                                    OnClick="btnClientNameUpdate_Click"
                                    Visible="False" />
                            </td>
                        </tr>
                    </table>

                    <asp:Label runat="server" ID="lbldlClientNamesHitsNoData"
                            Font-Bold="False" 
                            Font-Size="Smaller" 
                            ForeColor="Gray"  
                            Visible="False" 
                            Text="No Client Party Hit Data Found"></asp:Label>

                
                    <asp:Panel runat="server" ID="pnlCNH" ScrollBars="Vertical" Height="500px">

                    <asp:DataList runat="server" ID="dlClientNameHits" 
                            DataSourceID="dsClientNameHits" 
                            GridLines="Horizontal"
                            CssClass="myGrid" 
                            RepeatDirection="Horizontal" 
                            RepeatColumns="2"
                            CellPadding="2" 
                            CellSpacing="2" 
                            Width="98%"
                            OnItemDataBound="dlClientNameHits_ItemDataBound" 
                            ondatabinding="dlClientNameHits_DataBinding" 
                        onselectedindexchanged="dlClientNameHits_SelectedIndexChanged">
                        <HeaderTemplate>
                        
                        </HeaderTemplate>
                        <ItemStyle BorderColor="Black" BorderStyle="Dotted" />
                        <ItemTemplate>
                            <asp:HiddenField ID="hfClientNameSignOffCode" runat="server" 
                                Value='<%#Eval("signoffcode") %>'>
                            </asp:HiddenField>


                            <asp:Label ID="lblClientPartyStatus" runat="server"
                                Font-Bold="true" 
                                Font-Size="12px" 
                                Font-Underline="false"
                                ForeColor="Blue"  
                                Text='<%#Eval("cms_party_status_desc") %>'>
                            </asp:Label>
                            <br>
                            <asp:Label ID="lblResolution" runat="server"
                                ForeColor="Green"
                                Font-Bold="true" 
                                Font-Size="12px"
                                Text='<%#"Resolution: " + Eval("resolution") %>' >
                            </asp:Label>
                            <br>
                            <asp:CheckBox runat="server" ID="cbClientPartySignOff" 
                                Text="Select" AutoPostBack="true" 
                                oncheckedchanged="cbClientPartySignOff_CheckedChanged" />
                            <asp:CheckBox ID="cb1" runat="server"
                                Visible="false" 
                                Enabled="false"  
                                Text="Cleared" 
                                Checked='<%#Eval("needssignoff").ToString().Equals("0") %>' />
                             <asp:Label ID="lblID" runat="server" 
                                Visible="false" 
                                Text='<%#Eval("hitautoid").ToString() %>'>
                             </asp:Label>
                             <br>
                            <b>Party: </b>
                            <asp:Label ID="lblPN" runat="server" 
                                Text='<%#Eval("cms_party_name") %>' 
                                Font-Bold="true" 
                                ForeColor='Red'>
                            </asp:Label>
                            <br>
                            <b>Client: </b>
                            <asp:Label ID="lblCN" runat="server" 
                                Text='<%#Eval("cms_client_name") %>'>
                            </asp:Label>
                            <br>
                            <b>Matter: </b>
                            <asp:Label ID="lblMN" runat="server" 
                                Text='<%#Eval("cms_matter_name") %>'>
                            </asp:Label><br>
                            <b>Client.Matter: </b>
                            <asp:Label ID="lblCMC" runat="server" 
                                Text='<%#Eval("cms_clnt_matt_code") %>'>
                            </asp:Label><br>
                            <b>Open Date: </b>
                            <asp:Label ID="lblOpenDt" runat="server" 
                                Text='<%#Eval("cms_open_date") %>'>
                            </asp:Label>
                            <b>Date Added: </b>
                            <asp:Label ID="lblDtAdded" runat="server" 
                                Text='<%#Eval("cms_date_added") %>'>
                            </asp:Label>
                            <br>
                            <b>Last Worked: </b>
                            <asp:Label ID="lblLastWorkDt" runat="server" 
                                Text='<%#Eval("cms_last_worked") %>'>
                            </asp:Label>
                            <b>Close Date: </b>
                            <asp:Label ID="lblCloseDt" runat="server" 
                                Text='<%#Eval("cms_close_date") %>'>
                            </asp:Label>
                            <br>
                            <b>MRP: </b>
                            <asp:Label ID="lblMRP" runat="server" 
                                Text='<%#Eval("cms_crp_mrp") %>'>
                            </asp:Label>
                            <b>BA:</b>
                            <asp:Label ID="lblBA" runat="server" 
                                Text='<%#Eval("cms_ba") %>'>
                            </asp:Label>
                            <br>
                            <b>Comments:</b>
                            <asp:Label ID="Label1" runat="server" 
                                Text='<%#Eval("cms_comments") %>'>
                            </asp:Label>
                            <br>
                        
                            <asp:DropDownList ID="ddlClientNameSignOff" runat="server" 
                                        ondatabound="ddlClientNameSignOff_DataBound" 
                                        DataSourceID="objClearReason" 
                                        DataValueField="txtReasonCode" 
                                        DataTextField="txtReasonDesc"
                                        Visible="false">
                            </asp:DropDownList>
                        
                            <asp:TextBox ID="txtClientPartySignOffComments" runat="server" 
                                    Width="95%" 
                                    TextMode="MultiLine"
                                    Wrap="true"
                                    Rows="2"
                                    Text='<%#Eval("comments") %>'>
                            </asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="WatermarkControl" 
                                        runat="server"  
                                        WatermarkCssClass="watermark"
                                        TargetControlID="txtClientPartySignOffComments"  
                                        WatermarkText="Enter comments here ..."></ajaxToolkit:TextBoxWatermarkExtender>
                            <br>
                        </ItemTemplate>
                        <AlternatingItemStyle BackColor="#DDDDDD" />
                        <FooterTemplate>
                        </FooterTemplate>
                    </asp:DataList>
  
                    </asp:Panel>
                

                <asp:SqlDataSource ID="dsClientHits" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_clienthitdetail" 
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter 
                            ControlID="hfClientSignOff" 
                            DbType="Int16" 
                            DefaultValue="1"
                            Name="signoff" />
                        <asp:ControlParameter 
                            ControlID="ddlPKId" 
                            DefaultValue="" 
                            Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>
               

                <asp:SqlDataSource ID="dsClientHitCount" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommandType="StoredProcedure" 
                    SelectCommand="prc_qsel_ClientHitCount">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:ObjectDataSource ID="objClearReason" runat="server" 
                    SelectMethod="GetReasonCodes" 
                    TypeName="DAL">
                </asp:ObjectDataSource>
                
                <asp:HiddenField ID="hfClientSignOff" runat="server" Value="1" />
                    

                
                <asp:SqlDataSource ID="dsClientNameHits" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_ClientNameHitDetail" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="hfClientNameSignOff" DbType="Int16" DefaultValue="1" Name="signoff" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="dsClientNameHitCount" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_ClientNameHitCount" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="hfClientNameSignOff" runat="server" Value="1" />

            </ContentTemplate>

            

        </ajaxToolkit:TabPanel>


        <ajaxToolkit:TabPanel ID="tpName" runat="server" HeaderText="Party Hits">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="cbNameAll" runat="server" Checked="false" CausesValidation="true"
                                AutoPostBack="true" Text="Display Cleared Party Hits" OnCheckedChanged="cbNameAll_CheckedChanged" />
                        </td>
                        <td>
                            &nbsp&nbsp
                        </td>
                        <td align="right">
                            <asp:DataList ID="dlNameHitCount" runat="server" DataSourceID="dsNameHitCount">
                                <ItemTemplate>
                                    Remaining Hits:
                                    <asp:Label ID="lblToBeClearedHits" runat="server" CssClass="redBold" Text='<%#Eval("tobeclearedhits")%>'></asp:Label>&#160;&#160;
                                    Total Hits:
                                    <asp:Label ID="lblTotalHits" runat="server" CssClass="redBold" Text='<%#Eval("totalhits")%>'></asp:Label></ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
                <asp:Label runat="server" Font-Bold="false" Font-Size="Smaller" ForeColor="Gray" ID="lbldlNameHitsNoData" Visible="false" Text="No Party Hit Data Found"></asp:Label>


                <asp:DataList runat="server" ID="dlNameHits" DataSourceID="dsNameHits" GridLines="Horizontal"
                    CssClass="myGrid" RepeatLayout="Table" RepeatDirection="Horizontal" RepeatColumns="3"
                    CellPadding="2" CellSpacing="2" AlternatingItemStyle-BackColor="#DDDDDDDD" ItemStyle-BorderColor="Black"
                    ItemStyle-BorderStyle="Dotted" OnItemDataBound="dlNameHits_ItemDataBound">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hfSignOffCode" runat="server" Value='<%#Eval("signoffcode") %>'>
                        </asp:HiddenField>
                        <asp:Label ID="lblPartyStatus" Font-Bold="true" Font-Size="12px" Font-Underline="false"
                            ForeColor="Blue" runat="server" Text='<%#Eval("cms_party_status_desc") %>'></asp:Label><br>
                        <asp:CheckBox Visible="false" Enabled="false" ID="cb1" runat="server" Text="Cleared" Checked='<%#Eval("needssignoff").ToString().Equals("0") %>' /><asp:Label
                            ID="lblID" runat="server" Visible="false" Text='<%#Eval("hitautoid").ToString() %>'></asp:Label><br>
                        <b>Party: </b>
                        <asp:Label ID="lblPN" runat="server" Text='<%#Eval("cms_party_name") %>'  Font-Bold="true" ForeColor='Red'></asp:Label><br>
                        <b>Client: </b>
                        <asp:Label ID="lblCN" runat="server" Text='<%#Eval("cms_client_name") %>'></asp:Label><br>
                        <b>Matter: </b>
                        <asp:Label ID="lblMN" runat="server" Text='<%#Eval("cms_matter_name") %>'></asp:Label><br>
                        <b>Client.Matter: </b>
                        <asp:Label ID="lblCMC" runat="server" Text='<%#Eval("cms_clnt_matt_code") %>'></asp:Label><br>
                        <b>Open Date: </b>
                        <asp:Label ID="lblOpenDt" runat="server" Text='<%#Eval("cms_open_date") %>'></asp:Label><b>Date
                            Added: </b>
                        <asp:Label ID="lblDtAdded" runat="server" Text='<%#Eval("cms_date_added") %>'></asp:Label><br>
                        <b>Last Worked: </b>
                        <asp:Label ID="lblLastWorkDt" runat="server" Text='<%#Eval("cms_last_worked") %>'></asp:Label><b>Close
                            Date: </b>
                        <asp:Label ID="lblCloseDt" runat="server" Text='<%#Eval("cms_close_date") %>'></asp:Label><br>
                        <b>MRP: </b>
                        <asp:Label ID="lblMRP" runat="server" Text='<%#Eval("cms_crp_mrp") %>'></asp:Label><b>BA:
                        </b>
                        <asp:Label ID="lblBA" runat="server" Text='<%#Eval("cms_ba") %>'></asp:Label>
                        <br>
                        <b>Comments:</b>
                        <asp:Label ID="Label1" runat="server" 
                            Text='<%#Eval("cms_comments") %>'>
                        </asp:Label><br>
                        <asp:DropDownList ID="ddlSignOff" runat="server" DataSourceID="objClearReason2" DataValueField="txtReasonCode" DataTextField="txtReasonDesc">
                        </asp:DropDownList>
                        <br>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:DataList><asp:Button runat="server" ID="btnNameUpdate" Text="Update" OnClick="btnNameUpdate_Click"
                    Visible="false" />
                    <br />

                
                <asp:SqlDataSource ID="dsNameHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_NameHitDetail" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="hfNameSignOff" DbType="Int16" DefaultValue="1" Name="signoff" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>
                
                <asp:SqlDataSource ID="dsNameHitCount" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_NameHitCount" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>
                
                <asp:ObjectDataSource ID="objClearReason2" runat="server" SelectMethod="GetReasonCodes" TypeName="DAL"></asp:ObjectDataSource>

                <asp:HiddenField ID="hfNameSignOff" runat="server" Value="1" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>


        <ajaxToolkit:TabPanel ID="tpPendingName" HeaderText="Pending Names" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="cbPendingNameAll" runat="server" CausesValidation="True" AutoPostBack="True"
                                Text="Display Cleared Pending Hits" OnCheckedChanged="cbPendingNameAll_CheckedChanged" />
                        </td>
                        <td>
                            &nbsp&nbsp
                        </td>
                        <td align="right">
                            <asp:DataList ID="dlPendingNameHitCount" runat="server" DataSourceID="dsPendingNameHitCount">
                                <ItemTemplate>
                                    Remaining Hits:
                                    <asp:Label ID="lblToBeClearedHits" runat="server" CssClass="redBold" Text='<%#Eval("tobeclearedhits")%>'></asp:Label>&#160;&#160;
                                    Total Hits:
                                    <asp:Label ID="lblTotalHits" runat="server" CssClass="redBold" Text='<%#Eval("totalhits")%>'></asp:Label></ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
                <asp:Label runat="server" Font-Bold="false" Font-Size="Smaller" ForeColor="Gray" ID="lbldlPendingHitsNoData" Visible="false" Text="No Pending Hit Data Found"></asp:Label>
                <asp:DataList runat="server" ID="dlPendingHits" DataSourceID="dsPendingHits" GridLines="Horizontal"
                    CssClass="myGrid" RepeatLayout="Table" RepeatDirection="Horizontal" RepeatColumns="3"
                    CellPadding="2" CellSpacing="2" AlternatingItemStyle-BackColor="#DDDDDDDD" ItemStyle-BorderColor="Black"
                    ItemStyle-BorderStyle="Dotted" OnItemDataBound="dlPendingHits_ItemDataBound">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hfSignOffCodeb" runat="server" Value='<%#Eval("signoffcode") %>'>
                        </asp:HiddenField>
                        <asp:Label ID="lblPartyStatus" Font-Bold="true" Font-Size="12px" Font-Underline="false"
                            ForeColor="Blue" runat="server" Text='<%#Eval("cms_party_status_desc") %>'></asp:Label><br>
                        <asp:CheckBox  Visible="false" Enabled="false" ID="cb1b" runat="server" Text="Cleared" Checked='<%#Eval("needssignoff").ToString().Equals("0") %>' /><asp:Label
                            ID="lblIDb" runat="server" Visible="false" Text='<%#Eval("hitautoid").ToString() %>'></asp:Label><br>
                        <b>Pending Party: </b>
                        <asp:Label ID="lblPNb" runat="server" Text='<%#Eval("cms_party_name") %>' Font-Bold="true" ForeColor='Red'></asp:Label><br>
                        <b>Client: </b>
                        <asp:Label ID="lblCNb" runat="server" Text='<%#Eval("cms_client_name") %>'></asp:Label><br>
                        <b>Pending Matter: </b>
                        <asp:Label ID="lblMNb" runat="server" Text='<%#Eval("cms_matter_name") %>'></asp:Label><br>
                        <b>Client.Matter: </b>
                        <asp:Label ID="lblCMCb" runat="server" Text='<%#Eval("cms_clnt_matt_code") %>'></asp:Label><br>
                        <b>Status: </b>
                        <asp:Label ID="lblHTb" runat="server" Text='<%#Eval("hittype") %>'></asp:Label><br>
                        <b>Req Atty: </b>
                        <asp:Label ID="lblRAb" runat="server" Text='<%#Eval("cms_crp_mrp") %>'></asp:Label><br>
                        <asp:DropDownList ID="ddlSignOffb" runat="server" DataSourceID="objClearReason3" DataValueField="txtReasonCode" DataTextField="txtReasonDesc">
                        </asp:DropDownList>
                        <br>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:DataList><asp:Button runat="server" ID="btnPendingNameUpdate" Text="Update"
                    OnClick="btnPendingNameUpdate_Click" Visible="false" />
                    
                    
                
                <asp:SqlDataSource ID="dsPendingHits"
                        runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_PendingNameHitDetail" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                            <asp:ControlParameter ControlID="hfPendingNameSignOff" DbType="Int16" DefaultValue="1"
                                Name="signoff" />
                            <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                        </SelectParameters>
                    </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsPendingNameHitCount" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_PendingNameHitCount" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:ObjectDataSource ID="objClearReason3" runat="server" SelectMethod="GetReasonCodes" TypeName="DAL"></asp:ObjectDataSource>

                <asp:HiddenField ID="hfPendingNameSignOff" runat="server" Value="1" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>


        <ajaxToolkit:TabPanel ID="tpNoClear" HeaderText="No SignOff Required" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlNCClients" runat="server">
                <h3>Client Hits Not To Be Cleared</h3>
                <asp:GridView ID="gvTotalClientHits" runat="server" DataSourceID="dsTotalClientHits"
                    DataKeyNames="hitautoid" AutoGenerateColumns="False" SkinID="MyGrid" PageSize="20"
                    ShowFooter="True" AllowPaging="True" AllowSorting="True" EmptyDataText="No Client Data Found">
                    <Columns>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField Visible="False" DataField="PartyPKID" />
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblCMS_Uno" runat="server" Text='<%#Eval("cms_uno")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        

                        <asp:TemplateField SortExpression="cms_client_name" HeaderText='Client (Code) Name'>
                         <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# "Conflicts_DD.aspx?uno=" + Eval("cms_uno") + "&source=ROclient"  + "&name=" + 
                        HttpUtility.UrlEncode(Eval("cms_client_name").ToString()) + "&fid=" + Eval("fid") + "&signoff=" + Eval("SignOffCode") + "&partypkid=" + Eval("PartyPKID") + "&ddlpartypkid=" + ddlPKId.SelectedValue + "&activetab=3"  %>'
                                    runat="server" Target="_self" Text='<%#Eval("cms_client_name")%>' />
                           </ItemTemplate>
                         </asp:TemplateField>


                        
                        <asp:BoundField ReadOnly="True" DataField="needssignoff" SortExpression="needssignoff"
                            HeaderText="Sign Off Req'd" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                         
                        
                        <asp:BoundField ReadOnly="True" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="CRP" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField ReadOnly="True" DataField="cms_ba" SortExpression="cms_ba" 
                            HeaderText="Billing Atty" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField ReadOnly="True" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
               
            </asp:Panel>
            
            <asp:Panel ID="pnlNCPartyHits" runat="server">
                <h3>Party Hits Not To Be Cleared</h3>
                <asp:GridView ID="gvNameClearedHits" runat="server" DataSourceID="dsNameClearedHits"
                    DataKeyNames="hitautoid" AutoGenerateColumns="false" SkinID="MyGrid" PageSize="20"
                    ShowFooter="true" ShowHeader="true" AllowPaging="true" AllowSorting="true" EmptyDataText="No Party Hit Data Found">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ReadOnly="true" DataField="needssignoff" SortExpression="needssignoff"
                            HeaderText="Sign Off Req'd" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_status_desc" SortExpression="cms_party_status_desc"
                            HeaderText="Party Status" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_clnt_matt_code" SortExpression="cms_clnt_matt_code"
                            HeaderText="Client.Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_name" SortExpression="cms_party_name"
                            HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_client_name" SortExpression="cms_client_name"
                            HeaderText="Client" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_matter_name" SortExpression="cms_matter_name"
                            HeaderText="Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="MRP" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_ba" SortExpression="cms_ba" HeaderText="Billing Atty"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                </asp:GridView>
                
            </asp:Panel>

            <asp:Panel ID="pnlNCPendingHits" runat="server">
                <h3>Pending Client Hits Not To Be Cleared</h3>
                <asp:GridView ID="gvTotalPendingClientHits" runat="server" DataSourceID="dsTotalPendingClientHits"
                    DataKeyNames="hitautoid" AutoGenerateColumns="false" SkinID="MyGrid" PageSize="20"
                    ShowFooter="true" ShowHeader="true" AllowPaging="true" AllowSorting="true" EmptyDataText="No Pending Data Found">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ReadOnly="true" DataField="needssignoff" SortExpression="needssignoff"
                            HeaderText="Sign Off Req'd" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_status_desc" SortExpression="cms_party_status_desc"
                            HeaderText="Party Status" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_clnt_matt_code" SortExpression="cms_clnt_matt_code"
                            HeaderText="Client.Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_name" SortExpression="cms_party_name"
                            HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_client_name" SortExpression="cms_client_name"
                            HeaderText="Client" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_matter_name" SortExpression="cms_matter_name"
                            HeaderText="Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="MRP" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_ba" SortExpression="cms_ba" HeaderText="Billing Atty"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                </asp:GridView>
                <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;">
                </div>
            </asp:Panel>


                <asp:SqlDataSource ID="dsTotalClientHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:Parameter Name="cms_hit_source" DefaultValue="Client" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="dsNameClearedHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:Parameter Name="cms_hit_source" DefaultValue="Name" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="dsTotalPendingClientHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
		                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
		                    <SelectParameters>
		                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
		                        <asp:Parameter Name="cms_hit_source" DefaultValue="Pending" />
                                <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
		                    </SelectParameters>
                </asp:SqlDataSource>
            </ContentTemplate>


        </ajaxToolkit:TabPanel>

        
    </ajaxToolkit:TabContainer>

    <asp:HiddenField ID="hfActiveTab" runat="server" Value="0" />
</asp:Content>


