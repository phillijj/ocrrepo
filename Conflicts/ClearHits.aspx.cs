﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Conflicts_ClearHits : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["fid"] == null)
                Response.Redirect("http://dpnbi/conflicts/timeout.aspx");
            FID.Value = Request.QueryString["fid"];
            if (Request.QueryString["partypkid"] == null)
                PKID.Value = "-99";
            else
                PKID.Value = Request.QueryString["partypkid"];

            ddlPKId.DataBind();
            ddlPKId.SelectedValue = PKID.Value;

            if (FID.Value != string.Empty)
                SetCounts();
        }

      

        HyperLink hl = (HyperLink)Master.FindControl("hlPrint");
        hl.NavigateUrl = WebConfigurationManager.AppSettings["ConflictReportLocation"].ToString() + "&rs:Command=Render&fid=" + FID.Value;
        hl.Visible = true;
        Label l = Master.FindControl("lblHeader") as Label;
        l.Text = "Day Pitney Online Conflict Report";

        if (Request.QueryString["activetab"] != null)
            tc1.ActiveTabIndex = Convert.ToInt16(Request.QueryString["activetab"]);
        else
            tc1.ActiveTabIndex = Convert.ToInt16(hfActiveTab.Value);


        
    }

    protected void ddlPKId_SelectedIndexChanged(object sender, EventArgs e)
    {
        PKID.Value = ddlPKId.SelectedValue;
        SetCounts();
    }

    private void SetCounts()
    {
        IList<string> al = new List<string>();
        al = DAL.GetHitCount(FID.Value, Convert.ToInt32(PKID.Value), null);

        if (al.Count == 12)
        {
            AjaxControlToolkit.TabContainer tc = (AjaxControlToolkit.TabContainer)tc1;
            foreach (object obj in tc.Controls)
            {
                if (obj is AjaxControlToolkit.TabPanel)
                {
                    AjaxControlToolkit.TabPanel tabPanel = (AjaxControlToolkit.TabPanel)obj;

                    if (tabPanel.ID == "tpClient")
                    {
                        tabPanel.HeaderText = "Client Related (" + al[0] + ")(" + al[3] + ")";
                        if (gvClientHits.FooterRow != null)
                        {
                            Button b = (Button)gvClientHits.FooterRow.FindControl("btnClientUpdate");
                            if (b != null)
                            {
                                b.Visible = true;
                                if (cbClientAll.Checked && al[3].ToString() == "0")
                                    b.Visible = false;
                                else if (!cbClientAll.Checked && al[0].ToString() == "0")
                                    b.Visible = false;
                            }
                        }
                        //Client Party Hits dl added
                        btnClientNameUpdate.Visible = true;
                        if (!cbClientNameAll.Checked && al[6].ToString() == "0")
                            btnClientNameUpdate.Visible = false;
                        else if (cbClientNameAll.Checked && al[7].ToString() == "0")
                            btnClientNameUpdate.Visible = false;   

                    }
                    else if (tabPanel.ID == "tpName")
                    {
                        tabPanel.HeaderText = "Party Hits (" + al[1] + ")(" + al[4] + ")";

                        btnNameUpdate.Visible = true;
                        if (cbNameAll.Checked && al[4].ToString() == "0")
                            btnNameUpdate.Visible = false;
                        else if (!cbNameAll.Checked && al[1].ToString() == "0")
                            btnNameUpdate.Visible = false;
                    }
                    else if (tabPanel.ID == "tpPendingName")
                    {
                        tabPanel.HeaderText = "Pending Names (" + al[2] + ")(" + al[5] + ")";

                        btnPendingNameUpdate.Visible = true;
                        if (cbPendingNameAll.Checked && al[5].ToString() == "0")
                            btnPendingNameUpdate.Visible = false;
                        else if (!cbPendingNameAll.Checked && al[2].ToString() == "0")
                            btnPendingNameUpdate.Visible = false;

                    }
                }
            }

            //set checkbox visibility

            /*
            cbClientAll.Visible = (!cbClientAll.Checked && (Convert.ToInt16(al[9]) > 0) ||
                 cbClientAll.Checked && (Convert.ToInt16(al[8]) > 0)); //tobecleared > 0 or cleared > 0

            cbClientNameAll.Visible = false;
            cbClientNameAll.Visible = (!cbClientNameAll.Checked && (Convert.ToInt16(al[11]) > 0) ||
                 cbClientNameAll.Checked && (Convert.ToInt16(al[10]) > 0)); //tobecleared > 0 or cleared > 0

            cbNameAll.Visible = false;
            cbNameAll.Visible = (!cbNameAll.Checked && (Convert.ToInt16(al[4]) > 0) ||
                 cbNameAll.Checked && (Convert.ToInt16(al[1]) > 0)); //tobecleared > 0 or cleared > 0

            cbPendingNameAll.Visible = false;
            cbPendingNameAll.Visible = (!cbPendingNameAll.Checked && (Convert.ToInt16(al[5]) > 0) ||
                 cbPendingNameAll.Checked && (Convert.ToInt16(al[2]) > 0)); //tobecleared > 0 or cleared > 0
             */
        }
    }

    #region Updates
    protected void btnClientUpdate_Click(object sender, EventArgs e)
    {
        Boolean updated = true;
        DropDownList ddl = (DropDownList)gvClientHits.HeaderRow.FindControl("ddlSignOff");
        string d = ddl.SelectedValue;

        if (!cbClientAll.Checked && d == string.Empty)//do not allow clearing on uncleared items
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('Sign-Off reason must be selected.');", true);
            return;
        }

        foreach (GridViewRow gvr in gvClientHits.Rows)
        {
            CheckBox cb = (CheckBox)gvr.FindControl("cbClientSignOff");
            if (cb.Checked)
            {
                Label l = (Label)gvr.FindControl("lblPK");
                
                bool b = DAL.ClearHits(d, Convert.ToInt32(l.Text),"");
                if (updated == true)//only set if updated is true, once false it is kept
                    updated = b;
            }
        }
        if (updated)
        {
            gvClientHits.DataBind();
            dlClientHitCount.DataBind();
            SetCounts();
        }

    }
    protected void btnNameUpdate_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < dlNameHits.Items.Count; i++)
        {
            Label l = (Label)dlNameHits.Items[i].FindControl("lblID");
            DropDownList ddl = (DropDownList)dlNameHits.Items[i].FindControl("ddlSignOff");
            HiddenField hf = (HiddenField)dlNameHits.Items[i].FindControl("hfSignOffCode");

            if (l != null && ddl != null)
            {
                if (!(ddl.SelectedValue == hf.Value))
                {
                    DAL.ClearHits(ddl.SelectedValue, Convert.ToInt32(l.Text),"");
                }
            }
        }

        dlNameHitCount.DataBind();
        dlNameHits.DataBind();
        SetCounts();
    }

    protected void btnClientNameUpdate_Click(object sender, EventArgs e)
    {
        //code to handle dropdown list embedded inside datalist
        /*
        for (int i = 0; i < dlClientNameHits.Items.Count; i++)
        {
            Label l = (Label)dlClientNameHits.Items[i].FindControl("lblID");
            DropDownList ddl = (DropDownList)dlClientNameHits.Items[i].FindControl("ddlClientNameSignOff");
            HiddenField hf = (HiddenField)dlClientNameHits.Items[i].FindControl("hfClientNameSignOffCode");

            if (l != null && ddl != null)
            {
                if (!(ddl.SelectedValue == hf.Value))
                {
                    DAL.ClearHits(ddl.SelectedValue, Convert.ToInt32(l.Text));
                }
            }
        }
        */

        //code to handle single dropdown list and individual checkboxes

        if (!cbClientNameAll.Checked && ddlClientPartySignOff.SelectedValue == string.Empty)//do not allow clearing on uncleared items
        {
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "modify", "alert('Sign-Off Reason must be selected.');", true);
            return;
        }

        for (int i = 0; i < dlClientNameHits.Items.Count; i++)
        {
            Label l = (Label)dlClientNameHits.Items[i].FindControl("lblID");
            CheckBox cb = (CheckBox)dlClientNameHits.Items[i].FindControl("cbClientPartySignOff");
            TextBox t = (TextBox)dlClientNameHits.Items[i].FindControl("txtClientPartySignOffComments");
            if (l != null && cb != null)
            {
                if(cb.Checked)
                    DAL.ClearHits(ddlClientPartySignOff.SelectedValue, Convert.ToInt32(l.Text),t.Text);
            }
        }


        dlClientNameHitCount.DataBind();
        dlClientNameHits.DataBind();
        SetCounts();
    }

    protected void btnPendingNameUpdate_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < dlPendingHits.Items.Count; i++)
        {
            Label l = (Label)dlPendingHits.Items[i].FindControl("lblIDb");
            DropDownList ddl = (DropDownList)dlPendingHits.Items[i].FindControl("ddlSignOffb");
            HiddenField hf = (HiddenField)dlPendingHits.Items[i].FindControl("hfSignOffCodeb");

            if (l != null && ddl != null)
            {
                if (!(ddl.SelectedValue == hf.Value))
                {
                    DAL.ClearHits(ddl.SelectedValue, Convert.ToInt32(l.Text),"");
                }
            }
        }

        dlPendingNameHitCount.DataBind();
        dlPendingHits.DataBind();
        SetCounts();

    }

    #endregion

    #region CheckChanged
    protected void cbClientAll_CheckedChanged(object sender, EventArgs e)
    {
        this.hfClientSignOff.Value = (cbClientAll.Checked ? "0" : "1");
        lbldlClientHitsNoData.Text = (cbClientAll.Checked ? "No Cleared Client Hit Data Found" : "No UnCleared Client Hit Data Found");
        SetCounts();
    }
    protected void cbNameAll_CheckedChanged(object sender, EventArgs e)
    {
        this.hfNameSignOff.Value = (cbNameAll.Checked ? "0" : "1");
        lbldlNameHitsNoData.Text = (cbNameAll.Checked ? "No Cleared Party Hit Data Found" : "No UnCleared Party Hit Data Found");
        SetCounts();
    }
    protected void cbPendingNameAll_CheckedChanged(object sender, EventArgs e)
    {
        this.hfPendingNameSignOff.Value = (cbPendingNameAll.Checked ? "0" : "1");
        this.lbldlPendingHitsNoData.Text = (cbPendingNameAll.Checked ? "No Cleared Pending Hit Data Found" : "No UnCleared Pending Hit Data Found");
        SetCounts();
    }

    protected void cbClientNameAll_CheckedChanged(object sender, EventArgs e)
    {
        this.hfClientNameSignOff.Value = (cbClientNameAll.Checked ? "0" : "1");
        this.lbldlClientNamesHitsNoData.Text = (cbClientNameAll.Checked ? "No Cleared Client Party Hit Data Found" : "No UnCleared Client Party Hit Data Found");
        
        SetCounts();
    }

    //individual checkbox in Client Party Data List
    protected void cbClientPartySignOff_CheckedChanged(object sender, EventArgs e)
    {
        /*
        var selected = dlClientNameHits.Items.Cast<DataListItem>()
        .Where(li => ((CheckBox)li.FindControl("cbClientPartySignOff")).Checked)
        .Select(li => li);
        */

        //set backcolor of selected items
        foreach (DataListItem li in dlClientNameHits.Items)
        {
            CheckBox cb = (CheckBox)li.FindControl("cbClientPartySignOff");
            if (cb.Checked)
                li.BackColor = System.Drawing.Color.Salmon;
        }

    }

    #endregion

    #region FillDropDownListsInsideDataObjects
    protected void dlNameHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        HiddenField hf = (HiddenField)e.Item.FindControl("hfSignOffCode");
        DropDownList dl = (DropDownList)e.Item.FindControl("ddlSignOff");
        if (hf != null && dl != null)
        {
            FillDropDownLists(ref dl, hf.Value);
        }

        if (dlNameHits.Items.Count > 0)
            lbldlNameHitsNoData.Visible = false;
        else
            lbldlNameHitsNoData.Visible = true;
    }
    protected void dlPendingHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        HiddenField hf = (HiddenField)e.Item.FindControl("hfSignOffCodeb");
        DropDownList dl = (DropDownList)e.Item.FindControl("ddlSignOffb");

        if (hf != null && dl != null)
        {
            FillDropDownLists(ref dl, hf.Value);
        }

        if (dlPendingHits.Items.Count > 0)
            lbldlPendingHitsNoData.Visible = false;
        else
            lbldlPendingHitsNoData.Visible = true;
    }

    protected void dlClientNameHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        HiddenField hf = (HiddenField)e.Item.FindControl("hfClientNameSignOffCode");
        DropDownList dl = (DropDownList)e.Item.FindControl("ddlClientNameSignOff");
        if (hf != null && dl != null)
        {
            FillDropDownLists(ref dl, hf.Value);
        }

        //Set visibility for Client Party Resolution
        Label l = (Label)e.Item.FindControl("lblResolution");
        if(hf != null)
            l.Visible = !hf.Value.Equals(string.Empty);
                
        //Set visibility for Client Party No Data Label
        if (dlClientNameHits.Items.Count > 0)
            lbldlClientNamesHitsNoData.Visible = false;
        else
            lbldlClientNamesHitsNoData.Visible = true;

        //set visibility of Client Name Hits Dropdown
        ddlClientPartySignOff.Visible = (dlClientNameHits.Items.Count > 0);

    }

    private void FillDropDownLists(ref DropDownList _dl, string _hf)
    {
        _dl.Items.Clear();

       _dl.DataBind();

        if (_hf != "")
        {
            _dl.Items.Add(new ListItem("UnClear", ""));
            _dl.SelectedValue = _hf;
        }
        else
        {
            _dl.Items.Add(new ListItem("Select Sign-Off reason", ""));
            _dl.SelectedValue = "";
        }
    }

    #endregion

    #region DataBinding
    protected void gvClientHits_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            DropDownList dl = (DropDownList)e.Row.FindControl("ddlSignOff");
            if (dl != null)
            {
                dl.Items.Clear();
                dl.DataBind();
                if (cbClientAll.Checked)//show cleared items, give unclear option
                {
                    dl.Items.Insert(0, new ListItem("UnClear", ""));
                }
                else//show uncleared items, give signoff option
                {
                    dl.Items.Insert(0, new ListItem("Select Sign-Off reason", ""));
                }

                
                dl.SelectedValue = "";
            }
        }

    }
    
    protected void ddlClientNameSignOff_DataBound(object sender, EventArgs e)
    {

    }
    protected void ddlSignOff_ItemDataBound(object sender, EventArgs e)
    {

    }
    protected void gvClientHits_DataBound(object sender, EventArgs e)
    {
        if (gvClientHits.Rows.Count > 0)
        {
            gvClientHits.Columns[7].Visible = cbClientAll.Checked;
            lbldlClientHitsNoData.Visible = false;
        }
        else
            lbldlClientHitsNoData.Visible = true;
    }


    protected void ddlClientPartySignOff_ItemDataBound(object sender, EventArgs e)
    {

    }

    #endregion

    protected void dlClientNameHits_DataBinding(object sender, EventArgs e)
    {
        //ddlClientPartySignOff
        ddlClientPartySignOff.Items.Clear();
        ddlClientPartySignOff.DataBind();
        if (cbClientNameAll.Checked)//show cleared items, give unclear option
        {
            ddlClientPartySignOff.Items.Insert(0, new ListItem("UnClear", ""));
        }
        else//show uncleared items, give signoff option
        {
            ddlClientPartySignOff.Items.Insert(0, new ListItem("Select Sign-Off reason", ""));
        }


        ddlClientPartySignOff.SelectedValue = "";

        
    }
    protected void dlClientNameHits_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void tc1_ActiveTabChanged(object sender, EventArgs e)
    {
        hfActiveTab.Value = tc1.ActiveTabIndex.ToString();
        
    }
    protected void dlNewClientHits_DataBinding(object sender, EventArgs e)
    {

    }
    protected void dlNewClientHits_ItemDataBound(object sender, DataListItemEventArgs e)
    {

    }
    protected void dlNewClientHits_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}