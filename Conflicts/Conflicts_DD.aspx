﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Conflicts_DD.aspx.cs" Inherits="Conflicts_DD"  Theme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<ajaxToolkit:ToolkitScriptManager ID="scriptmanager" runat="server"></ajaxToolkit:ToolkitScriptManager>
<br /><h2>
<asp:Label ID="lblClient" runat="server" CssClass="Bold" ForeColor="#005f9c" Font-Names="Verdana"></asp:Label>
<br />
<asp:Label ID="lblStatus" runat="server" Font-Bold="true" ForeColor="Red" Font-Names="Verdana" ></asp:Label>
</h2>
<br/>
    <asp:LinkButton runat="server" Text="Back to Hits" ID="lbBack"></asp:LinkButton>
<br />

<asp:Panel ID="pnlMatters" runat="server" Visible="false">
<h3>Matters</h3>
        <asp:GridView ID="gvMatters" runat="server"  DataSourceID="dsMatters" AutoGenerateColumns="false" SkinID="MyGrid" EmptyDataText="No Matters found"  AllowPaging="true" PageSize="25" AllowSorting="true">
        <HeaderStyle CssClass="myGridHeader" />
            <Columns>
    	        <asp:BoundField ReadOnly="True" HeaderText="Code" DataField="Matter_Code" SortExpression="matter_code" />
                <asp:BoundField ReadOnly="True" HeaderText="Name" DataField="Matter_Name" SortExpression="matter_name" />
                <asp:BoundField ReadOnly="True" HeaderText="Open" DataField="Open_Date" DataFormatString="{0:MM/dd/yyyy}" SortExpression="open_date"/>
                <asp:BoundField ReadOnly="True" HeaderText="Close" DataField="Close_Date" DataFormatString="{0:MM/dd/yyyy}" SortExpression="close_date"/>
                <asp:BoundField ReadOnly="True" HeaderText="MRP" DataField="mrp" SortExpression="crp" />
                <asp:BoundField ReadOnly="True" HeaderText="BA" DataField="ba" SortExpression="ba" />
                <asp:BoundField ReadOnly="True" HeaderText="Last Work" DataField="last_work_date" DataFormatString="{0:MM/dd/yyyy}" SortExpression="last_work_date"/>
          </Columns>
        </asp:GridView>
</asp:Panel>


<asp:Panel ID="pnlRelatedNames" runat="server">
 <h3>Related Names</h3>
        <asp:GridView ID="gvRelatedNames" runat="server" DataSourceID="dsRelatedNames" AutoGenerateColumns="false" SkinID="MyGrid" EmptyDataText="No Related Names found" AllowSorting="true">
        <HeaderStyle CssClass="myGridHeader" />
            <Columns>
                <asp:BoundField ReadOnly="True" HeaderText="Relationship" DataField="desc" SortExpression="desc" />
            </Columns>
        </asp:GridView>

</asp:Panel>

<!--
<h3>Client Contacts</h3>
        <asp:GridView ID="gvClientContacts" runat="server" 
            DataSourceID="dsClientContacts" AutoGenerateColumns="false" SkinID="MyGrid" 
            EmptyDataText="No Client Contacts found" 
            onrowcreated="gvClientContacts_RowCreated" AllowSorting="true">
            <HeaderStyle CssClass="myGridHeader" />
            <Columns>
         <asp:TemplateField ItemStyle-Width="40" ItemStyle-HorizontalAlign="Right">
         <ItemTemplate>
            <asp:Image ID="imgMagnify" runat="server" ImageUrl="~/Pictures/magnify.gif" />
            
             <ajaxToolkit:PopupControlExtender ID="extend1" runat="server"
                PopupControlID="pnlClientContacts"
               TargetControlID="imgMagnify"
               DynamicContextKey='<%# Eval("name_uno") %>'
               DynamicControlID="pnlClientContacts"
               DynamicServiceMethod="GetDynamicContent" Position="Bottom">
             </ajaxToolkit:PopupControlExtender>

         </ItemTemplate>
      </asp:TemplateField> 
                <asp:BoundField ReadOnly="True" HeaderText="Name" DataField="name" SortExpression="name"  />
                <asp:BoundField ReadOnly="True" HeaderText="Contact Type" DataField="cont_type_desc"  />
            </Columns>
        </asp:GridView>
        <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;"> </div>
        <asp:Panel ID="pnlClientContacts" runat="server" BorderColor="#060F40" BorderWidth="2px" BorderStyle="Solid"
         BackColor="#ffffcc" ForeColor="#060F40" Font-Size="Smaller">
         </asp:Panel>  
-->



<asp:Panel ID="pnlPartyHits" runat="server" Visible="false">
    <h3>Party Hits</h3>
        
        <asp:GridView ID="gvPartyHits" runat="server" DataSourceID="dsPartyHits" AutoGenerateColumns="false" SkinID="MyGrid" EmptyDataText="No Party Hits found">
        
            <Columns>
                <asp:BoundField ReadOnly="True" HeaderText="Party Status" DataField="Party_Status"/>
                <asp:BoundField ReadOnly="True" HeaderText="Client.Matter" DataField="clnt_matt_code"/>
                <asp:BoundField ReadOnly="True" HeaderText="Client Name" DataField="client_name"/>
                <asp:BoundField ReadOnly="True" HeaderText="Open Date" DataField="open_date" DataFormatString="{0:MM/dd/yyyy}"/>
                <asp:BoundField ReadOnly="True" HeaderText="MRP" DataField="mrp"/>
            </Columns>
        </asp:GridView>

        <asp:GridView ID="gvPartyHits2" runat="server" DataSourceID="dsPartyHits" AutoGenerateColumns="false" SkinID="MyGrid" >
            <Columns>
                <asp:BoundField ReadOnly="True" HeaderText="Matter Party Name" DataField="Matter_Party_Name"/>
                <asp:BoundField ReadOnly="True" HeaderText="Matter Name" DataField="matter_name"/>
                <asp:BoundField ReadOnly="True" HeaderText="BA" DataField="ba"/>
                <asp:BoundField ReadOnly="True" HeaderText="Last Worked" DataField="last_time_date" DataFormatString="{0:MM/ddyyyy}"/>
            </Columns>
        </asp:GridView>
        <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;"> </div>
</asp:Panel>

<asp:Panel ID="pnlSignOff" runat="server" Visible="false">
<h3>Sign Off</h3>
            <asp:DropDownList ID="ddlSignOff" runat="server" 
        AppendDataBoundItems="True" ondatabound="ddlSignOff_DataBound">
                <asp:ListItem Selected="True" Text="Select Sign-Off reason" Value=""></asp:ListItem>
                <asp:ListItem Text="OK" Value="OK"></asp:ListItem>
                <asp:ListItem Text="Friendly" Value="MyFriend"></asp:ListItem>
                <asp:ListItem Text="Client" Value="CurrentClient"></asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="btnUpdate" runat="server" Text="Update" onclick="btnUpdate_Click" />
</asp:Panel>


<asp:SqlDataSource ID="dsPartyHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
			    SelectCommand="prc_qsel_PartyHits" SelectCommandType="StoredProcedure" >
                <SelectParameters>
                    <asp:QueryStringParameter Name="uno" QueryStringField="uno" />
                </SelectParameters>
		</asp:SqlDataSource>

        <asp:SqlDataSource ID="dsMatters" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
			SelectCommand="prc_qsel_MatterHits" SelectCommandType="StoredProcedure" >
                <SelectParameters>
                    <asp:QueryStringParameter Name="uno" QueryStringField="uno" />
                </SelectParameters>
		</asp:SqlDataSource>

       <asp:SqlDataSource ID="dsClientContacts" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>" SelectCommand="prc_qsel_ClientContactHits" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:QueryStringParameter Name="uno" QueryStringField="uno" />
                </SelectParameters>
        </asp:SqlDataSource>
        
        <asp:SqlDataSource ID="dsRelatedNames" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>" 
                    SelectCommand="prc_qsel_RelatedNameHits" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:QueryStringParameter Name="uno" QueryStringField="uno" />
                </SelectParameters>
           </asp:SqlDataSource>
    
</asp:Content>


