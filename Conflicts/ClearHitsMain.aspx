<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ClearHitsMain.aspx.cs" Inherits="Conflicts_ClearHitsMain" Theme="General" MaintainScrollPositionOnPostback="true" ValidateRequest="false"  %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">

    

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
 
    <ajaxToolkit:ToolkitScriptManager ID="scriptmanager" runat="server" 
        onasyncpostbackerror="scriptmanager_AsyncPostBackError"></ajaxToolkit:ToolkitScriptManager>


    <asp:UpdateProgress DynamicLayout="false" ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upTop" >
        <ProgressTemplate>
          <div class="CenterPB">
                    <img alt="progress" src="../Styles/Images/Bert2_Loader.gif"  
                            style="margin:0 auto;position:relative;height:10px;width:90px" />
                      <br /><label>Processing....</label>      
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:Panel runat="server" ID="pnlNoHitsToClear" Visible="false">
        <div style="vertical-align:middle; font-size:medium;  text-align:center; background-color:Black; height:20px">
            <asp:Label ID="lblNoHitsToClear" runat="server" ForeColor="White"></asp:Label>
        </div>
    </asp:Panel>


     <asp:UpdatePanel ID="upTop" runat="server" UpdateMode="Conditional">

        <ContentTemplate>


        <asp:Panel ID="pnlTop" runat="server" BackColor="#D3DEEF" Height="115px" Width="100%" >


            <div style="width:325px; float:left; border-style:solid; border-width:thin; 
                        padding-left:2px; padding-right:2px;">
                    <label style="font-size:small; text-decoration:underline;">To Clear Hits:</label><br />
                    <img alt="" style="height:15px" src="../Pictures/Number-1-icon.png" /> 
                    <label style="text-align:left; font-size:xx-small">Select items by clicking in a single box, multiple boxes or by using the 'Select ALL active and retired displayed hits up to the first 100' or select first closed hits feature</label><br />
                    <img alt="" style="height:15px" src="../Pictures/Number-2-icon.png" /> 
                    <label style="font-size:xx-small">Choose Sign Off reason</label><br />
                    <img alt="" style="height:15px" src="../Pictures/Number-3-icon.png" /> 
                    <label style="font-size:xx-small">Add Comments as needed and</label><br />
                    <img alt="" style="height:15px" src="../Pictures/Number-4-icon.png" /> 
                    <label style="font-size:xx-small">Click the SAVE button</label><br />
            </div>


            <div style="width:550px; float:left; height:auto; " >
                <table style="width:550px;">
                    <tr>
                        <td colspan="2">
                            <asp:Label runat="server" ID="lblFilter" 
                                Font-Size="Smaller" 
                                Font-Italic="true" 
                                Font-Underline="true"
                                Text="Filters:">
                             </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:150px">
                            <label style="font-size:xx-small">Select Matter Party:</label>
                        </td>
                        <td style="width:400px">
                            <asp:DropDownList ID="ddlPKId" runat="server" 
                                DataSourceID="dsPKId"  Width="400px"
                                DataTextField="originalnameaffiliation" 
                                DataValueField="partypkid" 
                                AppendDataBoundItems="true" 
                                AutoPostBack="true"
                                onselectedindexchanged="ddlPKId_SelectedIndexChanged">
                                <asp:ListItem   Selected="True" 
                                                Text="Select Party Name" 
                                                Value="0">
                                </asp:ListItem>
                                <asp:ListItem   Selected="False"    
                                                Text="All Party Names Searched with affiliation" 
                                                Value="-99">
                                </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:150px">
                            <label style="font-size:xx-small">Select Responsible Partner:</label>
                        </td>
                        <td style="width:400px">
                            <asp:DropDownList ID="ddlMRPCRP" runat="server"
                                DataSourceID="dsMRPCRP" Width="400px"
                                DataTextField="cms_crp_mrp"
                                DataValueField="cms_crp_mrp_uno"
                                AutoPostBack="true"
                                AppendDataBoundItems="true" 
                                onselectedindexchanged="ddlMRPCRP_SelectedIndexChanged">
                                    <asp:ListItem   Selected="False" 
                                                    Text="All Responsible Partners" 
                                                    Value="-99">
                                    </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                            <asp:CheckBox ID="cbDisplayHits" 
                                runat="server" 
                                CausesValidation="True" 
                                ForeColor="Black"
                                AutoPostBack="true"
                                Text="Display Cleared Hits" 
                                Font-Size="XX-Small"
                                OnCheckedChanged="cbDisplayHits_CheckedChanged" />
                        </td>
                    </tr>
                </table>
            </div>


            <div style="width:120px; text-align:right; float:right">
                <asp:DataList ID="dlTotalHitCount" runat="server" 
                                DataSourceID="dsTotalHitCount"
                                ForeColor="Blue">
                                <ItemTemplate>
                                    Remaining Hits:
                                    <asp:Label ID="lblToBeClearedHits" runat="server" 
                                        CssClass="redBold"  
                                        Text='<%#Eval("tobeclearedhits")%>'>
                                    </asp:Label>
                                <br />
                                     Total Hits:
                                    <asp:Label ID="lblTotalHits" runat="server" 
                                        CssClass="redBold" 
                                        Text='<%#Eval("totalhits")%>'>
                                    </asp:Label>

                                </ItemTemplate>
                   </asp:DataList>
            </div>


            

            
           
    </asp:Panel>

    
    <asp:Panel ID="pnlSpace_" runat="server" Height="2px"></asp:Panel>



    <asp:Panel  ID="pnlMid" runat="server" Height="60px" BackColor="#90B9D2" Width="100%" BorderStyle="Solid" BorderWidth="2px" BorderColor="Black" >                        

        <div style="float:left; width:100%; margin-top:1px; margin-left:5px; vertical-align:top; height:auto;">
            
            <div style="clear:both; width:35%; float:left">

                 <asp:Label runat="server" ID="lblClearLabel"  
                                        Font-Size="Smaller" 
                                        Font-Italic="true" 
                                        Font-Underline="true"
                                        Text="Clear:">
                </asp:Label><br />

                <asp:Image ID="img2" runat="server" ImageUrl="../Pictures/Number-2-icon.png" Height="15px" />

                <asp:DropDownList ID="ddlSignOff" runat="server" Width="95%"
                                  BackColor="#ffffaa"                          
                                  DataSourceID="objClearReason" 
                                  DataValueField="txtReasonCode" 
                                  DataTextField="txtReasonDesc">
                        <asp:ListItem   Selected="True" 
                                        Text="Select Sign-Off Reason" 
                                        Value="">
                        </asp:ListItem>
                </asp:DropDownList>
            </div>
            
            <div style="width:50%; float:left">
                <asp:Image ID="img3" runat="server" ImageUrl="../Pictures/Number-3-icon.png"
                                                 Height="15px" ImageAlign="Top"/>

                <asp:TextBox ID="txtSignOffComments"  
                                    runat="server" Width="95%"
                                    TextMode="MultiLine"
                                    Wrap="true" 
                                    MaxLength="1200"
                                    Rows="3">
                            </asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="WatermarkControl" 
                                        runat="server"  
                                        WatermarkCssClass="watermark"
                                        TargetControlID="txtSignOffComments"  
                                        WatermarkText="Enter comments here (**1200 character limit)...">
                            </ajaxToolkit:TextBoxWatermarkExtender>
                </div>
                <div style="width:10%; float:left">    
                    <br />
                            <asp:Image ID="img4" runat="server" ImageUrl="../Pictures/Number-4-icon.png"
                                                 Height="15px" ImageAlign="Top" />

                            <asp:Button runat="server" ID="btnUpdate"
                                    Width="60px"
                                    Height="40px" 
                                    BackColor="Black" ForeColor="White" Font-Size="Medium"
                                    Text="Save" 
                                    OnClick="btnUpdate_Click"
                                    CssClass="submitButton"
                                    Visible="true" />
            </div>     
      </div>

        

    </asp:Panel>
                
    <asp:Panel ID="pnlSpace" runat="server" Height="2px"></asp:Panel>

    <asp:UpdatePanel ID="upTabContainer" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        
        
    <ajaxToolkit:TabContainer ID="tc1" runat="server" 
        Font-Bold="true" 
        AutoPostBack="false" 
        ViewStateMode="Enabled"
        onactivetabchanged="tc1_ActiveTabChanged">
    
    
        <ajaxToolkit:TabPanel ID="tpClient" runat="server" HeaderText="Client Related Hits" Height="100%">
        
            <ContentTemplate>
            
                
                <!-- HIDE
                <asp:UpdatePanel ID="upClientGrid" runat="server" Visible="false">
                    <ContentTemplate>

                        <asp:GridView ID="gvClientHits" runat="server" 
                         Visible="true"
                                AllowPaging="True" 
                                AllowSorting="True"
                                DataSourceID="dsClientHits" 
                                DataKeyNames="hitautoid" 
                                AutoGenerateColumns="False" 
                                SkinID="MyGrid"
                                Width="100%"
                                PageSize="20" 
                                ShowFooter="True" 
                                OnDataBound="gvClientHits_DataBound" 
                                OnRowDataBound="gvClientHits_RowDataBound" >

                                <Columns>

                                    <asp:BoundField Visible="false" DataField="hitautoid"/>
                                    <asp:BoundField Visible="false" DataField="cms_uno" />

                        
                                     <asp:TemplateField HeaderStyle-ForeColor="White">
                                        <HeaderTemplate>
                                            <asp:CheckBox runat="server" ID="cbClientSignOffAll" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="cbClientSignOff"/>
                                        </ItemTemplate>
                                         <HeaderStyle ForeColor="White" />
                                    </asp:TemplateField>

                                    <asp:BoundField Visible="True" DataField="cms_party_status_desc" HeaderText="Party Status" 
                                            HeaderStyle-ForeColor="White" >
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
                                    <asp:BoundField ReadOnly="True" HeaderText="Hit Name" DataField="cms_hit_name"
                                                SortExpression="cms_hit_name" HeaderStyle-ForeColor="White" >

      
                        
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>

                                                      <asp:TemplateField SortExpression="NBIPartyName" 
                                                        HeaderText='Client Name' 
                                                        HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hlClient" runat="server" ForeColor="Blue"
                                                NavigateUrl='<%# "Conflicts_DD.aspx?uno=" + Eval("cms_uno") + "&source=client"  + "&name=" + 
                                    HttpUtility.UrlEncode(Eval("nbipartyname").ToString()) + "&autoid=" + Eval("hitautoid") + "&fid=" + Eval("fid") + "&signoff=" + Eval("SignOffCode") + "&partypkid=" + Eval("PartyPKID") + "&ddlpartypkid=" + ddlPKId.SelectedValue + "&activetab=0"  %>'
                                                 Target="_self" 
                                                 Text='<%#Eval("NBIPartyName")%>' />
                                         </ItemTemplate>
                                    </asp:TemplateField>
                        
                                    <asp:BoundField Visible="True" DataField="cms_matter_name" HeaderText="Matter Name" 
                                            HeaderStyle-ForeColor="White" >
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
                                    <asp:BoundField Visible="True" DataField="cms_clnt_matt_code" HeaderText="Client.Matter" 
                                            HeaderStyle-ForeColor="White" >
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
                                    <asp:BoundField Visible="True" DataField="cms_last_worked" HeaderText="Last Worked" 
                                            HeaderStyle-ForeColor="White" >
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
                                    <asp:BoundField Visible="True" DataField="cms_open_date" HeaderText="Open Date" 
                                            HeaderStyle-ForeColor="White" >
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
                                    <asp:BoundField Visible="True" DataField="cms_date_added" HeaderText="Date Added" 
                                            HeaderStyle-ForeColor="White" >
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
                                    <asp:BoundField Visible="True" DataField="cms_close_date" HeaderText="Close Date" 
                                            HeaderStyle-ForeColor="White" >
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
                                    <asp:BoundField Visible="True" DataField="cms_crp_mrp" HeaderText="CRP/MRP" 
                                        SortExpression="cms_crp_mrp"                                        
                                        HeaderStyle-ForeColor="White" >
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
                                    <asp:BoundField Visible="True" DataField="cms_ba" HeaderText="BA" SortExpression="cms_ba" 
                                                HeaderStyle-ForeColor="White" >
            
                                    <HeaderStyle ForeColor="White" />
                                    </asp:BoundField>
            
                                    <asp:TemplateField SortExpression="txtReasonDesc" HeaderText="Resolution" HeaderStyle-ForeColor="Green"  
                                        Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblResolution" runat="server" Text='<%#Eval("resolution")%>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle ForeColor="Yellow" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderStyle-ForeColor="White">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbComments" 
                                                Text="Comments"
                                                OnClick="lbComments_Click">
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>                  
                            
                                </Columns>
                            </asp:GridView>   
            
            
                        <asp:Button ID="Button1" runat="server" Text="" style="display: none;"  />
                    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" 
                        BackgroundCssClass="ModalPopupBG"
                        BehaviorID="mpe"
                        runat="server" 
                        CancelControlID="btnCancel" 
                        OkControlID="btnOkay" 
                        TargetControlID="Button1"
                        PopupControlID="Panel1" 
                        PopupDragHandleControlID="PopupHeader">
                    </ajaxToolkit:ModalPopupExtender>
                    <div id="Panel1" style="display: none;" class="popupConfirmation">
                        <div class="popup_Container">
                            <div class="popup_Titlebar" id="PopupHeader">
                                <div class="TitlebarLeft">Comments</div>
                                <div class="TitlebarRight"  onclick="HidePopup()"></div>
                            </div>
                            <div class="popup_Body">
                            <asp:FormView ID="fvComments" runat="server" EmptyDataText="No comments entered">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtComments" runat="server" 
                                            ReadOnly="true" 
                                            TextMode="MultiLine" 
                                            Rows="20" 
                                            Width="375px" 
                                            Height="250px" 
                                            ForeColor="Blue"
                                            Text='<%#Eval("comments").ToString() %>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:FormView>
                            </div>
                            <div class="popup_Buttons">
                                <input id="btnOkay" value="Done" type="button" style="display: none;" />
                                <input id="btnCancel" value="Cancel" type="button" />
                            </div>
                        </div>
                    </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                -->

                <asp:Panel runat="server" ID="pnlClientList" ScrollBars="Vertical" Height="500px" Width="100%" >
                            <div id="div1">

                            <div style="float:right; width:40%">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            &nbsp&nbsp
                                        </td>
                                        <td align="right">
                                            <asp:DataList ID="dlClientHitCount" runat="server" 
                                                DataSourceID="dsClientHitCount"><ItemTemplate>
                                                    Remaining Hits:
                                                    <asp:Label ID="lblToBeClearedHits" runat="server" 
                                                        CssClass="redBold" 
                                                        Text='<%#Eval("tobeclearedhits")%>'>
                                                    </asp:Label>
                                                    &#160;&#160;
                                                    Total Hits:
                                                    <asp:Label ID="lblTotalHits" runat="server" 
                                                        CssClass="redBold" 
                                                        Text='<%#Eval("totalhits")%>'>
                                                    </asp:Label>
                                                  
</ItemTemplate>
</asp:DataList>


                                        </td>
                                    </tr>
                                </table>
                                </div>

                            <asp:Label ID="lbldlClientNamesHitsNoData" runat="server" 
                                    Font-Bold="False" 
                                    Font-Size="Smaller" 
                                    ForeColor="Gray"  
                                    Visible="False" 
                                    Text="No Client Data Found"></asp:Label>



                             <asp:UpdatePanel ID="upClientNameHits" runat="server" UpdateMode="Conditional"><ContentTemplate>

                                    

                                    <div style="float:left; width:50%">
                                      
                                        <asp:Image ID="imgClientAllList" runat="server" ImageUrl="../Pictures/Number-1-icon.png"
                                                 Height="15px" />

                                      <asp:CheckBoxList 
                                        ID="chkClientAllList"  
                                        runat="server" 
                                        AutoPostBack="true" 
                                        OnSelectedIndexChanged="chkClientAllList_SelectedIndexChanged">
                                        <asp:ListItem Text="" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="" Value="1"></asp:ListItem>
                                    </asp:CheckBoxList>
                                    </div>

                                    <asp:DataList runat="server" ID="dlClientNameHits"  Visible="true"
                                        DataSourceID="dsClientHits" 
                                        GridLines="Both"
                                        CssClass="myGrid" 
                                        RepeatDirection="Horizontal" 
                                        AlternatingItemStyle-BackColor="#DDDDDDDD"
                                        RepeatColumns="3"
                                        CellPadding="1" 
                                        CellSpacing="1" 
                                        ItemStyle-Width="33%"
                                        ItemStyle-Wrap="true"
                                        Width="98%"
                                        OnItemDataBound="dlClientNameHits_ItemDataBound">
                                    <HeaderTemplate>
                        
                                    </HeaderTemplate>
                                    <ItemStyle BorderColor="Black" BorderStyle="Dashed" />
                                    <AlternatingItemStyle BackColor="#DDDDDDDD" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hfClientNameSignOffCode" runat="server" 
                                            Value='<%#Eval("signoffcode") %>'>
                                        </asp:HiddenField>
                                        <asp:Label ID="lblID" runat="server" 
                                            Visible="false" 
                                            Text='<%#Eval("hitautoid").ToString() %>'>
                                         </asp:Label>
                                         <asp:Label ID="lblHitSource" runat="server" 
                                            Visible="false" 
                                            Text='<%#Eval("cms_hit_source").ToString() %>'>
                                         </asp:Label>

                                        <asp:Label ID="lblClientPartyStatus" runat="server"
                                            Font-Bold="true" 
                                            Font-Size="12px" 
                                            Font-Underline="false"
                                            ForeColor="Blue"  
                                            Text='<%#Eval("cms_party_status_desc") %>'>
                                        </asp:Label>
                                        <br>

                                        <asp:Label ID="lblResolution" runat="server"
                                            ForeColor="Green"
                                            Font-Bold="true" 
                                            Font-Size="12px"
                                            Text='<%#"Resolution: " + Eval("resolution") %>' >
                                        </asp:Label>
                                        <br>

                                        <asp:Image ID="img1a" runat="server" ImageUrl="../Pictures/Number-1-icon.png"
                                                 Height="15px" />
                                      
                                         <asp:CheckBox runat="server" ID="cbSelectItem" 
                                            Text="<--Click here to select" 
                                            AutoPostBack="true"
                                            Font-Bold="true" 
                                            ForeColor="Black"
                                            OnCheckedChanged="cbSelectItem_CheckedChanged"/>

                                         <br>

                                        <b>Party: </b>
                                        <asp:Label ID="lblPN" runat="server" 
                                            Text='<%#Eval("cms_party_name") %>' 
                                            Font-Bold="true" 
                                            ForeColor="DarkBlue">
                                        </asp:Label>

                                        <br>

                                        <b>Client: </b>

                                        <asp:HyperLink  ID="hlClient" runat="server" ForeColor="Blue"
                                   NavigateUrl='<%# "ConflictDetailsDD.aspx?uno=" + Eval("cms_uno") + "&source=client"  +
                                     "&name=(" + HttpUtility.UrlEncode(Eval("cms_clnt_matt_code").ToString().TrimEnd() + ") " + Eval        ("cms_client_name").ToString()) + "&autoid=" + Eval("hitautoid") + "&fid=" + Eval("fid") + "&signoff=" + Eval("SignOffCode") %>'
                                                Target="drilld"
                                                Text='<%#Eval("cms_client_name") + " <--Must click to review matters"%>' />

 
 

       



                                        <asp:Label ID="lblClientName" runat="server"
                                            Text='<%#Eval("cms_client_name")%>'
                                            Visible="false">                                           
                                        </asp:Label>
                                        <br>

                                        <b>Matter: </b>
                                        <asp:Label ID="lblMN" runat="server" 
                                            Text='<%#Eval("cms_matter_name") %>'>
                                        </asp:Label>
                            
                                        <br>

                                        <b>Client.Matter: </b>
                                        <asp:Label ID="lblCMC" runat="server" 
                                            Text='<%#Eval("cms_clnt_matt_code") %>'>
                                        </asp:Label>
                            
                                        <br>

                                        <b>Last Worked: </b>
                                        <asp:Label ID="lblLastWorkDt" runat="server" 
                                            Text='<%#Eval("cms_last_worked") %>'>
                                        </asp:Label>

                                        <br />

                                        <b>Open Date: </b>
                                        <asp:Label ID="lblOpenDt" runat="server" 
                                            Text='<%#Eval("cms_open_date") %>'>
                                        </asp:Label>

                                        <br />

                                        <b>Date Added: </b>
                                        <asp:Label ID="lblDtAdded" runat="server" 
                                            Text='<%#Eval("cms_date_added") %>'>
                                        </asp:Label>

                                        <br/>
                            
                                        <b>Close Date: </b>
                                        <asp:Label ID="lblCloseDt" runat="server" 
                                            Text='<%#Eval("cms_close_date") %>'>
                                        </asp:Label>

                                        <br>

                                        <b>CRP/MRP: </b>
                                        <asp:Label ID="lblMRP" runat="server" 
                                            Text='<%#Eval("cms_crp_mrp") %>'>
                                        </asp:Label>

                                        <br>

                                        <b>BA:</b>
                                        <asp:Label ID="lblBA" runat="server" 
                                            Text='<%#Eval("cms_ba") %>'>
                                        </asp:Label>

                                        <br>

                                        <b>CMS Remarks:</b>
                                        <asp:Label ID="lblCMSRemarks" runat="server" 
                                            Text='<%#Eval("cms_comments") %>'>
                                        </asp:Label>

                                        <br>
                                        <br>
                                        
                                        <asp:TextBox ID="txtClientNameSignOffComments" runat="server" 
                                                Width="375px" 
                                                TextMode="MultiLine"
                                                Wrap="true"
                                                Rows="7"
                                                ReadOnly="true"
                                                CssClass="myGridItemComments"
                                                Text='<%#Eval("extended_comments") %>'>
                                        </asp:TextBox>

                                        <br>
                                        </ItemTemplate>
                                    </asp:DataList>

                                
</ContentTemplate>
</asp:UpdatePanel>

                               
                                    
                            
                            <!--HIDE
                                <asp:DataList runat="server" ID="dlClientNameHits2" Visible="false"
                                        DataSourceID="dsClientHits" 
                                        GridLines="Both"
                                        CssClass="myGrid" 
                                        RepeatDirection="Vertical" 
                                        RepeatColumns="0"
                                        CellPadding="1" 
                                        CellSpacing="2" 
                                        ItemStyle-Wrap="true"
                                        Width="98%"
                                        RepeatLayout="Table"
                                        OnItemDataBound="dlClientNameHits_ItemDataBound">
                                  
                                  					<HeaderTemplate>
					                                                    
					                                    </HeaderTemplate>
                                                        <HeaderStyle CssClass="myGridHeader" />
					                                    <ItemStyle BorderColor="Black" BorderStyle="Dashed" CssClass="myGridItem" />
                                                        <AlternatingItemStyle CssClass="myGridAltItem" />
					
					                                    <ItemTemplate>
					
					                                        <asp:HiddenField ID="hfClientNameSignOffCode" 
					                                            runat="server" 
					                                            Value='<%#Eval("signoffcode") %>'>
					                                        </asp:HiddenField>
					                                        <asp:Label ID="lblID" 
					                                            runat="server" 
					                                            Visible="false" 
					                                            Text='<%#Eval("hitautoid").ToString() %>'>
					                                        </asp:Label>
					
					
					                                            <asp:CheckBox runat="server" ID="cbClientNameSignOff" 
					                                                        Text="" 
					                                                        AutoPostBack="true" 
					                                                        Font-Bold="true" />  
                                                                             
					                                           &nbsp;
                                                                  
					                                                    <asp:Label ID="lblClientPartyStatus" runat="server"
					                                                        Font-Bold="true" 
					                                                        Font-Size="12px" 
					                                                        Font-Underline="false"
					                                                        ForeColor="Blue"  
					                                                        Text='<%#Eval("cms_party_status_desc") %>'>
					                                                    </asp:Label>
					
					                                             <br />
					                                                    <asp:Label ID="lblResolution" runat="server"
					                                                        ForeColor="Green"
					                                                        Font-Bold="true" 
					                                                        Font-Size="12px"
					                                                        Text='<%#"Resolution: " + Eval("resolution") %>' >
					                                                    </asp:Label>
					
					                                           <table class="myGrid">
                                                               <th>Party Name</th>
                                                               <th>Client Name</th>
                                                               <th>Matter Name</th>
                                                               <th>Client.Matter</th>
                                                               <th>Last Worked</th>
                                                               <th>Opened</th>
                                                               <th>Added</th>
                                                               <th>Closed</th>
                                                               <th>CRP/MRP</th>
                                                               <th>BA</th>
					                                            <tr>
                                                                    
					                                                <td>
					                                                     <asp:Label ID="lblPN" runat="server" 
					                                                        Text='<%#Eval("cms_party_name") %>' 
					                                                        Font-Bold="true" 
					                                                        ForeColor="DarkBlue">
					                                                    </asp:Label>
					                                                </td>
					                                                <td>
					                                                    
                                                                        <asp:HyperLink ID="hlClient" runat="server" ForeColor="Blue"
                                                NavigateUrl='<%# "Conflicts_DD.aspx?uno=" + Eval("cms_uno") + "&source=client"  + "&name=" + 
                                    HttpUtility.UrlEncode(Eval("nbipartyname").ToString()) + "&autoid=" + Eval("hitautoid") + "&fid=" + Eval("fid") + "&signoff=" + Eval("SignOffCode") + "&partypkid=" + Eval("PartyPKID") + "&ddlpartypkid=" + ddlPKId.SelectedValue + "&activetab=3"  %>'
                                                 Target="_self" 
                                                 Text='<%#Eval("NBIPartyName")%>' />
					                                                </td>
					                                                <td>
					                                                    <asp:Label ID="lblMN" runat="server" 
					                                                        Text='<%#Eval("cms_matter_name") %>'>
					                                                    </asp:Label>
					                                                </td>
					                                                <td>
					                                                    <asp:Label ID="lblCMC" runat="server" 
					                                                        Text='<%#Eval("cms_clnt_matt_code") %>'>
					                                                    </asp:Label>
					                                                </td>
					                                                <td>
					                                                    <asp:Label ID="lblLastWorkDt" runat="server" 
					                                                        Text='<%#Eval("cms_last_worked") %>'>
					                                                    </asp:Label>
					                                                </td>
					                                                <td>
					                                                    <asp:Label ID="lblOpenDt" runat="server" 
					                                                        Text='<%#Eval("cms_open_date") %>'>
					                                                    </asp:Label>
					                                                </td>
					                                                <td>
					                                                    <asp:Label ID="lblDtAdded" runat="server" 
					                                                        Text='<%#Eval("cms_date_added") %>'>
					                                                    </asp:Label>
					                                                </td>
					                                                    <asp:Label ID="lblCloseDt" runat="server" 
					                                                        Text='<%#Eval("cms_close_date") %>'>
					                                                    </asp:Label>
					                                                <td>
					                                                    <asp:Label ID="lblMRP" runat="server" 
					                                                        Text='<%#Eval("cms_crp_mrp") %>'>
					                                                    </asp:Label>
					                                                </td>
					                                                <td>
					                                                    <asp:Label ID="lblBA" runat="server" 
					                                                        Text='<%#Eval("cms_ba") %>'>
					                                                    </asp:Label>
					                                                </td>
					                                            </tr>
					                                           </table>              
					                                            
                                                                <b>CMS Remarks:</b>
					                                                    <asp:Label ID="Label2" runat="server" 
					                                                        Text='<%#Eval("cms_comments") %>'>
					                                                    </asp:Label>
					                                        <br />
					                                        <b>Comments:</b>
					                                                    <asp:Label ID="Label1" runat="server" 
					                                                        Text='<%#Eval("comments") %>'>
					                                                    </asp:Label>
					                                    </ItemTemplate>
					                                    <FooterTemplate>
					                                        
					                                    </FooterTemplate>

                            </asp:DataList>
                            -->
                            </div>
                    </asp:Panel>






                <asp:SqlDataSource ID="dsClientHits" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                                SelectCommand="prc_qsel_clientclientpartyhitdetail"
                                OnSelected="dsClientHits_OnSelected"
                                SelectCommandType="StoredProcedure"><SelectParameters>
<asp:QueryStringParameter Name="fid" QueryStringField="fid" />
<asp:ControlParameter 
                                        ControlID="hfClientSignOff" 
                                        DbType="Int16" 
                                        DefaultValue="1"
                                        Name="signoff" />
<asp:ControlParameter 
                                        ControlID="ddlPKId" 
                                        DefaultValue="" 
                                        Name="partypkid"/>
<asp:ControlParameter ControlID="ddlMRPCRP" DefaultValue="-99" Name="cms_crp_mrp_uno"/>
</SelectParameters>
</asp:SqlDataSource>



                <asp:SqlDataSource ID="dsClientHitCount" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                                SelectCommandType="StoredProcedure" 
                                SelectCommand="prc_qsel_ClientClientNameHitCount"><SelectParameters>
<asp:QueryStringParameter Name="fid" QueryStringField="fid" />
<asp:ControlParameter ControlID="ddlPKId" DefaultValue="-99" Name="partypkid"/>
<asp:ControlParameter ControlID="ddlMRPCRP" DefaultValue="-99" Name="cms_crp_mrp_uno"/>
</SelectParameters>
</asp:SqlDataSource>


                  
            
</ContentTemplate>

        









</ajaxToolkit:TabPanel>

        <ajaxToolkit:TabPanel ID="tpName" runat="server" HeaderText="Adverse Hits/Warnings">
            <ContentTemplate>
                

                <asp:UpdatePanel ID="upNameHits" runat="server" UpdateMode="Conditional">
                   <ContentTemplate>

                   <div style="float:right; width:40%">
                    <table width="100%">
                    <tr>
                        <td>
                            &nbsp&nbsp
                        </td>
                        <td align="right">
                            <asp:DataList ID="dlNameHitCount" runat="server" DataSourceID="dsNameHitCount">
                                <ItemTemplate>
                                    Remaining Hits:
                                    <asp:Label ID="lblToBeClearedHits" runat="server" CssClass="redBold" Text='<%#Eval("tobeclearedhits")%>'></asp:Label>&#160;&#160;
                                    Total Hits:
                                    <asp:Label ID="lblTotalHits" runat="server" CssClass="redBold" Text='<%#Eval("totalhits")%>'></asp:Label></ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                    </table>
                    </div>
                

                <asp:Label runat="server" ID="lbldlNameHitsNoData" 
                    Font-Bold="false" 
                    Font-Size="Smaller" 
                    ForeColor="Gray"  
                    Visible="false" 
                    Text="No Party Hit Data Found">
                </asp:Label>

                       <asp:Image ID="imgNameAllList" runat="server" ImageUrl="../Pictures/Number-1-icon.png"
                                                 Height="15px" />

                    <div style="float:left; width:35%">
                       
                        <asp:CheckBoxList 
                        ID="chkNameAllList" 
                        runat="server" 
                        AutoPostBack="true" 
                        OnSelectedIndexChanged="chkNameAllList_SelectedIndexChanged">
                        <asp:ListItem Text="" Value="0"></asp:ListItem>
                        <asp:ListItem Text="" Value="1"></asp:ListItem>
                    </asp:CheckBoxList>

                    </div>

                <asp:DataList runat="server" ID="dlNameHits" 
                    DataSourceID="dsNameHits" 
                    GridLines="Horizontal"
                    CssClass="myGrid" 
                    RepeatLayout="Table" 
                    RepeatDirection="Horizontal" 
                    RepeatColumns="3"
                    CellPadding="2" 
                    CellSpacing="1" 
                    ItemStyle-Width="33%"
                    AlternatingItemStyle-BackColor="#DDDDDDDD" 
                    ItemStyle-BorderColor="Black"
                    ItemStyle-BorderStyle="Dotted" 
                    OnItemDataBound="dlNameHits_ItemDataBound">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hfSignOffCode" runat="server" 
                            Value='<%#Eval("signoffcode") %>'>
                        </asp:HiddenField>

                        <asp:Label ID="lblPartyStatus" runat="server"
                            Font-Bold="true" 
                            Font-Size="12px" 
                            Font-Underline="false"
                            ForeColor="Blue"  
                            Text='<%#Eval("cms_party_status_desc") %>'>
                        </asp:Label>
                        <br>

                        <asp:Label ID="lblID" runat="server" 
                            Visible="false" 
                            Text='<%#Eval("hitautoid").ToString() %>'>
                        </asp:Label>

                        

                        <asp:Label ID="lblResolution" runat="server"
                            ForeColor="Green"
                            Font-Bold="true" 
                            Font-Size="12px"
                            Text='<%#"Resolution: " + Eval("resolution") %>' >
                        </asp:Label>
                        <br>
                        <asp:Image ID="img1b" runat="server" ImageUrl="../Pictures/Number-1-icon.png"
                                                 Height="15px" /> 
                        <asp:CheckBox runat="server" ID="cbSelectItem" 
                            Text="<--Click here to select" 
                            AutoPostBack="true" 
                            Font-Bold="true"
                            ForeColor="Black"
                            OnCheckedChanged="cbSelectItem_CheckedChanged"/>
                        <br>

                        <b>Party: </b>
                        <asp:Label ID="lblPN" runat="server" 
                            Text='<%#Eval("cms_party_name") %>'  
                            Font-Bold="true" 
                            ForeColor="Blue">
                        </asp:Label>
                        <br>

                        <b>Client: </b>
                        <asp:Label ID="lblCN" runat="server" 
                            Text='<%#Eval("cms_client_name") %>'>
                        </asp:Label>
                        <br>

                        <b>Matter: </b>
                        <asp:Label ID="lblMN" runat="server" 
                            Text='<%#Eval("cms_matter_name") %>'>
                        </asp:Label>
                        <br>

                        <b>Client.Matter:</b>
                        <asp:Label ID="lblCMC" runat="server" 
                            Text='<%#Eval("cms_clnt_matt_code") %>'>
                        </asp:Label>
                        <br>

                        <b>Open Date: </b>
                        <asp:Label ID="lblOpenDt" runat="server" 
                            Text='<%#Eval("cms_open_date") %>'>
                        </asp:Label>
                        <br />
                        
                        <b>Date Added: </b>
                        <asp:Label ID="lblDtAdded" runat="server" 
                            Text='<%#Eval("cms_date_added") %>'>
                        </asp:Label>
                        <br>

                        <b>Last Worked: </b>
                        <asp:Label ID="lblLastWorkDt" runat="server" 
                            Text='<%#Eval("cms_last_worked") %>'>
                        </asp:Label>
                        <br />
                        
                        <b>Close Date: </b>
                        <asp:Label ID="lblCloseDt" runat="server" 
                            Text='<%#Eval("cms_close_date") %>'>
                        </asp:Label>
                        <br>

                        <b>MRP: </b>
                        <asp:Label ID="lblMRP" runat="server" 
                            Text='<%#Eval("cms_crp_mrp") %>'>
                        </asp:Label>
                        <br>
                        
                        <b>BA:</b>
                        <asp:Label ID="lblBA" runat="server" 
                            Text='<%#Eval("cms_ba") %>'>
                        </asp:Label>
                        <br>

                        <b>CMS Remarks:</b>
                        <asp:Label ID="Label1" runat="server" 
                            Text='<%#Eval("cms_comments") %>'>
                        </asp:Label>

                        <br>
                        <br>
                        
                        <asp:TextBox ID="txtClientNameSignOffComments" runat="server" 
                                Width="375px" 
                                TextMode="MultiLine"
                                Wrap="true"
                                Rows="7"
                                ReadOnly="true"
                                CssClass="myGridItemComments"
                                Text='<%#Eval("extended_comments") %>'>
                        </asp:TextBox>
                        <br>
                        
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:DataList>
               
                </ContentTemplate>
                </asp:UpdatePanel>
                    <br />

                
                <asp:SqlDataSource ID="dsNameHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_NameHitDetail" 
                    SelectCommandType="StoredProcedure"
                    OnSelected="dsNameHits_OnSelected">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="hfNameSignOff" DbType="Int16" DefaultValue="1" Name="signoff" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                        <asp:ControlParameter ControlID="ddlMRPCRP" DefaultValue="-99" Name="cms_crp_mrp_uno"/>
                    </SelectParameters>
                </asp:SqlDataSource>
                
                <asp:SqlDataSource ID="dsNameHitCount" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_NameHitCount" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                        <asp:ControlParameter ControlID="ddlMRPCRP" DefaultValue="-99" Name="cms_crp_mrp_uno"/>
                    </SelectParameters>
                </asp:SqlDataSource>
                
                <asp:HiddenField ID="hfNameSignOff" runat="server" Value="1" />
            
</ContentTemplate>
        








</ajaxToolkit:TabPanel>

        <ajaxToolkit:TabPanel ID="tpPendingName" HeaderText="Opposite Pending Hits" runat="server">
            <ContentTemplate>
                
                <asp:UpdatePanel ID="upPendingHits" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                    <div style="float:right; width:40%">
                    <table width="100%">
                        <tr>
                            <td align="left"></td>
                            <td>&nbsp&nbsp</td>
                            <td align="right">
                                <asp:DataList ID="dlPendingNameHitCount" runat="server" DataSourceID="dsPendingNameHitCount">
                                    <ItemTemplate>
                                        Remaining Hits:
                                        <asp:Label ID="lblToBeClearedHits" runat="server" 
                                            CssClass="redBold" 
                                            Text='<%#Eval("tobeclearedhits")%>'>
                                        </asp:Label>&#160;&#160;
                                        Total Hits:
                                        <asp:Label ID="lblTotalHits" runat="server" 
                                            CssClass="redBold" 
                                            Text='<%#Eval("totalhits")%>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                    </table>
                </div>

                <asp:Label ID="lbldlPendingHitsNoData" runat="server" 
                    Font-Bold="false" 
                    Font-Size="Smaller" 
                    ForeColor="Gray"  
                    Visible="false" 
                    Text="No Pending Hit Data Found">
                </asp:Label>
                    
                        <asp:Image ID="imgPendingNameAllList" runat="server" ImageUrl="../Pictures/Number-1-icon.png"
                                                 Height="15px" />

                    <div style="float:left; width:35%">
                        <asp:CheckBoxList  
                            ID="chkPendingNameAllList" 
                            runat="server" 
                            AutoPostBack="true" 
                            OnSelectedIndexChanged="chkPendingNameAllList_SelectedIndexChanged">
                            <asp:ListItem Text="" Value="0"></asp:ListItem>
                        </asp:CheckBoxList>
                    </div>

                <asp:DataList runat="server" ID="dlPendingHits" 
                    DataSourceID="dsPendingHits" 
                    GridLines="Both"
                    CssClass="myGrid" 
                    RepeatDirection="Horizontal" 
                    AlternatingItemStyle-BackColor="#DDDDDDDD"
                    RepeatColumns="3"
                    CellPadding="1" 
                    CellSpacing="1" 
                    ItemStyle-Width="33%"
                    ItemStyle-Wrap="true"
                    Width="98%" 
                    OnItemDataBound="dlPendingHits_ItemDataBound">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hfSignOffCodeb" runat="server" Value='<%#Eval("signoffcode") %>'>
                        </asp:HiddenField>

                        <asp:Label ID="lblPartyStatus" runat="server"
                            Font-Bold="true" 
                            Font-Size="12px" 
                            Font-Underline="false"
                            ForeColor="Blue"  
                            Text='<%#Eval("cms_party_status_desc") %>'>
                        </asp:Label>
                        <br>
                        <asp:Image ID="img1c" runat="server" ImageUrl="../Pictures/Number-1-icon.png"
                                                 Height="15px" />
                        <asp:CheckBox runat="server" ID="cbSelectItem" 
                            Text="<--Click here to select" 
                            AutoPostBack="true" 
                            Font-Bold="true"
                            ForeColor="Black"
                            OnCheckedChanged="cbSelectItem_CheckedChanged"/>
                        <br>

                        <asp:Label ID="lblID" runat="server" 
                            Visible="false" 
                            Text='<%#Eval("hitautoid").ToString() %>'>
                        </asp:Label>

                        <asp:Label ID="lblResolution" runat="server"
                            ForeColor="Green"
                            Font-Bold="true" 
                            Font-Size="12px"
                            Text='<%#"Resolution: " + Eval("resolution") %>' >
                        </asp:Label>
                        <br>

                        <b>Pending Party: </b>
                        <asp:Label ID="lblPNb" runat="server" 
                            Text='<%#Eval("cms_party_name") %>' 
                            Font-Bold="true" 
                            ForeColor='Red'>
                        </asp:Label>
                        <br>

                        <b>Client: </b>
                        <asp:Label ID="lblCNb" runat="server" 
                            Text='<%#Eval("cms_client_name") %>'>
                        </asp:Label>
                        <br>

                        <b>Pending Matter: </b>
                        <asp:Label ID="lblMNb" runat="server" 
                            Text='<%#Eval("cms_matter_name") %>'>
                        </asp:Label>
                        <br>

                        <b>Client.Matter: </b>
                        <asp:Label ID="lblCMCb" runat="server" 
                            Text='<%#Eval("cms_clnt_matt_code") %>'>
                        </asp:Label>
                        <br>

                        <b>Status: </b>
                        <asp:Label ID="lblHTb" runat="server" 
                            Text='<%#Eval("hittype") %>'>
                        </asp:Label>
                        <br>

                        <b>Req Atty: </b>
                        <asp:Label ID="lblRAb" runat="server" 
                            Text='<%#Eval("cms_crp_mrp") %>'>
                        </asp:Label>
                        <br>

                        
                        <asp:TextBox ID="txtClientNameSignOffComments" runat="server" 
                                Width="375px" 
                                TextMode="MultiLine" 
                                Wrap="true"
                                Rows="7"
                                ReadOnly="true"
                                CssClass="myGridItemComments"
                                Text='<%#Eval("extended_comments") %>'>
                        </asp:TextBox>

                        <br>

                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                
                 </asp:DataList>
                    
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:SqlDataSource ID="dsPendingHits" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_PendingNameHitDetail" 
                        SelectCommandType="StoredProcedure"
                        OnSelected="dsPendingNameHits_OnSelected">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                            <asp:ControlParameter ControlID="hfPendingNameSignOff" DbType="Int16" DefaultValue="1"
                                Name="signoff" />
                            <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                            <asp:ControlParameter ControlID="ddlMRPCRP" DefaultValue="-99" Name="cms_crp_mrp_uno" />
                        </SelectParameters>
                 </asp:SqlDataSource>

                <asp:SqlDataSource ID="dsPendingNameHitCount" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_PendingNameHitCount" 
                        SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                        <asp:ControlParameter ControlID="ddlMRPCRP" DefaultValue="-99" Name="cms_crp_mrp_uno"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:HiddenField ID="hfPendingNameSignOff" runat="server" Value="1" />
            
</ContentTemplate>
        








</ajaxToolkit:TabPanel>

        <ajaxToolkit:TabPanel ID="tpNoClear" HeaderText="No SignOff Required" runat="server">
            <HeaderTemplate>
                    <asp:Label runat="server" ID="lblNSR"  Text="No SignOff Required"></asp:Label>
                </HeaderTemplate>
        <ContentTemplate>
            <asp:Panel ID="pnlNCClients" runat="server">
                <h3>Client Hits Not To Be Cleared</h3>
                <asp:GridView ID="gvTotalClientHits" runat="server" 
                    DataSourceID="dsTotalClientHits"
                    DataKeyNames="hitautoid" 
                    AutoGenerateColumns="False" 
                    HeaderStyle-ForeColor="#FFFFFF"
                    SkinID="MyGrid" 
                    PagerStyle-CssClass="myGridPagerStyle"
                    PageSize="20"
                    ShowFooter="True"
                    AllowPaging="True"  
                    AllowSorting="True" 
                    EmptyDataText="No Client Data Found">
                    <Columns>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField Visible="False" DataField="PartyPKID" />
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblCMS_Uno" runat="server" Text='<%#Eval("cms_uno")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        

                        <asp:TemplateField SortExpression="cms_client_name" HeaderText='Client (Code) Name'>
                         <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" 
                                NavigateUrl='<%# "ConflictDetailsDD.aspx?uno=" + Eval("cms_uno") + "&source=ROclient"  + "&name=" +
                                HttpUtility.UrlEncode(Eval("cms_client_name").ToString()) + "&fid=" + Eval("fid") + 
                                "&signoff=" + Eval("SignOffCode")%>'
                                    runat="server" 
                                    ForeColor="Blue"
                                    Target="drilld" 
                                    Text='<%#Eval("cms_client_name") + " <--Click to review matters"%>' />
                           </ItemTemplate>
                         </asp:TemplateField>
                         
                        
                        <asp:BoundField ReadOnly="True" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="CRP" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField ReadOnly="True" DataField="cms_ba" SortExpression="cms_ba" 
                            HeaderText="Billing Atty" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField ReadOnly="True" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" >
                        <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
               
            </asp:Panel>
            
            <asp:Panel ID="pnlNCPartyHits" runat="server">
                <h3>Party Hits Not To Be Cleared</h3>
                <asp:GridView ID="gvNameClearedHits" runat="server" 
                    DataSourceID="dsNameClearedHits"
                    DataKeyNames="hitautoid" 
                    AutoGenerateColumns="false" 
                    SkinID="MyGrid" 
                    PageSize="20"
                    HeaderStyle-ForeColor="#FFFFFF"
                    PagerStyle-CssClass="myGridPagerStyle"
                    ShowFooter="true" 
                    ShowHeader="true" 
                    AllowPaging="true" 
                    AllowSorting="true" 
                    EmptyDataText="No Party Hit Data Found">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:BoundField ReadOnly="true" DataField="cms_party_status_desc" SortExpression="cms_party_status_desc"
                            HeaderText="Party Status" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_clnt_matt_code" SortExpression="cms_clnt_matt_code"
                            HeaderText="Client.Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_name" SortExpression="cms_party_name"
                            HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_client_name" SortExpression="cms_client_name"
                            HeaderText="Client" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_matter_name" SortExpression="cms_matter_name"
                            HeaderText="Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="MRP" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_ba" SortExpression="cms_ba" HeaderText="Billing Atty"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField ReadOnly="true" DataField="cms_close_date" SortExpression="cms_close_date"
                            HeaderText="Close Date" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                </asp:GridView>
                
            </asp:Panel>

            <asp:Panel ID="pnlNCPendingHits" runat="server">
                <h3>Pending Hits Not To Be Cleared</h3>
                <asp:GridView ID="gvTotalPendingClientHits" runat="server" 
                    DataSourceID="dsTotalPendingClientHits"
                    DataKeyNames="hitautoid" 
                    AutoGenerateColumns="false" 
                    HeaderStyle-ForeColor="#FFFFFF"
                    PagerStyle-CssClass="myGridPagerStyle"
                    SkinID="MyGrid" 
                    PageSize="20"
                    ShowFooter="true" 
                    ShowHeader="true" 
                    AllowPaging="true" 
                    AllowSorting="true" 
                    EmptyDataText="No Pending Data Found">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:BoundField ReadOnly="true" DataField="cms_party_status_desc" SortExpression="cms_party_status_desc"
                            HeaderText="Party Status" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_clnt_matt_code" SortExpression="cms_clnt_matt_code"
                            HeaderText="Client.Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_name" SortExpression="cms_party_name"
                            HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_client_name" SortExpression="cms_client_name"
                            HeaderText="Client" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_matter_name" SortExpression="cms_matter_name"
                            HeaderText="Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="MRP" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_ba" SortExpression="cms_ba" HeaderText="Billing Atty"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                </asp:GridView>
            </asp:Panel>

            <asp:Panel ID="pnlNCRelatedNameHits" runat="server">
                <h3>Related Name Hits Not To Be Cleared</h3>
                <asp:GridView ID="gvRelatedNameHits" runat="server" 
                    DataSourceID="dsRelatedNameHits"
                    DataKeyNames="pkrelatednameid" 
                    AutoGenerateColumns="false" 
                    HeaderStyle-ForeColor="#FFFFFF"
                    PagerStyle-CssClass="myGridPagerStyle"
                    SkinID="MyGrid" 
                    PageSize="20"
                    ShowFooter="true" 
                    ShowHeader="true" 
                    AllowPaging="true" 
                    AllowSorting="true"  
                    OnRowDataBound="gvRelatedNameHits_RowDataBound"
                    EmptyDataText="No Related Name Data Found">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" 
                                    Text='<%#Eval("pkrelatednameid")%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                       <asp:TemplateField HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblDomHitName" runat="server" Text='<%#Eval("dom_hit_name")%>'></asp:Label>
                        </ItemTemplate>
                       </asp:TemplateField>

                        <asp:BoundField ReadOnly="true" DataField="relationship_desc" SortExpression="relationship_desc"
                            HeaderText="Relation" HeaderStyle-HorizontalAlign="Left" />
                        
                        <asp:TemplateField HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblHitName" runat="server" Text='<%#Eval("hit_name")%>'></asp:Label>
                        </ItemTemplate>
                       </asp:TemplateField>
                        
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblIsClient" runat="server" 
                                    Text='<%#Eval("is_client")%>'>
                                </asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblDomIsClient" runat="server" 
                                    Text='<%#Eval("dom_is_client")%>'>
                                </asp:Label></ItemTemplate>
                        </asp:TemplateField>
                                                      

 
                    </Columns>
                </asp:GridView>
                <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;">
                </div>
            </asp:Panel>

            <asp:Panel ID="pnlNCNameLinks" runat="server">
                <h3>Name Link Hits Not To Be Cleared</h3>
                <asp:GridView ID="gvNameLinkHits" runat="server" 
                    DataSourceID="dsNameLinkHits" 
                    AutoGenerateColumns="false" 
                    HeaderStyle-ForeColor="#FFFFFF"
                    PagerStyle-CssClass="myGridPagerStyle"
                    SkinID="MyGrid" 
                    PageSize="20"
                    ShowFooter="true" 
                    ShowHeader="true" 
                    AllowPaging="true" 
                    AllowSorting="true"  
                    OnRowDataBound="gvNameLinkHits_RowDataBound"
                    EmptyDataText="No Name Link Data Found">
                    <Columns>
                                               
                       

                        <asp:BoundField ReadOnly="true" DataField="name" SortExpression="name"
                            HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />

                        <asp:BoundField ReadOnly="true" DataField="link type" SortExpression="link type"
                            HeaderText="Link Type" HeaderStyle-HorizontalAlign="Left" />
                        
                        <asp:BoundField ReadOnly="true" DataField="name link" SortExpression="name link"
                            HeaderText="Name Link" HeaderStyle-HorizontalAlign="Left" />
                                                      

 
                    </Columns>
                </asp:GridView>
                <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;">
                </div>
            </asp:Panel>

            


                <asp:SqlDataSource ID="dsTotalClientHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:Parameter Name="cms_hit_source" DefaultValue="Client" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="dsNameClearedHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:Parameter Name="cms_hit_source" DefaultValue="Name" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="dsTotalPendingClientHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
		                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
		                    <SelectParameters>
		                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
		                        <asp:Parameter Name="cms_hit_source" DefaultValue="Pending" />
                                <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
		                    </SelectParameters>
                </asp:SqlDataSource>

                
                <asp:SqlDataSource ID="dsRelatedNameHits" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_RelatedNames" 
                        SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="folderid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>

                 <asp:SqlDataSource ID="dsNameLinkHits" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_HitDetails" 
                        SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:Parameter Name="cms_hit_source" DefaultValue="Name Link" />
                        <asp:ControlParameter ControlID="ddlPKId" DefaultValue="" Name="partypkid"/>
                    </SelectParameters>
                </asp:SqlDataSource>
            


</ContentTemplate>
        







</ajaxToolkit:TabPanel>

        <ajaxToolkit:TabPanel ID="tpEmail" runat="server"  HeaderText="Email">
                <HeaderTemplate>
                    <asp:Label runat="server" ID="lblEmail" BackColor="Yellow" Text="Email"></asp:Label>
                </HeaderTemplate>
            <ContentTemplate>
        
           <h3>Construct Email to Solicit CRP/MRP Party Hits Clearance</h3>
                <br></br>
           <asp:UpdatePanel ID="upEmailMsg" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table class="myTable">
                    <tr>
                        <td>
                            <b>Select CRP/MRP:</b>
                        </td>
                        <td>
                             <asp:DropDownList ID="ddlEmailMRPCRP" runat="server"
                                DataSourceID="dsEmailMRPCRP"
                                DataTextField="cms_crp_mrp"
                                DataValueField="cms_crp_mrp_uno"
                                AutoPostBack="true"
                                AppendDataBoundItems="true"
                                 OnSelectedIndexChanged="ddlEmailMRPCRP_SelectedIndexChanged">
                                 <asp:ListItem Selected="True" Text="Select CRP/MRP" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                         <td>
                            <b>Select Party Name:</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEmailPKId" runat="server" 
                                DataSourceID="dsEmailPKId" 
                                DataTextField="nbipartyname" 
                                DataValueField="partypkid" 
                                AppendDataBoundItems="true" 
                                AutoPostBack="true"
                                OnSelectedIndexChanged="ddlEmailPKId_SelectedIndexChanged"
                                OnDataBound="ddlEmailPKId_DataBound">
                                <asp:ListItem Selected="True" Text="Select CRP/MRP above to populate associated Party Names." Value=""></asp:ListItem>
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>
                         <td>
                            <b>Select Email Recipient:</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEmailRecipient" runat="server" 
                                DataSourceID="dsEmailRecipient" 
                                DataTextField="employee_name" 
                                DataValueField="email" 
                                AppendDataBoundItems="true" 
                                AutoPostBack="true">
                                <asp:ListItem Selected="True" Text="Select Email Recipient" Value=""></asp:ListItem>
                            </asp:DropDownList>

                        </td>
                    </tr>
                    <tr>    
                         <td>
                            <b>Select Subject:</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEmailSubject" runat="server">
                                <asp:ListItem Text="CRP/MRP Review on Conflict report requested"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                         <td>
                            <b>Select Message:</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEmailMessage" runat="server" 
                                    OnSelectedIndexChanged="ddlEmailMessage_SelectedIndexChanged"
                                     AutoPostBack="false">
                                    <asp:ListItem Text=""></asp:ListItem>
                                    <asp:ListItem Text="In connection with opening a new matter, your review is needed as the Responsible Partner on matter(s) included on the report link."></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtMessage" runat="server" Width="99%" TextMode="MultiLine" Rows="3" Font-Names="Verdana" Font-Size=".90em">
                            </asp:TextBox>
                            <ajaxToolkit:TextBoxWatermarkExtender ID="tbeEmailBody" 
                                        runat="server"  
                                        WatermarkCssClass="watermark"
                                        TargetControlID="txtMessage"  
                                        WatermarkText="Enter new message here or enter text here to append to the message selected above...">
                            </ajaxToolkit:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right">
                            <asp:Button ID="btnSend" runat="server" Text="Send" OnClick="btnSend_Click" />
                        </td>
                    </tr>
                </table>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSend" />
                </Triggers>
           </asp:UpdatePanel>
  

                <br />
                <br />
                <asp:Label ID="lblEmailLog" runat="server" Visible="false" Text="Email Log"></asp:Label>

                <asp:GridView ID="gvEmailSent" runat="server"  
                         Visible="true"
                                AllowPaging="True"  
                                AllowSorting="True"
                                DataSourceID="dsEmailSent" 
                                DataKeyNames="id" 
                                AutoGenerateColumns="False" 
                                SkinID="MyGrid"
                                Width="100%"
                                PageSize="10"
                                OnDataBound="gvEmailSent_DataBound">

                                <Columns>

                                    <asp:BoundField Visible="false" DataField="ID"/>
                                    <asp:BoundField Visible="false" DataField="FolderID" />
                                    <asp:BoundField Visible="true" DataField="SendDate" HeaderText="Date Sent" />
                                    <asp:BoundField Visible="true" DataField="Sender" HeaderText="Sender"/>
                                    <asp:BoundField Visible="true" DataField="SentTo" HeaderText="Recipient"/>
                                    <asp:BoundField Visible="true" DataField="crpmrp" HeaderText="CRP/MRP"/>
                                    <asp:BoundField Visible="true" DataField="PartyHits" HeaderText="Hits To Clear"/>
                                    
                                </Columns>
                </asp:GridView>

               <br />
               
               <br />

               <asp:Label ID="lblURL" runat="server" Width="500px" Visible="false"></asp:Label>

                <asp:SqlDataSource ID="dsEmailMRPCRP" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_EmailMRPCRP" 
		            SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:SqlDataSource>



                <asp:SqlDataSource ID="dsEmailPKId" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_EmailParyInfo"
                        SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="fid" QueryStringField="fid" ConvertEmptyStringToNull="true" />
                            <asp:ControlParameter   ControlID="ddlEmailMRPCRP" 
                                                    PropertyName="SelectedValue" 
                                                    Name="cms_crp_mrp_uno"
                                                    ConvertEmptyStringToNull="true" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                
                <asp:SqlDataSource ID="dsEmailSent" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_EmailLog"
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" ConvertEmptyStringToNull="true" />
                    </SelectParameters>
                </asp:SqlDataSource>
            
</ContentTemplate>
        







</ajaxToolkit:TabPanel>
                
    </ajaxToolkit:TabContainer>

        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:SqlDataSource ID="dsPKId" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_PartyOriginalName"
                        SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="fid" QueryStringField="fid" ConvertEmptyStringToNull="true" />
                        </SelectParameters>
                    </asp:SqlDataSource>


    <asp:SqlDataSource ID="dsTotalHitCount" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                                SelectCommandType="StoredProcedure" 
                                SelectCommand="prc_qsel_TotalHitCount">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                                    <asp:ControlParameter ControlID="ddlPKId" DefaultValue="-99" Name="partypkid"/>
                                </SelectParameters>
                    </asp:SqlDataSource>
    <asp:SqlDataSource ID="dsMRPCRP" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_MRPCRP"
                        SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="fid" QueryStringField="fid" ConvertEmptyStringToNull="true" />
                        </SelectParameters>
                    </asp:SqlDataSource>
    <asp:ObjectDataSource ID="objClearReason" runat="server" 
                                SelectMethod="GetReasonCodes" 
                                TypeName="DAL">
                     </asp:ObjectDataSource>

    <asp:SqlDataSource ID="dsEmailRecipient" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:DPDataHubConnectionString %>"
                        SelectCommand="prc_qsel_EmailRecipient" 
                        SelectCommandType="StoredProcedure">
                    
                </asp:SqlDataSource>

    <asp:HiddenField ID="hfClientSignOff" runat="server" Value="1" />
    </ContentTemplate>
  </asp:UpdatePanel>
    
       
</asp:Content>

