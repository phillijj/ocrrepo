﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ConflictMock.aspx.cs" Inherits="Conflicts_ConflictMock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<ajaxToolkit:ToolkitScriptManager ID="scriptmanager" runat="server"></ajaxToolkit:ToolkitScriptManager>

<asp:LinkButton runat="server" ID="lb" Text="TEST"></asp:LinkButton>

<asp:DropDownList ID="ddlFID" runat="server" DataSourceID="dsFID" 
        DataTextField="FID" DataValueField="FID" AppendDataBoundItems="true" AutoPostBack="true"
        onselectedindexchanged="ddlFID_SelectedIndexChanged">
        <asp:ListItem Selected="True" Text="Select FID" Value=""></asp:ListItem>
</asp:DropDownList>
<asp:DropDownList ID="ddlPKId" runat="server" DataSourceID="dsPKId" 
        DataTextField="originalname" DataValueField="partypkid" AppendDataBoundItems="true" AutoPostBack="true"
        onselectedindexchanged="ddlPKId_SelectedIndexChanged">
        <asp:ListItem Selected="False" Text="Select Name" Value=""></asp:ListItem>
</asp:DropDownList>
<asp:SqlDataSource ID="dsFID" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>" SelectCommand="select distinct fid from nbi_cms_conflict_hits order by fid desc"></asp:SqlDataSource>
<asp:SqlDataSource ID="dsPKId" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
SelectCommand="select partypkid,originalname from NBI_CMS_Conflict_Sessions where fid = @fid">
<SelectParameters>
<asp:ControlParameter ControlID="ddlFID" ConvertEmptyStringToNull="true" Name="fid" />
</SelectParameters>
</asp:SqlDataSource>

<asp:DropDownList ID="ddlSignOff" runat="server"  DataSourceID="objClearReason" 
        DataValueField="txtReasonCode" DataTextField="txtReasonDesc" 
        ondatabinding="ddlSignOff_DataBinding" ondatabound="ddlSignOff_DataBound">
 
                                </asp:DropDownList>
 <asp:ObjectDataSource ID="objClearReason" runat="server" SelectMethod="GetReasonCodes" TypeName="DAL"></asp:ObjectDataSource>


<ajaxToolkit:Accordion ID="MyAccordion" runat="server" SelectedIndex="0"
            HeaderCssClass="accordionHeader" HeaderSelectedCssClass="accordionHeaderSelected"
            ContentCssClass="accordionContent" FadeTransitions="false" FramesPerSecond="40" 
            TransitionDuration="250" AutoSize="None" RequireOpenedPane="false" SuppressHeaderPostbacks="true">
            
            <Panes>
                <ajaxToolkit:AccordionPane ID="AccordionPane1" runat="server">
                    <Header><a href="" class="accordionLink">Clients</a></Header>

                   
                </ajaxToolkit:AccordionPane>
           
            
                <ajaxToolkit:AccordionPane ID="AccordionPane2" runat="server">
                    <Header><a href="" class="accordionLink">Client Parties</a></Header>
                </ajaxToolkit:AccordionPane>
            </Panes>
        </ajaxToolkit:Accordion>

</asp:Content>

