﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="Conflicts_Default" Theme="General" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <ajaxToolkit:ToolkitScriptManager ID="scriptmanager" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <ajaxToolkit:TabContainer ID="tc1" runat="server" Font-Bold="true">



        <ajaxToolkit:TabPanel ID="tpClient" runat="server" HeaderText="Clients">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="cbClientAll" runat="server" CausesValidation="True" AutoPostBack="True"
                                Text="Display Cleared Hits" OnCheckedChanged="cbClientAll_CheckedChanged" />
                        </td>
                        <td>
                            &nbsp&nbsp
                        </td>
                        <td align="right">
                            <asp:DataList ID="dlClientHitCount" runat="server" DataSourceID="dsClientHitCount">
                                <ItemTemplate>
                                    Remaining Hits:
                                    <asp:Label ID="lblToBeClearedHits" runat="server" CssClass="redBold" Text='<%#Eval("tobeclearedhits")%>'></asp:Label>&#160;&#160;
                                    Total Hits:
                                    <asp:Label ID="lblTotalHits" runat="server" CssClass="redBold" Text='<%#Eval("totalhits")%>'></asp:Label></ItemTemplate>
                            
                            </asp:DataList>
                        </td>
                    </tr>
                </table>


                <asp:GridView ID="gvClientHits" runat="server" AllowPaging="True" AllowSorting="True"
                    DataSourceID="dsClientHits" DataKeyNames="FID" AutoGenerateColumns="False" SkinID="MyGrid"
                    PageSize="20" ShowFooter="True" OnDataBound="gvClientHits_DataBound" OnRowDataBound="gvClientHits_RowDataBound" EmptyDataText="No Uncleared Client Hits Found">
                    <Columns>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField Visible="False" DataField="PartyPKID" />
                        <asp:BoundField ReadOnly="True" HeaderText="Party Name" DataField="NBIPartyName"
                            SortExpression="NBIPartyName" />
                        <asp:BoundField ReadOnly="True" HeaderText="Affiliation" DataField="nbiAffiliation"
                            SortExpression="nbiaffiliation" />
                        <asp:TemplateField SortExpression="cms_hit_name" HeaderText='Hit Name'>
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# "Conflicts_DD.aspx?uno=" + Eval("cms_uno") + "&source=client"  + "&name=" + 
                        HttpUtility.UrlEncode(Eval("cms_hit_name").ToString()) + "&autoid=" + Eval("hitautoid") + "&fid=" + Eval("fid") + "&signoff=" + Eval("SignOffCode") %>'
                                    runat="server" Target="_self" Text='<%#Eval("cms_hit_name")%>' /></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ReadOnly="True" HeaderText="Hit Status" DataField="cms_hit_status"
                            SortExpression="cms_hit_status" />
                        <asp:TemplateField SortExpression="cms_hit_source" HeaderText="Source">
                            <ItemTemplate>
                                <asp:Label ID="lblHitSource" runat="server" Text='<%#Eval("cms_hit_source")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ReadOnly="True" HeaderText="Sign Off" DataField="SignOffCode" SortExpression="SignOffCode" />
                        <asp:TemplateField HeaderText="Select">
                            <FooterTemplate>
                                <asp:Button ID="btnClientUpdate" runat="server" Text="Update" Visible="true" OnClick="btnClientUpdate_Click" /></FooterTemplate>
                            <HeaderTemplate>
                                <asp:DropDownList ID="ddlSignOff" runat="server" OnDataBound="ddlSignOff_ItemDataBound">
                                    <asp:ListItem Selected="True" Text="Select Sign-Off reason" Value=""></asp:ListItem>
                                    <asp:ListItem Text="OK" Value="OK"></asp:ListItem>
                                    <asp:ListItem Text="Friendly" Value="MyFriend"></asp:ListItem>
                                    <asp:ListItem Text="Client" Value="CurrentClient"></asp:ListItem>
                                </asp:DropDownList>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="cbClientSignOff" /></ItemTemplate>
                            <FooterStyle CssClass="submitButton" />
                        </asp:TemplateField>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="lblCMS_Uno" runat="server" Text='<%#Eval("cms_uno")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;">
                </div>

                <h3>All Client Hits</h3>
                <asp:GridView ID="gvTotalClientHits" runat="server" DataSourceID="dsTotalClientHits"
                    DataKeyNames="hitautoid" AutoGenerateColumns="false" SkinID="MyGrid" PageSize="20"
                    ShowFooter="true" ShowHeader="true" AllowPaging="true" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ReadOnly="true" DataField="needssignoff" SortExpression="needssignoff"
                            HeaderText="Sign Off Req'd" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="signoffcode" SortExpression="signoffcode"
                            HeaderText="Sign Off" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_status_desc" SortExpression="cms_party_status_desc"
                            HeaderText="Party Status" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_clnt_matt_code" SortExpression="cms_clnt_matt_code"
                            HeaderText="Client.Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_name" SortExpression="cms_party_name"
                            HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_client_name" SortExpression="cms_client_name"
                            HeaderText="Client" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_matter_name" SortExpression="cms_matter_name"
                            HeaderText="Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="MRP" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_ba" SortExpression="cms_ba" HeaderText="Billing Atty"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                </asp:GridView>
                <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;">
                </div>


                <asp:SqlDataSource ID="dsClientHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_clienthitdetail" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="hfClientSignOff" DbType="Int16" DefaultValue="1"
                            Name="signoff" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsTotalClientHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:Parameter Name="cms_hit_source" DefaultValue="Client" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsClientHitCount" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommandType="StoredProcedure" SelectCommand="prc_qsel_ClientHitCount">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="hfClientSignOff" runat="server" Value="1" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>


        <ajaxToolkit:TabPanel ID="tpName" runat="server" HeaderText="Names">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="cbNameAll" runat="server" Checked="false" CausesValidation="true"
                                AutoPostBack="true" Text="Display Cleared Hits" OnCheckedChanged="cbNameAll_CheckedChanged" />
                        </td>
                        <td>
                            &nbsp&nbsp
                        </td>
                        <td align="right">
                            <asp:DataList ID="dlNameHitCount" runat="server" DataSourceID="dsNameHitCount">
                                <ItemTemplate>
                                    Remaining Hits:
                                    <asp:Label ID="lblToBeClearedHits" runat="server" CssClass="redBold" Text='<%#Eval("tobeclearedhits")%>'></asp:Label>&#160;&#160;
                                    Total Hits:
                                    <asp:Label ID="lblTotalHits" runat="server" CssClass="redBold" Text='<%#Eval("totalhits")%>'></asp:Label></ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
                <asp:DataList runat="server" ID="dlNameHits" DataSourceID="dsNameHits" GridLines="Horizontal"
                    CssClass="myGrid" RepeatLayout="Table" RepeatDirection="Horizontal" RepeatColumns="3"
                    CellPadding="2" CellSpacing="2" AlternatingItemStyle-BackColor="#DDDDDDDD" ItemStyle-BorderColor="Black"
                    ItemStyle-BorderStyle="Dotted" OnItemDataBound="dlNameHits_ItemDataBound">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hfSignOffCode" runat="server" Value='<%#Eval("signoffcode") %>'>
                        </asp:HiddenField>
                        <asp:Label ID="lblPartyStatus" Font-Bold="true" Font-Size="12px" Font-Underline="true"
                            ForeColor="Blue" runat="server" Text='<%#Eval("cms_party_status_desc") %>'></asp:Label><br>
                        <asp:CheckBox Enabled="false" ID="cb1" runat="server" Text="Cleared" Checked='<%#Eval("needssignoff").ToString().Equals("0") %>' /><asp:Label
                            ID="lblID" runat="server" Visible="false" Text='<%#Eval("hitautoid").ToString() %>'></asp:Label><br>
                        <b>Party: </b>
                        <asp:Label ID="lblPN" runat="server" Text='<%#Eval("cms_party_name") %>'></asp:Label><br>
                        <b>Client: </b>
                        <asp:Label ID="lblCN" runat="server" Text='<%#Eval("cms_client_name") %>'></asp:Label><br>
                        <b>Matter: </b>
                        <asp:Label ID="lblMN" runat="server" Text='<%#Eval("cms_matter_name") %>'></asp:Label><br>
                        <b>Client.Matter: </b>
                        <asp:Label ID="lblCMC" runat="server" Text='<%#Eval("cms_clnt_matt_code") %>'></asp:Label><br>
                        <b>Open Date: </b>
                        <asp:Label ID="lblOpenDt" runat="server" Text='<%#Eval("cms_open_date") %>'></asp:Label><b>Date
                            Added: </b>
                        <asp:Label ID="lblDtAdded" runat="server" Text='<%#Eval("cms_date_added") %>'></asp:Label><br>
                        <b>Last Worked: </b>
                        <asp:Label ID="lblLastWorkDt" runat="server" Text='<%#Eval("cms_last_worked") %>'></asp:Label><b>Close
                            Date: </b>
                        <asp:Label ID="lblCloseDt" runat="server" Text='<%#Eval("cms_close_date") %>'></asp:Label><br>
                        <b>MRP: </b>
                        <asp:Label ID="lblMRP" runat="server" Text='<%#Eval("cms_crp_mrp") %>'></asp:Label><b>BA:
                        </b>
                        <asp:Label ID="lblBA" runat="server" Text='<%#Eval("cms_ba") %>'></asp:Label><br>
                        <asp:DropDownList ID="ddlSignOff" runat="server">
                        </asp:DropDownList>
                        <br>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:DataList><asp:Button runat="server" ID="btnNameUpdate" Text="Sign-Off" OnClick="btnNameUpdate_Click"
                    Visible="false" />
                    <br />

                <h3>Name Hits</h3>
                <asp:GridView ID="gvNameClearedHits" runat="server" DataSourceID="dsNameClearedHits"
                    DataKeyNames="hitautoid" AutoGenerateColumns="false" SkinID="MyGrid" PageSize="20"
                    ShowFooter="true" ShowHeader="true" AllowPaging="true" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ReadOnly="true" DataField="needssignoff" SortExpression="needssignoff"
                            HeaderText="Sign Off Req'd" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="signoffcode" SortExpression="signoffcode"
                            HeaderText="Sign Off" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_status_desc" SortExpression="cms_party_status_desc"
                            HeaderText="Party Status" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_clnt_matt_code" SortExpression="cms_clnt_matt_code"
                            HeaderText="Client.Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_name" SortExpression="cms_party_name"
                            HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_client_name" SortExpression="cms_client_name"
                            HeaderText="Client" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_matter_name" SortExpression="cms_matter_name"
                            HeaderText="Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="MRP" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_ba" SortExpression="cms_ba" HeaderText="Billing Atty"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                </asp:GridView>
                <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;">
                </div>
                <asp:SqlDataSource ID="dsNameHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_NameHitDetail" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:ControlParameter ControlID="hfNameSignOff" DbType="Int16" DefaultValue="1" Name="signoff" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsNameClearedHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                        <asp:Parameter Name="cms_hit_source" DefaultValue="Name" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsNameHitCount" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_NameHitCount" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="hfNameSignOff" runat="server" Value="1" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>


        <ajaxToolkit:TabPanel ID="tpPendingName" HeaderText="Pending Names" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="cbPendingNameAll" runat="server" CausesValidation="True" AutoPostBack="True"
                                Text="Display Cleared Hits" OnCheckedChanged="cbPendingNameAll_CheckedChanged" />
                        </td>
                        <td>
                            &nbsp&nbsp
                        </td>
                        <td align="right">
                            <asp:DataList ID="dlPendingNameHitCount" runat="server" DataSourceID="dsPendingNameHitCount">
                                <ItemTemplate>
                                    Remaining Hits:
                                    <asp:Label ID="lblToBeClearedHits" runat="server" CssClass="redBold" Text='<%#Eval("tobeclearedhits")%>'></asp:Label>&#160;&#160;
                                    Total Hits:
                                    <asp:Label ID="lblTotalHits" runat="server" CssClass="redBold" Text='<%#Eval("totalhits")%>'></asp:Label></ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
                <asp:DataList runat="server" ID="dlPendingHits" DataSourceID="dsPendingHits" GridLines="Horizontal"
                    CssClass="myGrid" RepeatLayout="Table" RepeatDirection="Horizontal" RepeatColumns="3"
                    CellPadding="2" CellSpacing="2" AlternatingItemStyle-BackColor="#DDDDDDDD" ItemStyle-BorderColor="Black"
                    ItemStyle-BorderStyle="Dotted" OnItemDataBound="dlPendingHits_ItemDataBound">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HiddenField ID="hfSignOffCodeb" runat="server" Value='<%#Eval("signoffcode") %>'>
                        </asp:HiddenField>
                        <asp:Label ID="lblPartyStatus" Font-Bold="true" Font-Size="12px" Font-Underline="true"
                            ForeColor="Blue" runat="server" Text='<%#Eval("cms_party_status_desc") %>'></asp:Label><br>
                        <asp:CheckBox Enabled="false" ID="cb1b" runat="server" Text="Cleared" Checked='<%#Eval("needssignoff").ToString().Equals("0") %>' /><asp:Label
                            ID="lblIDb" runat="server" Visible="false" Text='<%#Eval("hitautoid").ToString() %>'></asp:Label><br>
                        <b>Pending Party: </b>
                        <asp:Label ID="lblPNb" runat="server" Text='<%#Eval("cms_party_name") %>'></asp:Label><br>
                        <b>Client: </b>
                        <asp:Label ID="lblCNb" runat="server" Text='<%#Eval("cms_client_name") %>'></asp:Label><br>
                        <b>Pending Matter: </b>
                        <asp:Label ID="lblMNb" runat="server" Text='<%#Eval("cms_matter_name") %>'></asp:Label><br>
                        <b>Client.Matter: </b>
                        <asp:Label ID="lblCMCb" runat="server" Text='<%#Eval("cms_clnt_matt_code") %>'></asp:Label><br>
                        <b>Status: </b>
                        <asp:Label ID="lblHTb" runat="server" Text='<%#Eval("hittype") %>'></asp:Label><br>
                        <b>Req Atty: </b>
                        <asp:Label ID="lblRAb" runat="server" Text='<%#Eval("cms_crp_mrp") %>'></asp:Label><br>
                        <asp:DropDownList ID="ddlSignOffb" runat="server">
                        </asp:DropDownList>
                        <br>
                    </ItemTemplate>
                    <FooterTemplate>
                    </FooterTemplate>
                </asp:DataList><asp:Button runat="server" ID="btnPendingNameUpdate" Text="Sign-Off"
                    OnClick="btnPendingNameUpdate_Click" Visible="false" />
                    
                    <h3>Total Pending Client Hits</h3>
                <asp:GridView ID="gvTotalPendingClientHits" runat="server" DataSourceID="dsTotalPendingClientHits"
                    DataKeyNames="hitautoid" AutoGenerateColumns="false" SkinID="MyGrid" PageSize="20"
                    ShowFooter="true" ShowHeader="true" AllowPaging="true" AllowSorting="true">
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblPK" runat="server" Text='<%#Eval("hitautoid")%>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField ReadOnly="true" DataField="needssignoff" SortExpression="needssignoff"
                            HeaderText="Sign Off Req'd" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="signoffcode" SortExpression="signoffcode"
                            HeaderText="Sign Off" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_status_desc" SortExpression="cms_party_status_desc"
                            HeaderText="Party Status" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_clnt_matt_code" SortExpression="cms_clnt_matt_code"
                            HeaderText="Client.Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_party_name" SortExpression="cms_party_name"
                            HeaderText="Party Name" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_client_name" SortExpression="cms_client_name"
                            HeaderText="Client" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_matter_name" SortExpression="cms_matter_name"
                            HeaderText="Matter" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_crp_mrp" SortExpression="cms_crp_mrp"
                            HeaderText="MRP" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_ba" SortExpression="cms_ba" HeaderText="Billing Atty"
                            HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField ReadOnly="true" DataField="cms_last_worked" SortExpression="cms_last_worked"
                            HeaderText="Last Worked" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                </asp:GridView>
                <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;">
                </div>
                
                <asp:SqlDataSource ID="dsPendingHits"
                        runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                        SelectCommand="prc_qsel_PendingNameHitDetail" SelectCommandType="StoredProcedure">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                            <asp:ControlParameter ControlID="hfPendingNameSignOff" DbType="Int16" DefaultValue="1"
                                Name="signoff" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                <asp:SqlDataSource ID="dsPendingNameHitCount" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                    SelectCommand="prc_qsel_PendingNameHitCount" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:SqlDataSource ID="dsTotalPendingClientHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
		                    SelectCommand="prc_qsel_HitDetails" SelectCommandType="StoredProcedure">
		                    <SelectParameters>
		                        <asp:QueryStringParameter Name="fid" QueryStringField="fid" />
		                        <asp:Parameter Name="cms_hit_source" DefaultValue="Pending" />
		                    </SelectParameters>
                </asp:SqlDataSource>

                <asp:HiddenField ID="hfPendingNameSignOff" runat="server" Value="1" />
            </ContentTemplate>
        </ajaxToolkit:TabPanel>


    </ajaxToolkit:TabContainer>
</asp:Content>
