﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DPConflicts;

public partial class Conflicts_ConflictMock : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {



            lb.PostBackUrl = "~/Conflicts/ClearHitsMain.aspx?fid=0900000000000000000000000004878&usage=Review";

        }


        Label l = Master.FindControl("lblHeader") as Label;
        l.Text = "Day Pitney Online Conflict Report Mock Form";

        ddlSignOff.Items.Clear();
        
        ddlSignOff.DataBind();
        ddlSignOff.Items.Insert(0,new ListItem("TEST", ""));
    }
    protected void ddlFID_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlPKId.Items.Clear();
        ddlPKId.DataBind();
        Response.Redirect("~/Conflicts/ClearHitsMain.aspx?fid=" + ddlFID.SelectedValue);
    }
    protected void ddlPKId_SelectedIndexChanged(object sender, EventArgs e)
    {
        Response.Redirect("~/Conflicts/ClearHitsMain.aspx?fid=" + ddlFID.SelectedValue + "&partypkid=" + ddlPKId.SelectedValue);
    }
    protected void ddlSignOff_DataBound(object sender, EventArgs e)
    {
        

    }
    protected void ddlSignOff_DataBinding(object sender, EventArgs e)
    {
       
    }
    protected void gv1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
}