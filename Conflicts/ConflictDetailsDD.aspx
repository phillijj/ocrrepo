﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ConflictDetailsDD.aspx.cs" Inherits="Conflicts_ConflictDetailsDD" Theme="General" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

<ajaxToolkit:ToolkitScriptManager ID="scriptmanager" runat="server"></ajaxToolkit:ToolkitScriptManager>

    <asp:UpdateProgress ID="updProgress"
                        AssociatedUpdatePanelID="UpTop"  
                        runat="server">
        <ProgressTemplate>
           <div class="ModalPopupBG" 
                style=" position: fixed; 
                        text-align: center; 
                        height: 100%; 
                        width: 100%; 
                        top: 0; 
                        right: 0; 
                        left: 0; 
                        z-index: 9999999;">
                    <img alt="progress" src="../Styles/Images/loading.gif" 
                            style="padding: 10px;position:fixed;top:45%;left:50%;" />
            </div>
        
      
        </ProgressTemplate>
    </asp:UpdateProgress>

         <asp:UpdatePanel ID="upTop" runat="server">

        <ContentTemplate>

<input type="button" value=" Back " style="display: none;" onclick="parent.history.back(); return false;"/>


<asp:Label ID="lblClient" runat="server" Font-Bold="true" ForeColor="#005f9c" Font-Size="14px" Font-Names="Verdana"></asp:Label>
<br />
<asp:Label ID="lblStatus" runat="server" Font-Bold="true" ForeColor="Red" Font-Size="12px" Font-Names="Verdana" ></asp:Label>

<br/>


<asp:Panel ID="pnlMatters" runat="server" Visible="false">
<h3>Matters</h3>
        <asp:GridView ID="gvMatters" runat="server"  
            DataSourceID="dsMatters" 
            AutoGenerateColumns="false" 
            HeaderStyle-ForeColor="#FFFFFF"
            SkinID="MyGrid" 
            EmptyDataText="No Matters found"  
            AllowPaging="true"
            PagerStyle-CssClass="myGridPagerStyle"
            PageSize="25" 
            AllowSorting="true">
            <Columns>
    	        <asp:BoundField ReadOnly="True" HeaderText="Code" DataField="Matter_Code" SortExpression="matter_code" />
                <asp:BoundField ReadOnly="True" HeaderText="Name" DataField="Matter_Name" SortExpression="matter_name" />
                <asp:BoundField ReadOnly="True" HeaderText="Open" DataField="Open_Date" DataFormatString="{0:MM/dd/yyyy}" SortExpression="open_date"/>
                <asp:BoundField ReadOnly="True" HeaderText="Close" DataField="Close_Date" DataFormatString="{0:MM/dd/yyyy}" SortExpression="close_date"/>
                <asp:BoundField ReadOnly="True" HeaderText="MRP" DataField="mrp" SortExpression="crp" />
                <asp:BoundField ReadOnly="True" HeaderText="BA" DataField="ba" SortExpression="ba" />
                <asp:BoundField ReadOnly="True" HeaderText="Last Work" DataField="last_work_date" DataFormatString="{0:MM/dd/yyyy}" SortExpression="last_work_date"/>
          </Columns>
        </asp:GridView>
</asp:Panel>


<asp:Panel ID="pnlRelatedNames" runat="server">
 <h3>Related Names</h3>
        <asp:GridView ID="gvRelatedNames" runat="server" 
            DataSourceID="dsRelatedNames" 
            AutoGenerateColumns="false" 
            HeaderStyle-ForeColor="#FFFFFF"
            SkinID="MyGrid" 
            AllowPaging="true"
            PagerStyle-CssClass="myGridPagerStyle"
            EmptyDataText="No Related Names found" 
            AllowSorting="true">
            <Columns>
                <asp:BoundField ReadOnly="True" HeaderText="Relationship" DataField="desc" SortExpression="desc" />
            </Columns>
        </asp:GridView>

</asp:Panel>


<asp:Panel ID="pnlPartyHits" runat="server" Visible="false">
    <h3>Party Hits</h3>
        
        <asp:GridView ID="gvPartyHits" runat="server" 
            DataSourceID="dsPartyHits" 
            AutoGenerateColumns="false" 
            HeaderStyle-ForeColor="#FFFFFF"
            SkinID="MyGrid" 
            AllowPaging="true"
            PagerStyle-CssClass="myGridPagerStyle"
            EmptyDataText="No Party Hits found">
            <Columns>
                <asp:BoundField ReadOnly="True" HeaderText="Party Status" DataField="Party_Status"/>
                <asp:BoundField ReadOnly="True" HeaderText="Client.Matter" DataField="clnt_matt_code"/>
                <asp:BoundField ReadOnly="True" HeaderText="Client Name" DataField="client_name"/>
                <asp:BoundField ReadOnly="True" HeaderText="Open Date" DataField="open_date" DataFormatString="{0:MM/dd/yyyy}"/>
                <asp:BoundField ReadOnly="True" HeaderText="MRP" DataField="mrp"/>
                <asp:BoundField ReadOnly="True" HeaderText="Matter Party Name" DataField="Matter_Party_Name"/>
                <asp:BoundField ReadOnly="True" HeaderText="Matter Name" DataField="matter_name"/>
                <asp:BoundField ReadOnly="True" HeaderText="BA" DataField="ba"/>
                <asp:BoundField ReadOnly="True" HeaderText="Last Worked" DataField="last_time_date" 
                    DataFormatString="{0:MM/dd/yyyy}"/>
            </Columns>
        </asp:GridView>
         <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;"> </div>
      
</asp:Panel>

                        <asp:Panel ID="pnlRetMatters5" runat="server">
                <h3>Retired Matters Over 4 Years Old Not To Be Cleared</h3>
                <asp:GridView ID="gvRetMatters5" runat="server" 
                    DataSourceID="dsRetMatters5" 
                    AutoGenerateColumns="false" 
                    HeaderStyle-ForeColor="#FFFFFF"
                    PagerStyle-CssClass="myGridPagerStyle"
                    SkinID="MyGrid" 
                    PageSize="20"
                    ShowFooter="true" 
                    ShowHeader="true" 
                    AllowPaging="true" 
                    AllowSorting="true"  
                    EmptyDataText="No Retired Matters over 4 years old Found">
                    <Columns>
                                               
                       

                        <asp:BoundField ReadOnly="true" DataField="matter_code" SortExpression="matter_code"
                            HeaderText="Code" HeaderStyle-HorizontalAlign="Left" />

                        <asp:BoundField ReadOnly="true" DataField="matter_name" SortExpression="matter_name"
                            HeaderText="Name" HeaderStyle-HorizontalAlign="Left" />
                        
                        <asp:BoundField ReadOnly="true" DataField="open_date" SortExpression="open_date"
                            HeaderText="Open" HeaderStyle-HorizontalAlign="Left" />
                                                      
			<asp:BoundField ReadOnly="true" DataField="close_date" SortExpression="close_date"
                            HeaderText="Close" HeaderStyle-HorizontalAlign="Left" />
                            
                        <asp:BoundField ReadOnly="true" DataField="mrp" SortExpression="mrp"
                            HeaderText="MRP" HeaderStyle-HorizontalAlign="Left" />
                            
                        <asp:BoundField ReadOnly="true" DataField="ba" SortExpression="ba"
                            HeaderText="BA" HeaderStyle-HorizontalAlign="Left" />
                            
                        <asp:BoundField ReadOnly="true" DataField="last_work_date" SortExpression="last_work_date"
                            HeaderText="Last Work" HeaderStyle-HorizontalAlign="Left" />
 
                    </Columns>
                </asp:GridView>
                <div style="background-image: url(../Pictures/Shadow.gif); height: 16px; width: 100%;">
                </div>
            </asp:Panel>

             <asp:SqlDataSource ID="dsRetMatters5" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
                SelectCommand="prc_qsel_RetiredMatters5" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:QueryStringParameter Name="uno" QueryStringField="uno" />
                </SelectParameters>
        </asp:SqlDataSource>


<asp:SqlDataSource ID="dsMatters" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
			SelectCommand="prc_qsel_MatterHits" SelectCommandType="StoredProcedure" >
                <SelectParameters>
                    <asp:QueryStringParameter Name="uno" QueryStringField="uno" />
                </SelectParameters>
		</asp:SqlDataSource>
<asp:SqlDataSource ID="dsRelatedNames" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>" 
            SelectCommand="prc_qsel_RelatedNameHits" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:QueryStringParameter Name="uno" QueryStringField="uno" />
        </SelectParameters>
    </asp:SqlDataSource>
<asp:SqlDataSource ID="dsPartyHits" runat="server" ConnectionString="<%$ ConnectionStrings:MetastormConnectionString %>"
			    SelectCommand="prc_qsel_PartyHits" SelectCommandType="StoredProcedure" >
                <SelectParameters>
                    <asp:QueryStringParameter Name="uno" QueryStringField="uno" />
                </SelectParameters>
		</asp:SqlDataSource>

       
        </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>

