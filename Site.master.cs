﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DPConflicts;


public partial class SiteMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                String s = System.Web.HttpContext.Current.User.Identity.Name;
                UserProfile up = UserProfile.GetUserProfile(s);

                //if (System.DateTime.Compare(System.DateTime.Now.AddDays(-30), up.LastUpdatedDate) == 1)//older than 30 days
                //{
                    int i = s.IndexOf("\\");
                    s = s.Remove(0, i + 1);
                    AuthenticatedUser au = DAL.GetAuthenticatedUser(s);
                    up.EmailAddress = au.EmailAddress;
                    up.EmployeeCode = au.EmployeeCode;
                    up.Login = au.Login;
                    up.ProperName = au.ProperName;
                    up.Save();
                //}

            }

        }
        
    }
    


}
