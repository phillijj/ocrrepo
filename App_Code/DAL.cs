﻿//test
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using DP.DataLayer.DataObjects2;
using System.Data;
using System.Data.SqlClient;
using DP.Framework.Log;
using DPConflicts;

public static class DAL
{
    public static bool LogEmailRecord(string _folderid, string _sender, string _sentto, string _partyhits, string _mrpcrp)
    {
        /*
         prc_qinsrt_conflictemaillog
            @folderid nvarchar(31),
            @sender nvarchar(50),
            @sentto nvarchar(50),
            @partyhits nvarchar(100),
            @crpmrp nvarchar(75)
         */
        DB2 db = new DB2("MetastormWriterConnectionString");

        SqlParameter[] parameters = {   new SqlParameter("@folderid",SqlDbType.VarChar,31),
                                        new SqlParameter("@sender", SqlDbType.VarChar,50),
                                        new SqlParameter("@sentto",SqlDbType.VarChar,50),  
                                        new SqlParameter("@partyhits", SqlDbType.VarChar,100),
                                        new SqlParameter("@crpmrp", SqlDbType.VarChar,75)
                                    };

        parameters[0].Value = _folderid;
        parameters[1].Value = _sender;
        parameters[2].Value = _sentto;
        parameters[3].Value = _partyhits;
        parameters[4].Value = _mrpcrp;

        int i = 0;
        db.RunProcedure("prc_qinsrt_conflictemaillog", parameters, out i);

        if (!i.Equals(1))
            SingletonLogger.Instance.Error("CreateEmailRecord: " + i.ToString());

        return i == 1;
    }
	public static bool ClearHits(string _signOffCode, int _autoId, string _comments)
    {
        /*
         prc_qupdt_ClearHit] 
            (@signoffcode varchar(50),@comments varchar(3000), @hitautoid int, @user varchar(50)) 
         */

        DB2 db = new DB2("MetastormWriterConnectionString");
        int i = 0;
        bool _success = false;

        try
        {
           

            SqlParameter[] parameters = {   new SqlParameter("@signoffcode",SqlDbType.VarChar,50),
                                            new SqlParameter("@hitautoid", SqlDbType.Int),
                                            new SqlParameter("@comments",SqlDbType.VarChar,3000),  
                                            new SqlParameter("@user", SqlDbType.VarChar,50)
                                        };

            String s = System.Web.HttpContext.Current.User.Identity.Name;
            UserProfile up = UserProfile.GetUserProfile(s);

            parameters[0].Value = _signOffCode;
            parameters[1].Value = _autoId;
            parameters[2].Value = _comments;
            parameters[3].Value = up.ProperName;

            
            db.RunProcedure("prc_qupdt_ClearHit", parameters, out i);

            if (i > 0)
            {
                _success = true;
            }

        }
        catch (Exception ex)
        {
            SingletonLogger.Instance.Error(ex.Message);
        }

        SingletonLogger.Instance.Info("Info Message on Cleared hits : Result for Auto Id: " + _autoId.ToString() + " : " + _success.ToString());

        return _success;

    }
    public static bool FlagHitsInMS(string _folderId)
    {
        /*
         [NBI_FlagHitsAsOK] 
            (@FID char(31)) 
         */

        DB2 db = new DB2("MetastormWriterConnectionString");
        int i = 0;
        bool _success = false;

        try
        {


            SqlParameter[] parameters = {   new SqlParameter("@FID", SqlDbType.Char,31)};


            parameters[0].Value = _folderId;


            db.RunProcedure("NBI_FlagHitsAsOK", parameters, out i);

            if (i > 0)
            {
                _success = true;
            }

        }
        catch (Exception ex)
        {
            SingletonLogger.Instance.Error(ex.Message);
        }

        SingletonLogger.Instance.Info("Info Message on FlagHitsInMS : Result for Folder Id: " + _folderId.ToString() + " : " + _success.ToString());

        return _success;
    }

    public static bool IsComplete(string _folderId)
    {
        DB2 db = new DB2("MetastormConnectionString");
        SqlParameter[] parameters = {   new SqlParameter("@fid",SqlDbType.NVarChar,31)};

        parameters[0].Value = _folderId;
        return Convert.ToBoolean(db.GetScalar("prc_qsel_iscomplete",parameters));
    }
    public static bool FireHitsCompleteProc(string _folderId)
    {
        DB2 db = new DB2("MetastormConnectionString");
        SqlParameter[] parameters = { new SqlParameter("@FID", SqlDbType.Char, 31) };//based on type in proc

        parameters[0].Value = _folderId;
        return Convert.ToBoolean(db.GetScalar("NBI_FlagHitsAsOK", parameters));
    }
    public static bool IsRequired(string _signOffCode)
    {
        DB2 db = new DB2("MetastormConnectionString");
        SqlParameter[] parameters = { new SqlParameter("@signOffCode", SqlDbType.NVarChar, 8) };

        parameters[0].Value = _signOffCode;
        return Convert.ToBoolean(db.GetScalar("prc_qsel_isrequired", parameters));
    }
    public static IList<String> GetHitCount(string _fid, int? _pkid, int? _crpmrpuno)
    {
        IList<string> al = new List<String>();

        int _c = 0;//JJP 05/01/2013 hit counter

        SqlParameter[] parameters = {new SqlParameter("@fid",SqlDbType.NVarChar,31),
                                     new SqlParameter("@partypkid", SqlDbType.Int),
                                     new SqlParameter("@cms_crp_mrp_uno", SqlDbType.Int)};
        parameters[0].Value = _fid;
        parameters[1].Value = _pkid;
        parameters[2].Value = _crpmrpuno;
        try
        {

            string strConnString = System.Configuration.ConfigurationManager.ConnectionStrings["MetastormConnectionString"].ConnectionString;
            SqlCommand cmd = new SqlCommand("prc_qsel_HitCount");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@fid", _fid);
            cmd.Parameters.AddWithValue("@partypkid", _pkid);
            cmd.Parameters.AddWithValue("@cms_crp_mrp_uno", _crpmrpuno);

            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataSet ds = new DataSet())
                    {
                        sda.Fill(ds);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            DataRow dr = ds.Tables[0].Rows[0];

                            for (int i = 0; i < 12; i++)
                            {
                                if (dr != null)
                                {//JJP 05/01/2013
                                    al.Add(dr[i].ToString());
                                    if (i < 6)//JJP 05/01/2013 only count first 6
                                     _c += (int)dr[i];//JJP 05/01/2013
                                }//JJP 05/01/2013
                                else
                                    al.Add("0");
                            }
                        }
                        else
                        {
                            for (int i = 0; i < 12; i++)
                            {
                                    al.Add("0");
                            }
                        }
                    }
                }
            }


/*
            DB2 db = new DB2("MetastormConnectionString");
            DataRow dr = db.GetDataRow("prc_qsel_HitCount", parameters);

            for (int i = 0; i < 12; i++)
            {
                if (dr!= null)
                    al.Add(dr[i].ToString());
                else
                    al.Add("0");
            }
 */
        }
        catch (Exception ex)
        {
            SingletonLogger.Instance.Error(ex.Message);
        }

        al.Add(_c.ToString());//JJP 05/01/2013
        return al;
        
    }
    public static IList<Reason> GetReasonCodes()
    {
         IList<Reason> list = new List<Reason>();

        try
        {
            DB2 db = new DB2("MetastormConnectionString");
            DataTable dt = db.GetDataTable("prc_qsel_getClearReasons", null);
            
            foreach(DataRow dr in dt.Rows)
            {
                //bool b = Reason.ConvertCheckToBoolean(dr["chkRequiresComment"].ToString());

                string _desc = "* ";
                //if (b)
                    //_desc += dr["txtReasonDesc"].ToString();
                //else
                    _desc = dr["txtReasonDesc"].ToString();

                list.Add(new Reason(dr["txtReasonCode"].ToString(),_desc,
                    Reason.ConvertCheckToBoolean(dr["chkRequiresComment"].ToString())));
            }
 
        }
        catch (Exception ex)
        {
            SingletonLogger.Instance.Error(ex.Message);
        }

        return list;
    }
    public static DataTable GetDataTable(string _sql, string _connectionString)
    {
        DB2 db = new DB2(_connectionString);
        return db.GetDataTable(_sql);
    }
    
    public static DataTable GetDataTable(string _storedProc, SqlParameter[] _params, string ConnectionString)
    {
        DataTable dt = null;
        try
        {
            DB2 db = new DB2(ConnectionString);
            dt= db.GetDataTable(_storedProc, _params);
        }
        catch (Exception ex)
        {
            SingletonLogger.Instance.Warning(ex.Message);
        }
        return dt;
    }
    public static string GetMetastormValueAsString(string _sql)
    {
        DB2 db = new DB2("MetastormConnectionString");
        return db.GetScalar(_sql).ToString();
    }

    public static IList<String> GetHeaderInfo(string _fid)
    {
        IList<string> al = new List<String>();
        SqlParameter[] parameters = { new SqlParameter("@FID", SqlDbType.NVarChar, 31) };

        parameters[0].Value = _fid;
        
        DataTable _dt = GetDataTable("prc_qsel_ConflictReportHeaderInfo",parameters,"MetastormConnectionString");
        if (_dt.Rows.Count == 1)
        {
            al.Add(_dt.Rows[0][0].ToString());
            al.Add(_dt.Rows[0][1].ToString());
            al.Add(_dt.Rows[0][2].ToString());
            al.Add(_dt.Rows[0][3].ToString());
            al.Add(_dt.Rows[0][4].ToString());
        }
        else
        {
            SingletonLogger.Instance.Error("DAL.GetHeaderInfo returned " + _dt.Rows.Count + " row(s); one is expected.  See prc_qsel_ConflictReportHeaderInfo with Folder: " + _fid);
        }

        return al;
    }
    
    public static AuthenticatedUser GetAuthenticatedUser(string _loginoruno)
    {
        DB2 db = new DB2("DPDATAHUBConnectionString");

        int i = 0;
        DataRow dr;

        if (!Int32.TryParse(_loginoruno, out i))
            dr = db.GetDataRow("select propername,email,employee_code,login from employees where login = '" + _loginoruno + "'");
        else
            dr=db.GetDataRow("select propername,email,employee_code,login from employees where empl_uno = " + i );


        if (dr == null)
        {
            AuthenticatedUser au = new AuthenticatedUser(_loginoruno + "@daypitney.com",
                                                         _loginoruno.ToString(),
                                                         _loginoruno.ToString(),
                                                         "-99".ToString());
            return au;
        }

        if (!dr.HasErrors)
        {
            AuthenticatedUser au = new AuthenticatedUser(dr["email"].ToString(),
                                                         dr["propername"].ToString(),
                                                         dr["login"].ToString(),
                                                         dr["employee_code"].ToString());
            return au;
        }
        else
        {
            AuthenticatedUser au = new AuthenticatedUser(_loginoruno + "@daypitney.com",
                                                         _loginoruno.ToString(),
                                                         _loginoruno.ToString(),
                                                         "-99".ToString());
            return au;
        }

    }
}

[Serializable()]
public class Reason
{
    private string _code;
    private string _desc;
    private bool _commentsReq;

    public Reason(string _code,string _desc, bool _commentsReq)
    {
        this._code=_code;
        this._desc=_desc;
        this._commentsReq=_commentsReq;
    }

    public string txtReasonCode
    {
        get{return _code;}
        set{_code = value;}
    }
    public string txtReasonDesc
    {
        get{return _desc;}
        set{_desc=value;}
    }
    public Boolean chkRequiresComment
    {
        get{return _commentsReq;}
        set{_commentsReq=value;}
    }

    public static Boolean ConvertCheckToBoolean(string _val)
    {
        if (_val.Equals("0"))
            return false;
        else if (_val.Equals("1"))
            return true;
        else
            return false;
    }
}