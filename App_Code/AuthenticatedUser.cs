﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DPConflicts;


public class AuthenticatedUser
{
    private string _email;
    private string _properName;
    private string _login;
    private string _employeeCode;

    public AuthenticatedUser(string _email, string _properName, string _login, string _employeeCode)
    {
        EmailAddress = _email;
        ProperName = _properName;
        Login = _login;
        EmployeeCode = _employeeCode;

        
    }

    public string EmailAddress
    {
        get { return _email; }
        set { _email = value; }
    }
    public string ProperName
    {
        get { return _properName; }
        set { _properName = value; }
    }
    public string Login
    {
        get { return _login; }
        set { _login = value; }
    }
    public string EmployeeCode
    {
        get { return _employeeCode; }
        set { _employeeCode = value; }
    }
}
