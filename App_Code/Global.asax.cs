﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;

using DP.Framework.Log;
using DP.DataLayer.DataObjects;


// To Add Global CodeBehind......
// **Need to add global.asax to project, replace the Application Directive with the following:
// <%@ Application Language="C#" Inherits="Global" %>
// 

/// <summary>
/// This implementation of Global brings the model back to VS 2003 global.asax.
/// </summary>
public class Global : HttpApplication
{
    /// <summary>
    /// Event handler for application start event. Initializes logging.
    /// </summary>
    protected void Application_Start(Object sender, EventArgs e)
    {
        // Initialize logging facility
        InitializeLogger();

    }
    protected void Application_OnEnd(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown


    }
    /// <summary>
    /// Event handler for session start event.
    /// </summary>
    protected void Session_OnStart(Object sender, EventArgs e)
    {
        //Session.Timeout = 1;
        //Session["CustomSessionId"] = Guid.NewGuid();
        if (Context.User.Identity.Name.ToLower() == @"dpllp\phillijj")
        {
            return;
        }

        SingletonLogger.Instance.Info(Context.User.Identity.Name.ToString() + " has accessed the Online Conflict Report application at " + DateTime.Now);



    }
    protected void Session_End(Object sender, EventArgs e)
    {
        //SingletonLogger.Instance.Info(Context.User.Identity.Name.ToString() + " has ended the BDE application at " + DateTime.Now);
        //Response.Redirect("TimeoutPage.htm");

    }
    protected void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        SingletonLogger.Instance.Error(e.ToString());

    }
    protected void Application_AuthenticateRequest(Object sender, EventArgs e)
    {

    }
    protected void Application_EndRequest(Object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Initializes logging facility with severity level and observer(s).
    /// Private helper method.
    /// </summary>
    private void InitializeLogger()
    {
        // Read and assign application wide logging severity
        string severity = ConfigurationManager.AppSettings.Get("LogSeverity");
        SingletonLogger.Instance.Severity = (LogSeverity)Enum.Parse(typeof(LogSeverity), severity, true);

        ILog log;

        // Send log messages to database
        //log = new ObserverLogToDatabase();
        //SingletonLogger.Instance.Attach(log);

        // Send log messages to email
        string from = ConfigurationManager.AppSettings.Get("EmailAddressFrom").ToString();
        string to = ConfigurationManager.AppSettings.Get("EmailAddressTo").ToString();
        string subject = ConfigurationManager.AppSettings.Get("EmailSubject").ToString();
        SmtpClient smtpClient = new SmtpClient();

        log = new ObserverLogToEmail(from, to, subject, smtpClient);
        SingletonLogger.Instance.Attach(log);

        // Send log messages to a file
        //log = new ObserverLogToFile(@"C:\Temp\DP_BD.log");
        //SingletonLogger.Instance.Attach(log);

        // Send log message to event log
        //log = new ObserverLogToEventlog();
        //SingletonLogger.Instance.Attach(log);



    }
}
