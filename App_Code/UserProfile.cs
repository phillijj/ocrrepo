﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;

namespace DPConflicts
{
    [Serializable()]
    public class UserProfile : ProfileBase
    {
        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }

        public static UserProfile GetUserProfile() 
        { 
            return Create(Membership.GetUser().UserName) as UserProfile; 
        }

        [SettingsAllowAnonymous(false)]
        public string EmailAddress 
        {
            get { return base["EmailAddress"] as string; }
            set { base["EmailAddress"] = value; } 
        }

        [SettingsAllowAnonymous(false)]
        public string ProperName 
        {
            get { return base["ProperName"] as string; }
            set { base["ProperName"] = value; } 
        }

        [SettingsAllowAnonymous(false)]
        public string Login 
        {
            get { return base["Login"] as string; }
            set { base["Login"] = value; } 
        }

        [SettingsAllowAnonymous(false)]
        public string EmployeeCode
        {
            get { return base["EmployeeCode"] as string; }
            set { base["EmployeeCode"] = value; }
        }
    }
}